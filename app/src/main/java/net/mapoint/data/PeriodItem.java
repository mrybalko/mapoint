package net.mapoint.data;

import net.mapoint.R;

import java.util.ArrayList;
import java.util.List;

public enum PeriodItem implements ToggleItemInterface {

    Day("today", R.string.period_items_day, R.drawable.single_selection_btn_selector),
    Week("week",  R.string.period_items_week, R.drawable.single_selection_btn_selector),
    Month("month",  R.string.period_items_month, R.drawable.single_selection_btn_selector);

    private int labelUIResId;
    private String realValue;
    private int imageResId;
    private int bgResId;

    PeriodItem(String realValue, int labelResId, int bgResId) {
        this.realValue = realValue;
        labelUIResId = labelResId;
        this.bgResId = bgResId;
    }

    public int labelUIResId() {
        return labelUIResId;
    }

    public String realValue() {
        return realValue;
    }

    public List<Integer> imageResId() {
        return new ArrayList<>();
    }

    public int bgResId() {
        return bgResId;
    }

    public static PeriodItem byRealValue(final String value) {
        for (PeriodItem level : values()) {
            if (level.realValue().equals(value)) {
                return level;
            }
        }

        return null;
    }
}
