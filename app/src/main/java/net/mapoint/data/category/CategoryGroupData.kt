package net.mapoint.data.category

import net.mapoint.utils.data.CategoriesUtils

data class CategoryGroupData(val id: String,
                             val name: String,
                             val subcategoryList: List<CategoryData>,
                             val color: Int,
                             var needExpandItems:Boolean = true,
                             var icon:CategoryIconPathProvider
                             ) {

    fun isSomeSubcategoriesChecked(): Boolean {
        return CategoriesUtils.isSomeSubcategoriesSelected(subcategoryList)
    }
}