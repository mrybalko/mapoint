package net.mapoint.data.category

data class CategoryIconPathProvider(var remoteUrl:String="", var resourceIconId:Int=0)