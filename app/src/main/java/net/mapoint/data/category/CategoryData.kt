package net.mapoint.data.category

data class CategoryData(val id: String,
                        val name: String,
                        val groupCategoryId: String,
                        var isSelected: Boolean = false,
                        val color: Int)