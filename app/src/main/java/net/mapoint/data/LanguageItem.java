package net.mapoint.data;

import net.mapoint.R;

import java.util.ArrayList;
import java.util.List;

public enum LanguageItem implements ToggleItemInterface {

    Ru("ru", R.string.ru, R.drawable.single_selection_btn_selector),
    En("en", R.string.en, R.drawable.single_selection_btn_selector);

    private int labelUIResId;
    private String realValue;
    private int imageResId;
    private int bgResId;

    LanguageItem(String realValue, int labelResId, int bgResId) {
        this.realValue = realValue;
        labelUIResId = labelResId;
        this.bgResId = bgResId;
    }

    public int labelUIResId() {
        return labelUIResId;
    }

    public String realValue() {
        return realValue;
    }

    public List<Integer> imageResId() {
        return new ArrayList<>();
    }

    public int bgResId() {
        return bgResId;
    }

    public static LanguageItem byRealValue(final String value) {
        for (LanguageItem level : values()) {
            if (level.realValue().equals(value)) {
                return level;
            }
        }

        return null;
    }

    public static boolean contains(String countryCode) {
        for (LanguageItem level : values()) {
            if (level.realValue().equalsIgnoreCase(countryCode)) {
                return true;
            }
        }

        return false;
    }
}
