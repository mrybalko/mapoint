package net.mapoint.data;

import android.location.Location;

import net.mapoint.data.locations.LocationData;

import java.util.List;

public class LocationsDetailsData {

    private Location currentLocation;
    private List<LocationData> locationData;
    private int radius;

    private LocationsDetailsData(Builder builder) {
        currentLocation = builder.currentLocation;
        locationData = builder.locationData;
        radius = builder.radius;
    }

    public Location currentLocation() {
        return currentLocation;
    }

    public List<LocationData> locationData() {
        return locationData;
    }

    public int radius() {
        return radius;
    }

    public static final class Builder {
        private Location currentLocation;
        private List<LocationData> locationData;
        private int radius;

        public Builder() {
        }

        public Builder(LocationsDetailsData copy) {
            this.currentLocation = copy.currentLocation();
            this.locationData = copy.locationData();
            this.radius = copy.radius();
        }

        public Builder currentLocation(Location val) {
            currentLocation = val;
            return this;
        }

        public Builder locationData(List<LocationData> val) {
            locationData = val;
            return this;
        }

        public Builder radius(int val) {
            radius = val;
            return this;
        }

        public LocationsDetailsData build() {
            return new LocationsDetailsData(this);
        }

        public void clear() {
            currentLocation = null;
            locationData = null;
            radius = 0;
        }
    }
}
