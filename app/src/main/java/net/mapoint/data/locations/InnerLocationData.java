package net.mapoint.data.locations;

import java.io.Serializable;
import java.util.List;

public class InnerLocationData implements Serializable {
    private String id;

    private double lat;

    private double lng;

    private String address;

    private String type;

    private String name;

    private List<String> phones;

    private int distance;

    private InnerLocationData(Builder builder) {
        id = builder.id;
        lat = builder.lat;
        lng = builder.lng;
        address = builder.address;
        type = builder.type;
        name = builder.name;
        phones = builder.phones;
        distance = builder.distance;
    }

    public String getId() {
        return id;
    }

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }

    public String getAddress() {
        return address;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public List<String> getPhones() {
        return phones;
    }

    public int getDistance() {
        return distance;
    }


    public static InnerLocationData forTest() {
        return new Builder()
                .id("123456789")
                .lat(27.6123)
                .lng(53.9123)
                .address("test address")
                .type("test type")
                .name("test name")
                .distance(123)
                .build();
    }

    public static InnerLocationData forTest(String name) {
        return new Builder()
                .id("123456789")
                .lat(27.6123)
                .lng(53.9123)
                .address("test address")
                .type("test type")
                .name(name)
                .distance(123)
                .build();
    }


    public static final class Builder {
        private String id;
        private double lat;
        private double lng;
        private String address;
        private String type;
        private String name;
        private List<String> phones;
        private int distance;

        public Builder() {
        }

        public Builder id(String val) {
            id = val;
            return this;
        }

        public Builder lat(double val) {
            lat = val;
            return this;
        }

        public Builder lng(double val) {
            lng = val;
            return this;
        }

        public Builder address(String val) {
            address = val;
            return this;
        }

        public Builder type(String val) {
            type = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder phones(List<String> val) {
            phones = val;
            return this;
        }


        public Builder distance(int val) {
            distance = val;
            return this;
        }

        public InnerLocationData build() {
            return new InnerLocationData(this);
        }
    }
}
