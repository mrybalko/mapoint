package net.mapoint.data.locations;

import java.io.Serializable;

public class SimpleLocation implements Serializable {
    private double lat;
    private double lon;

    public SimpleLocation(double lat, double lon) {
        this.lat = lat;
        this.lon = lon;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    @Override
    public String toString() {
        return "SimpleLocation{" +
                "lat=" + lat +
                ", lon=" + lon +
                '}';
    }
}
