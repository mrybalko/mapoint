package net.mapoint.data.locations;

import java.io.Serializable;
import java.util.List;

public class LocationData implements Serializable {

    private String id;
    private double lat;
    private double lng;
    private String address;
    private String name;
    private String type;
    private List<EventData> facts;
    private List<EventData> offers;
    private List<String> phones;
    private List<LocationWorkingTimeData> locationWorkingTimes;
    private int distance;

    private LocationData(Builder builder) {
        id = builder.id;
        lat = builder.lat;
        lng = builder.lng;
        address = builder.address;
        name = builder.name;
        type = builder.type;
        facts = builder.facts;
        offers = builder.offers;
        phones = builder.phones;
        locationWorkingTimes = builder.locationWorkingTimes;
        distance = builder.distance;
    }

    public String id () {
        return id;
    }

    public double lat() {
        return lat;
    }

    public double lng() {
        return lng;
    }

    public String address() {
        return address;
    }

    public String name() {
        return name;
    }

    public String type() {
        return type;
    }

    public List<EventData> facts() {
        return facts;
    }

    public List<EventData> offers() {
        return offers;
    }

    public List<String> phones() {
        return phones;
    }

    public List<LocationWorkingTimeData> locationWorkingTimes() {
        return locationWorkingTimes;
    }

    public int distance() {
        return distance;
    }

    public static final class Builder {
        private String id;
        private double lat;
        private double lng;
        private String address;
        private String name;
        private String type;
        private List<EventData> facts;
        private List<EventData> offers;
        private List<String> phones;
        private List<LocationWorkingTimeData> locationWorkingTimes;
        private int distance;

        public Builder() {
        }

        public Builder id(String val) {
            id = val;
            return this;
        }

        public Builder lat(double val) {
            lat = val;
            return this;
        }

        public Builder lng(double val) {
            lng = val;
            return this;
        }

        public Builder address(String val) {
            address = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder type(String val) {
            type = val;
            return this;
        }

        public Builder facts(List<EventData> val) {
            facts = val;
            return this;
        }

        public Builder offers(List<EventData> val) {
            offers = val;
            return this;
        }

        public Builder phones(List<String> val) {
            phones = val;
            return this;
        }

        public Builder workingTime(List<LocationWorkingTimeData> val) {
            locationWorkingTimes = val;
            return this;
        }

        public Builder distance(int val) {
            distance = val;
            return this;
        }

        public LocationData build() {
            return new LocationData(this);
        }
    }
}
