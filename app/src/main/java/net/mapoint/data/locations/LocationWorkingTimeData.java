package net.mapoint.data.locations;

import java.io.Serializable;

public class LocationWorkingTimeData implements Serializable {

    private String id;
    private Integer dayNumber;
    private String startTime;
    private String endTime;

    public LocationWorkingTimeData(String id, Integer dayNumber, String startTime, String endTime) {
        this.id = id;
        this.dayNumber = dayNumber;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public String getId() {
        return id;
    }

    public Integer getDayNumber() {
        return dayNumber;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getEndTime() {
        return endTime;
    }
}
