package net.mapoint.data.locations;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

public class EventData implements Serializable {

    private InnerLocationData innerLocationData;

    private String id;

    private String text;

    private String fullDescription;

    private String link;

    private List<String> subcategories;

    private List<EventDatesItemData> dates;

    private EventType eventType;

    public InnerLocationData getInnerLocationData() {
        return innerLocationData;
    }

    public String getText() {
        return text;
    }

    public String getFullDescription() {
        return fullDescription;
    }

    public String getLink() {
        return link;
    }

    public List<String> getSubcategories() {
        return subcategories;
    }

    public EventType eventType() {
        return eventType;
    }

    public String getId() {
        return id;
    }

    public List<EventDatesItemData> dates() {
        return dates;
    }

    private EventData(Builder builder) {
        innerLocationData = builder.innerLocationData;
        id = builder.id;
        text = builder.text;
        fullDescription = builder.fullDescription;
        link = builder.link;
        subcategories = builder.subcategories;
        eventType = builder.eventType;
        dates = builder.dates;
    }

    public static EventData forTest() {

        return forTest("123456789",
                "test text");
    }

    public static EventData forTest(String id, String text) {
        return new Builder()
                .innerLocationData(InnerLocationData.forTest())
                .id(id)
                .text(text)
                .fullDescription("test full description text")
                .link("link")
                .subcategories(Arrays.asList("1", "2"))
                .eventType(EventType.Fact)
                .build();
    }

    public static final class Builder {
        private InnerLocationData innerLocationData;
        private String id;
        private String text;
        private String fullDescription;
        private String link;
        private List<String> subcategories;
        public EventType eventType;
        private List<EventDatesItemData> dates;

        public Builder() {
        }

        public Builder innerLocationData(InnerLocationData val) {
            innerLocationData = val;
            return this;
        }

        public Builder id(String val) {
            id = val;
            return this;
        }

        public Builder text(String val) {
            text = val;
            return this;
        }

        public Builder fullDescription(String val) {
            fullDescription = val;
            return this;
        }

        public Builder link(String val) {
            link = val;
            return this;
        }


        public Builder subcategories(List<String> val) {
            subcategories = val;
            return this;
        }

        public Builder dates(List<EventDatesItemData> val) {
            dates = val;
            return this;
        }

        public Builder eventType(EventType val) {
            eventType = val;
            return this;
        }

        public EventData build() {
            return new EventData(this);
        }
    }
}
