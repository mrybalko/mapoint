package net.mapoint.data.locations

data class MapMarkerData(
        val title: String,
        val locationName: String,
        val subcategories: List<String>,
        val date: List<EventDatesItemData>)