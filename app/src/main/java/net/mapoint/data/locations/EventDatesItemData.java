package net.mapoint.data.locations;

import java.io.Serializable;
import java.util.List;

public class EventDatesItemData implements Serializable {

    private String id;
    private String startDate;
    private String endDate;
    private List<String> sessions;
    
    public EventDatesItemData(String id,
                              String startDate,
                              String endDate,
                              List<String> sessions) {
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
        this.sessions = sessions;
    }

    public String getId() {
        return id;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public List<String> getSessions() {
        return sessions;
    }
}
