package net.mapoint.data.locations;

public enum EventType {
    Unknown("Unknown"),
    Fact("FACT"),
    Offer("OFFER"),
    Test("TEST");

    private String value;

    EventType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static EventType byValue(final String value) {
        for (EventType type : values()) {
            if (type.value.equals(value)) {
                return type;
            }
        }

        return Unknown;
    }
}