package net.mapoint.data.adding;

import java.util.ArrayList;
import java.util.List;

public class AddingEventData {

    private String address;
    private long locationId;

    private String phones;

    private String text;

    private String link;
    private String fullText;

    private Float lat;
    private Float lng;

    private ParameterData locationName;
    private List<ParameterData> categories;

    private List<AddingEventDateTimeData> datesTime;

    private AddingEventData(Builder builder) {
        address = builder.address;
        categories = builder.categories;
        phones = builder.phones;
        text = builder.text;
        link = builder.link;
        fullText = builder.fullText;
        lat = builder.lat;
        lng = builder.lng;
        locationName = builder.locationName;
        datesTime = builder.datesTime;
        locationId = builder.locationId;
    }

    public String getAddress() {
        return address;
    }

    public String getPhones() {
        return phones;
    }

    public String getText() {
        return text;
    }

    public String getLink() {
        return link;
    }

    public String getFullText() {
        return fullText;
    }

    public Float getLat() {
        return lat;
    }

    public Float getLng() {
        return lng;
    }

    public long getLocationId() {
        return locationId;
    }

    public List<ParameterData> getCategories() {
        return categories;
    }

    public List<AddingEventDateTimeData> getDatesTime() {
        return datesTime;
    }

    public String getLocationNameValue() {
        if (locationName != null) {
            return locationName.value();
        }
        return "";
    }


    public static final class Builder {
        private long locationId;
        private String address;
        private String phones;
        private String text;
        private String link;
        private String fullText;
        private Float lat;
        private Float lng;
        private ParameterData locationName;
        private List<ParameterData> categories;
        private List<AddingEventDateTimeData> datesTime;

        public Builder() {
        }

        public Builder address(String val) {
            address = val;
            return this;
        }

        public Builder categories(List<ParameterData> val) {
            categories = val;
            return this;
        }

        public Builder phones(String val) {
            phones = val;
            return this;
        }

        public Builder text(String val) {
            text = val;
            return this;
        }

        public Builder link(String val) {
            link = val;
            return this;
        }

        public Builder fullText(String val) {
            fullText = val;
            return this;
        }

        public Builder lat(Float val) {
            lat = val;
            return this;
        }

        public Builder lng(Float val) {
            lng = val;
            return this;
        }

        public Builder locationName(ParameterData val) {
            locationName = val;
            return this;
        }

        public Builder locationId(long val) {
            locationId = val;
            return this;
        }

        public Builder datesTime(List<AddingEventDateTimeData> val) {
            datesTime = new ArrayList<>(val);
            return this;
        }

        public String getText() {
            return text;
        }

        public String getAddress() {
            return address;
        }

        public ParameterData getLocationName() {
            return locationName;
        }

        public List<ParameterData> getCategories() {
            return categories;
        }

        public AddingEventData build() {
            return new AddingEventData(this);
        }

        @Override
        public String toString() {
            return "Builder{" +
                    "address='" + address + '\'' +
                    ", phones='" + phones + '\'' +
                    ", text='" + text + '\'' +
                    ", link='" + link + '\'' +
                    ", fullText='" + fullText + '\'' +
                    ", lat=" + lat +
                    ", lng=" + lng +
                    ", locationName=" + locationName +
                    ", categories=" + categories +
                    ", datesTime=" + datesTime +
                    '}';
        }
    }
}
