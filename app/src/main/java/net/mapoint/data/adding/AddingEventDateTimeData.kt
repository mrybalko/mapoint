package net.mapoint.data.adding

data class AddingEventDateTimeData(
        val startDate:String, val endDate:String = "", val times:MutableList<String>
)