package net.mapoint.data.adding

import java.io.Serializable

class ParameterData(private val id: String,
                    private val value: String,
                    var isSelected:Boolean = false) : Serializable {

    constructor(id: String, value: String) : this(id, value, false)

    fun id(): String {
        return id
    }

    fun value(): String {
        return value
    }
}
