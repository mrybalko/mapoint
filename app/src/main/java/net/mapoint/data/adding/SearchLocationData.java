package net.mapoint.data.adding;

import java.util.List;

public class SearchLocationData {

    private Long id;

    private Float lat;
    private Float lng;
    private String address;
    private String name;
    private String type;
    private List<String> phones;

    private SearchLocationData(Builder builder) {
        id = builder.id;
        lat = builder.lat;
        lng = builder.lng;
        address = builder.address;
        name = builder.name;
        type = builder.type;
        phones = builder.phones;
    }

    public Long getId() {
        return id;
    }

    public Float getLat() {
        return lat;
    }

    public Float getLng() {
        return lng;
    }

    public String getAddress() {
        return address;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public List<String> getPhones() {
        return phones;
    }

    public static final class Builder {
        private Long id;
        private Float lat;
        private Float lng;
        private String address;
        private String name;
        private String type;
        private List<String> phones;

        public Builder() {
        }

        public Builder id(Long val) {
            id = val;
            return this;
        }

        public Builder lat(Float val) {
            lat = val;
            return this;
        }

        public Builder lng(Float val) {
            lng = val;
            return this;
        }

        public Builder address(String val) {
            address = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder type(String val) {
            type = val;
            return this;
        }

        public Builder phones(List<String> val) {
            phones = val;
            return this;
        }

        public SearchLocationData build() {
            return new SearchLocationData(this);
        }
    }
}
