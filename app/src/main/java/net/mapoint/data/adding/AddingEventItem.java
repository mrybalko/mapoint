package net.mapoint.data.adding;

import net.mapoint.R;
import net.mapoint.data.ToggleItemInterface;
import net.mapoint.ui.base.MapointApp;

import java.util.ArrayList;
import java.util.List;

public enum AddingEventItem implements ToggleItemInterface {

    Event("Event", R.string.event, R.drawable.single_selection_btn_selector),
    Fact("Fact", R.string.fact, R.drawable.single_selection_btn_selector);

    private int labelUIResId;
    private String realValue;
    private int bgResId;

    AddingEventItem(String realValue, int labelResId, int bgResId) {
        this.realValue = realValue;
        labelUIResId = labelResId;
        this.bgResId = bgResId;
    }

    public int labelUIResId() {
        return labelUIResId;
    }

    public String realValue() {
        return realValue;
    }

    public List<Integer> imageResId() {
        return new ArrayList<>();
    }

    public int bgResId() {
        return bgResId;
    }

    public static AddingEventItem byRealValue(final String value) {
        for (AddingEventItem level : values()) {
            if (level.realValue().equals(value)) {
                return level;
            }
        }

        return null;
    }

}
