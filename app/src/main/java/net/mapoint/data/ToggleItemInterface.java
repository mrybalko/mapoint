package net.mapoint.data;

import java.util.List;

public interface ToggleItemInterface {
    int labelUIResId();
    String realValue();
    List<Integer> imageResId();
    int bgResId();
}
