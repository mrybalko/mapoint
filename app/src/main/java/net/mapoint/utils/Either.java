package net.mapoint.utils;


import com.annimon.stream.Optional;
import com.annimon.stream.function.Consumer;

public final class Either<L, R> {
    private final Optional<R> mRight;
    private final Optional<L> mLeft;

    public static <L, R> Either<L, R>  right(final R right) {
        return new Either<>(Optional.empty(), Optional.of(right));
    }

    public static <L, R> Either<L, R> left(final L left) {
        return new Either<>(Optional.of(left), Optional.empty());
    }

    private Either(final Optional<L> left,
                   final Optional<R> right) {

        mRight = right;
        mLeft = left;

        if (!(mRight.isPresent() || mLeft.isPresent())
                || (mRight.isPresent() && mLeft.isPresent())) {
            throw new IllegalArgumentException("must be one of left or right!");
        }
    }

    public Optional<R> right() {
        return mRight;
    }

    public Optional<L> left() {
        return mLeft;
    }

    public boolean isLeft() {
        return mLeft.isPresent();
    }

    public boolean isRight() {
        return mRight.isPresent();
    }

    public <T> Either<T, R> mapLeft(final com.annimon.stream.function.Function<? super L, T> mapper) {
        return new Either<>(left().map(mapper),
                right());
    }

    public <T> Either<L, T> mapRight(final com.annimon.stream.function.Function<? super R, T> mapper) {
        return new Either<>(left(),
                right().map(mapper));
    }

    public <T> T map(final com.annimon.stream.function.Function<? super L, T> left,
                     final com.annimon.stream.function.Function<? super R, T> right) {
        return left()
                .map(disallowNull(left))
                .orElseGet(() -> right()
                        .map(disallowNull(right))
                        .get());
    }

    private <Src, Res> com.annimon.stream.function.Function<Src, Res> disallowNull(final com.annimon.stream.function.Function<Src, Res> slave) {
        return arg -> {
            if (arg == null) throw new NullPointerException("arg can't be null");

            final Res result = slave.apply(arg);

            if (result == null) throw new NullPointerException("result can't be null");

            return result;
        };
    }

    public void apply(final Consumer<? super L> onLeft,
                      final Consumer<? super R> onRight) {
        this.<Runnable>map(left -> () -> onLeft.accept(left),
                right -> () -> onRight.accept(right))
                .run();
    }

    @Override
    public String toString() {
        return map(left -> "Eiter.left(" + left + ")",
                right -> "Eiter.right(" + right + ")");
    }

    @Override
    public boolean equals(final Object other) {
        if (other == this) return true;
        if (!(other instanceof Either)) return false;

        return ((Either<?, ?>)other)
                .map(otherLeft  -> left() .map(ownLeft ->  ownLeft .equals(otherLeft)),
                        otherRight -> right().map(ownRight -> ownRight.equals(otherRight)))
                .orElse(false);
    }

    @Override
    public int hashCode() {
        return map(Object::hashCode,
                Object::hashCode);
    }

}
