package net.mapoint.utils;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.mapoint.R;

import rx.functions.Action0;

public class ToggleItem extends FrameLayout {

    private boolean inited;

    private ImageView iconIV;
    private TextView labelTV;
    private View rootView;

    public ToggleItem(Context context) {
        super(context);
        init(null);
    }

    public ToggleItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public ToggleItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    @TargetApi(21)
    public ToggleItem(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs);
    }

    private void init(final AttributeSet attrs) {
        if (!inited) {
            final LayoutInflater inflater = LayoutInflater.from(getContext());
            rootView = inflater.inflate(R.layout.v_toggle_item, null);
            addView(rootView);
            setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1));

            setPadding(sidePadding(), 0, sidePadding(), 0);
            iconIV = (ImageView) rootView.findViewById(R.id.toggle_image);
            labelTV = (TextView) rootView.findViewById(R.id.toggle_label);

            if (attrs != null) {
                applyStyledAttributes(attrs);
            }

            inited = true;
        }
    }

    public int sidePadding() {
        return 0;
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        iconIV.setEnabled(enabled);
        rootView.setEnabled(enabled);
    }

    public void setBackground(int resId) {
        rootView.setBackgroundResource(resId);
    }

    public void setAction(final Action0 action) {
        rootView.setOnClickListener(view -> action.call());
    }

    public void setImageResource(final int resId){
        iconIV.setImageResource(resId);
    }

    public void setImageDrawable(final Drawable drawable) {
        iconIV.setImageDrawable(drawable);
        iconIV.setVisibility(VISIBLE);
    }

    public void setLabel(final String label) {
        labelTV.setText(label);
        labelTV.setVisibility(VISIBLE);
    }

    private void applyStyledAttributes(final AttributeSet attrs) {
       /* final TypedArray typedArray = getContext().getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.ButtonCenterTextIcon,
                0, 0);

        try {
            final String text = typedArray.getString(R.styleable.ButtonCenterTextIcon_btn_text);
            if (!TextUtils.isEmpty(text)) {
                setText(text);
            }

            final int resId = typedArray.getResourceId(R.styleable.ButtonCenterTextIcon_btn_icon, 0);
            if (resId > 0) {
                setIcon(resId);
            }

            ColorStateList colorStateList = typedArray.getColorStateList(R.styleable.ButtonCenterTextIcon_btn_text_color);
            if(colorStateList != null) {
                setTextColor(colorStateList);
            }

            final int textSize = typedArray.getDimensionPixelSize(R.styleable.ButtonCenterTextIcon_btn_text_size, 0);
            if (textSize > 0) {
                setTextSize(textSize);
            }

        } finally {
            typedArray.recycle();
        }*/
    }
}
