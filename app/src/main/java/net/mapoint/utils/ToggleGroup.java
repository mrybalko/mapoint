package net.mapoint.utils;

import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.ViewGroup;

import net.mapoint.data.ToggleItemInterface;
import net.mapoint.ui.base.MapointApp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import rx.functions.Action0;
import rx.functions.Func1;

public class ToggleGroup<Type extends ToggleItemInterface> {
    private Map<Type, ToggleItem> items;
    private boolean singleSelection;
    @Nullable
    private Action0 onSelectionChangedListener;
    private boolean enabled = true;
    private int itemSidePadding;

    public ToggleGroup() {
        items = new HashMap<>();
    }

    public void updateTypes(final ViewGroup holder,
                            final List<Type> types,
                            final Func1<List<Integer>, Drawable> drawableProvider) {
        items = new HashMap<>();

        for(Type type : types) {
            ToggleItem button = createToggle(type, drawableProvider);
            items.put(type, button);
            holder.addView(button);
        }

        setEnabled(enabled);
    }

    public void updateSelections(final List<Type> selections) {
        for (Type selectionItem : selections) {
            ToggleItem toggleItem = items.get(selectionItem);
            toggleItem.setSelected(toggleItem != null);
        }
    }

    public void updateDisabled(final List<Type> selections) {
        for (Type selectionItem : selections) {
            ToggleItem toggleItem = items.get(selectionItem);
            toggleItem.setEnabled(toggleItem == null);
        }
    }

    public void setEnabled(final boolean enabled) {
        this.enabled = enabled;
        Set<Type> keys = items.keySet();
        for (Type key : keys) {
            ToggleItem toggleItem = items.get(key);
            if (toggleItem != null) {
                toggleItem.setEnabled(enabled);
            }
        }
    }

    <Type extends ToggleItemInterface> ToggleItem createToggle(final Type item,
                                                               final Func1<List<Integer>, Drawable> drawableProvider) {

        ToggleItem toggleItem = new ToggleItem(MapointApp.getContext()) {

            @Override
            public int sidePadding() {
                return itemSidePadding();
            }
        };

        List<Integer> imageResIds = item.imageResId();
        if (imageResIds.isEmpty()) {
            toggleItem.setLabel(getString(item.labelUIResId()));
        } else {
            toggleItem.setImageDrawable(drawableProvider.call(imageResIds));
        }

        toggleItem.setBackground(item.bgResId());

        toggleItem.setAction(() -> {
            boolean isSelected = toggleItem.isSelected();

            if (!singleSelection || !isSelected) {
                toggleItem.requestFocus();
                onChecked(toggleItem, isSelected);
                if (onSelectionChangedListener != null) {
                    onSelectionChangedListener.call();
                }
            }
        });
        return toggleItem;
    }

    private String getString(int resId) {
        return MapointApp.getContext().getResources().getString(resId);
    }

    public void setOnSelectionChangedListener(@NonNull final Action0 onSelectionChangedListener) {
        this.onSelectionChangedListener = onSelectionChangedListener;
    }

    private void onChecked(final ToggleItem toggleItem, boolean isChecked) {
        if (singleSelection) {
            resetAllChecked();
            toggleItem.setSelected(true);
        } else {
            toggleItem.setSelected(!isChecked);
        }
    }

    public void resetAllChecked() {
        Set<Type> keys = items.keySet();
        for (Type key : keys) {
            ToggleItem toggleItem = items.get(key);
            if (toggleItem != null) {
                toggleItem.setSelected(false);
            }
        }
    }

    public void setSingleSelection(final boolean singleSelection) {
        this.singleSelection = singleSelection;
    }

    public List<Type> getAllSelections() {
        List<Type> allSelections = new ArrayList<Type>();
        Set<Type> keys = items.keySet();
        for (Type key : keys) {
            ToggleItem toggleItem = items.get(key);
            if (toggleItem != null && toggleItem.isSelected()) {
                allSelections.add(key);
            }
        }
        return allSelections;
    }

    public Type getSingleSelection() {
        Type selection = null;
        List<Type> allSelections = getAllSelections();
        if (allSelections.size() > 0) {
            selection = allSelections.get(0);
        }

        return selection;
    }

    public int itemSidePadding() {
        return itemSidePadding;
    }

    public void itemSidePadding(int itemSidePadding) {
        this.itemSidePadding = itemSidePadding;
    }
}
