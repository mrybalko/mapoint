package net.mapoint.utils.http;

import android.text.TextUtils;

import net.mapoint.utils.storage.SharedPreferencesProvider;

import java.io.IOException;
import java.util.Locale;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class LangInterceptor implements Interceptor {

    private SharedPreferencesProvider sharedPreferencesProvider;

    public LangInterceptor(SharedPreferencesProvider preferencesProvider) {
        this.sharedPreferencesProvider = preferencesProvider;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        String langValue = sharedPreferencesProvider.languageItem();
        if (TextUtils.isEmpty(langValue)) {
            langValue = Locale.getDefault().getLanguage();
        }
        HttpUrl url = request.url().newBuilder().addQueryParameter("lang",langValue).build();
        request = request.newBuilder().url(url).build();
        return chain.proceed(request);
    }
}