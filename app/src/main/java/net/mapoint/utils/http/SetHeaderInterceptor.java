package net.mapoint.utils.http;

import android.support.annotation.NonNull;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class SetHeaderInterceptor implements Interceptor {

    @NonNull private final String headerName;
    @NonNull private final String headerValue;

    public SetHeaderInterceptor(@NonNull final String header, @NonNull final String value) {
        headerName = header;
        headerValue = value;
    }

    @Override public Response intercept(@NonNull final Interceptor.Chain chain) throws IOException {
        final Request original = chain.request();
        final Request requestWithHeader = original.newBuilder()
                .header(headerName, headerValue)
                .build();
        return chain.proceed(requestWithHeader);
    }
}