package net.mapoint.utils;

public class Constants {

    public static final int PLAY_SERVICES_REQUEST = 1001;
    public static final int REQUEST_LOCATION_PERMISSION = 1002;
    public static final int REQUEST_CALL_PERMISSION = 1003;

    public static final String NOTIFICATION_CHANNEL_ID_EVENTS = "net.mapoint.channels.events";

    public static final String START_JOB_BROADCAST_RECEIVER = "net.mapoint.utils.intent.action.START_JOB_FIRSTTIME";

    public static final long SHOW_NOTIFICATION_FILTER_INTERVAL = 10 * 60 * 1000;
    public static final long TIME_INTERVAL_FOR_CHECKING_LOCATION = 15 * 60 * 1000;

    public static final long LOCATION_UPDATE_INTERVAL_IN_MILLISECONDS_FOREGROUND = 15 * 1000;
    public static final long LOCATION_UPDATE_INTERVAL_IN_MILLISECONDS_BACKGROUND = 1 * 1000;
    public static final int LOCATION_UPDATE_DISTANCE_IN_METERS = 500;

    public static final int NOTIFICATION_ID_ON_JOB_TEST = 1;
    public static final int NOTIFICATION_ID_ON_LOCATION_CHANGED = 2;
    public static final int NOTIFICATION_ID_CHECK_CAN_SHOW_NOTIFICAION = 3;
    public static final int NOTIFICATION_ID_CAN_SHOW_NOTIFICAION = 4;
    public static final int NOTIFICATION_ID_FIND_EVENT_DATA = 5;
    public static final int NOTIFICATION_ID_MAIN = 12345678;


    public static final int REQUEST_CODE_ADD_PARAMETER = 2001;
    public static final int REQUEST_CODE_ADD_PHONE = 2002;
    public static final int REQUEST_CODE_ADD_DATE_TIME = 2003;
    public static final int REQUEST_CODE_ADD_CATEGORY = 2004;
}
