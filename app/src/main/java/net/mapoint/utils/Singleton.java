package net.mapoint.utils;

import android.os.Looper;

import net.mapoint.BuildConfig;

public final class Singleton<T> {
    private final Supplier<T> factory;
    private T instance;

    public Singleton(final Supplier<T> factory) {
        if (factory == null) {
            throw new NullPointerException("Factory must not be null");
        }

        this.factory = factory;
    }

    public T instance() {
        if (BuildConfig.DEBUG && Thread.currentThread() != Looper.getMainLooper().getThread()) {
            throw new AssertionError("Not ui thread");
        }
        if (instance == null) {
            instance = factory.get();
        }

        return instance;
    }

    public interface Supplier<T> {
        T get();
    }
}
