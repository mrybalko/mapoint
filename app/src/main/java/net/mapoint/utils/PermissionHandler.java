package net.mapoint.utils;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.util.Pair;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import rx.functions.Action0;

public class PermissionHandler {

    private static Singleton<PermissionHandler> _instanceHolder = new Singleton<>(PermissionHandler::new);

    private PermissionHandler() {
        permissionsInProcess = new HashMap<>();
    }

    /*
            key - request code, value - pair<permission, action>
      */
    private Map<Integer, Pair<String, Action0>> permissionsInProcess;

    public static PermissionHandler instance() {
        return _instanceHolder.instance();
    }

    public void requestWithChecking(final Activity activity,
                                    final String permission,
                                    final int requestCode,
                                    final Action0 action,
                                    final Action0 showPermissionRationaleAction) {

        if (checkPermission(activity, permission)) {
            action.call();
        } else {
            permissionsInProcess.put(requestCode, new Pair<>(permission, action));

            requestPermissions(activity,
                    permission,
                    requestCode,
                    showPermissionRationaleAction);
        }
    }

    private boolean checkPermission(final Context context, final String permission) {
        return ActivityCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
    }

    public void handlePermissionRequestResult(int requestCode,
                                              String permissions[],
                                              int[] grantResults) {

        Set<Integer> keys = permissionsInProcess.keySet();

        for (Integer key : keys) {
            if (requestCode == key) {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                    Pair<String, Action0> pair = permissionsInProcess.get(key);
                    permissionsInProcess.remove(key);
                    if (pair != null && pair.second != null) {
                        pair.second.call();
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
            }
        }
    }

    private void requestPermissions(
            final Activity activity,
            final String permission,
            final int requestPermissionCode,
            final Action0 showPermissionRationaleAction) {

        if (ActivityCompat.shouldShowRequestPermissionRationale(
                activity,
                permission)) {

            showPermissionRationaleAction.call();

            // Show an explanation to the user *asynchronously* -- don't block
            // this thread waiting for the user's response! After the user
            // sees the explanation, try again to request the permission.

        } else {

            // No explanation needed, we can request the permission.

            ActivityCompat.requestPermissions(activity,
                    new String[]{permission},
                    requestPermissionCode);
        }
    }
}
