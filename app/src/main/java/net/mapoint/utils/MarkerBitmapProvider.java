package net.mapoint.utils;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.AppCompatDrawableManager;

import net.mapoint.R;
import net.mapoint.ui.base.MapointApp;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MarkerBitmapProvider {

    private static Singleton<MarkerBitmapProvider> _instanceHolder = new Singleton<>(MarkerBitmapProvider::new);

    private static Map<Integer, Bitmap> coloredBitmaps;
    private static Context context;

    private MarkerBitmapProvider() {
    }

    public void init() {
        context = MapointApp.getContext();
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        coloredBitmaps = new HashMap<>();
    }

    public static MarkerBitmapProvider instance() {
        return _instanceHolder.instance();
    }

    public void recreateBitmapsByColors(List<Integer> colors) {
        coloredBitmaps = new HashMap<>();

        for (Integer color: colors) {
            Bitmap bitmap = getBitmap(R.drawable.map_marker_monochrome_circle, color);
            coloredBitmaps.put(color, bitmap);
        }
    }

    public static Bitmap getBitmapByColor(Integer color) {

        Bitmap bitmap = coloredBitmaps.get(color);

        if (bitmap == null) {
            bitmap = getBitmap(R.drawable.map_marker_colored_circle);
        }

        return bitmap;
    }

    private static Bitmap getBitmap(int drawableId) {
        return getBitmap(drawableId, 0);
    }

    private static Bitmap getBitmap(int drawableId, int color) {
        Drawable drawable = ContextCompat.getDrawable(context, drawableId);
        if (color != 0) {
            drawable.setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC_IN));
        }

        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        } else if (drawable instanceof VectorDrawable) {
            return getBitmap((VectorDrawable) drawable);
        } else {
            throw new IllegalArgumentException("unsupported drawable type");
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private static Bitmap getBitmap(VectorDrawable vectorDrawable) {
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(),
                vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        vectorDrawable.draw(canvas);
        return bitmap;
    }

    @SuppressLint("RestrictedApi")
    public static Bitmap getBitmapFromVectorDrawable(int drawableId) {
        Drawable drawable = AppCompatDrawableManager.get().getDrawable(context, drawableId);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            drawable = (DrawableCompat.wrap(drawable)).mutate();
        }
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }
}
