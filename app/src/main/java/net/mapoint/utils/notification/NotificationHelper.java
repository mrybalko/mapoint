package net.mapoint.utils.notification;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.location.Location;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import com.flurry.android.FlurryAgent;

import net.mapoint.R;
import net.mapoint.analytics.BaseAnalyticsTracker;
import net.mapoint.data.locations.EventData;
import net.mapoint.data.locations.SimpleLocation;
import net.mapoint.ui.base.MapointApp;
import net.mapoint.ui.event.EventWithDetailsActivity;
import net.mapoint.ui.main.location.LocationUtils;
import net.mapoint.utils.Constants;
import net.mapoint.utils.NotificationDismissedReceiver;
import net.mapoint.utils.storage.SharedPreferencesProvider;

import rx.functions.Func1;

import static android.content.Context.NOTIFICATION_SERVICE;

public class NotificationHelper {

    private Context context;
    private Resources resources;
    private NotificationManager notificationManager;

    public NotificationHelper(Context context) {
        this.context = context;
        notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        resources = context.getResources();
    }

    @TargetApi(Build.VERSION_CODES.O)
    public NotificationChannel createNotificationChannelEvents(final boolean isNotificationSoundEnabled) {

        NotificationChannel notificationChannel = new NotificationChannel(
                Constants.NOTIFICATION_CHANNEL_ID_EVENTS,
                resources.getString(R.string.notification_channels_name_events),
                notificationManager.IMPORTANCE_DEFAULT);

        notificationChannel.setShowBadge(true);

        if(isNotificationSoundEnabled) {
            notificationChannel.setSound(soundUri(),
                    new AudioAttributes.Builder()
                            .setUsage(AudioAttributes.USAGE_NOTIFICATION_RINGTONE)
                            .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                            .build());
        }

        return notificationChannel;
    }

    private Uri soundUri() {
        return Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.coin_sound);
    }

    private Notification createNotification(EventData eventData,
                                            boolean isSoundEnabled,
                                            Func1<Integer, String> stringResourceProvider) {

        String notificationEventId = eventData.getId();

        Intent activityIntent = EventWithDetailsActivity.newIntent(context, eventData, notificationEventId);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);

        stackBuilder.addParentStack(EventWithDetailsActivity.class);
        stackBuilder.addNextIntent(activityIntent);
        PendingIntent activityPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent receiverIntent = NotificationDismissedReceiver.newIntent(notificationEventId, eventData.eventType());
        PendingIntent deletedEventIntent =
                PendingIntent.getBroadcast(MapointApp.getContext(), 0, receiverIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        int iconResId = android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP ?
                R.drawable.ic_stat_notify : R.mipmap.ic_launcher;

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder
                .setContentText(eventData.getText())
                .setContentTitle(LocationUtils.eventDataTitle(eventData, stringResourceProvider))
                .setSmallIcon(iconResId)
                .setContentIntent(activityPendingIntent)
                .setDeleteIntent(deletedEventIntent)
                .setAutoCancel(true);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            builder.setChannelId(Constants.NOTIFICATION_CHANNEL_ID_EVENTS);
        } else {
            if (isSoundEnabled) {
                Uri sound = soundUri();
                builder.setSound(sound);
            }
        }

        return builder.build();
    }

    public void updateNotification(final EventData eventData,
                                   final boolean isNotificationSoundEnabled,
                                   final Func1<Integer, String> stringResourceProvider) {

        updateNotification(Constants.NOTIFICATION_ID_MAIN,
                eventData,
                isNotificationSoundEnabled,
                stringResourceProvider);
    }

    public void updateNotification(final int notificationId,
                                   final EventData eventData,
                                   final boolean isNotificationSoundEnabled,
                                   final Func1<Integer, String> stringResourceProvider) {
        if (eventData != null) {
            String eventDataId = eventData.getId();

            SharedPreferencesProvider sharedPreferencesProvider = SharedPreferencesProvider.instance();
            String shownNotificationEventId = sharedPreferencesProvider.shownNotificationEventId();

            if (!eventDataId.equals(shownNotificationEventId)) {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    notificationManager.createNotificationChannel(createNotificationChannelEvents(isNotificationSoundEnabled));
                }
                notificationManager.notify(notificationId, createNotification(eventData,
                        isNotificationSoundEnabled,
                        stringResourceProvider));

                FlurryAgent.onStartSession(context);
                BaseAnalyticsTracker baseAnalyticsTracker = BaseAnalyticsTracker.instance();
                if (shownNotificationEventId != null) {
                    baseAnalyticsTracker.onNotificationEventRecreated(shownNotificationEventId, eventDataId);
                }
                sharedPreferencesProvider.shownNotificationEventId(eventDataId);

                baseAnalyticsTracker.onNotificationEventReceived(eventDataId, eventData.eventType());
                FlurryAgent.onEndSession(context);
            }
        }
    }

    public boolean needShowNotification(final Location newLocation,
                                        final SimpleLocation lastLocation,
                                        final long lastNotificationTime,
                                        final int minDistance) {
        long notificationTimeInterval = Constants.SHOW_NOTIFICATION_FILTER_INTERVAL;

        boolean isLocationChanged = isLocationChanged(newLocation,
                lastLocation,
                minDistance);

        boolean isTimeForNewNotification = System.currentTimeMillis() - lastNotificationTime > notificationTimeInterval;

        return isTimeForNewNotification && isLocationChanged;
    }

    public boolean isLocationChanged(final Location newLocation,
                                      final SimpleLocation lastLocation,
                                      final int minDistance) {
        boolean isLocationChanged = true;
        if (lastLocation != null) {

            Location lastLoc = new Location("");
            lastLoc.setLatitude(lastLocation.getLat());
            lastLoc.setLongitude(lastLocation.getLon());

            float distance = newLocation.distanceTo(lastLoc);

            isLocationChanged = minDistance > 0 && distance > minDistance;
        }

        return isLocationChanged;
    }
}
