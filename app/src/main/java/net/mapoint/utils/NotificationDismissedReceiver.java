package net.mapoint.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.flurry.android.FlurryAgent;

import net.mapoint.analytics.BaseAnalyticsTracker;
import net.mapoint.data.locations.EventType;
import net.mapoint.managers.LoggingManager;
import net.mapoint.ui.base.MapointApp;
import net.mapoint.utils.storage.SharedPreferencesProvider;

public class NotificationDismissedReceiver extends BroadcastReceiver {

    public static final String DISMISS_NOTIFICATION_ID = "dismissed_notification_id";
    public static final String DISMISS_NOTIFICATION_TYPE = "dismissed_notification_type";

    public static Intent newIntent(final String eventId, final EventType eventType) {
        Intent intent = new Intent(MapointApp.getContext(), NotificationDismissedReceiver.class);
        intent.putExtra(DISMISS_NOTIFICATION_ID, eventId);
        intent.putExtra(DISMISS_NOTIFICATION_TYPE, eventType.getValue());
        return intent;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String eventId = intent.getExtras().getString(DISMISS_NOTIFICATION_ID, null);
        String eventType = intent.getExtras().getString(DISMISS_NOTIFICATION_TYPE, null);

        if (!TextUtils.isEmpty(eventId)) {
            SharedPreferencesProvider sharedPreferencesProvider = SharedPreferencesProvider.instance();
            sharedPreferencesProvider.addSeenEvents(String.valueOf(eventId));
            sharedPreferencesProvider.shownNotificationEventId(null);

            LoggingManager.instance().logMessage("on dismiss : " + eventId);
            FlurryAgent.onStartSession(context);
            BaseAnalyticsTracker.instance().onNotificationEventDismissed(eventId, EventType.byValue(eventType));
            FlurryAgent.onEndSession(context);
        }
    }
}