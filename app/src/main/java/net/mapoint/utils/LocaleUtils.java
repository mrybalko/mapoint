package net.mapoint.utils;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;

import net.mapoint.ui.base.MapointApp;

import java.util.Locale;

public class LocaleUtils {

    public static Locale getCurrentLocale(){
        Context context = MapointApp.getContext();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
            return context.getResources().getConfiguration().getLocales().get(0);
        } else{
            //noinspection deprecation
            return context.getResources().getConfiguration().locale;
        }
    }

    public static void changeAppLanguage(String languageCode) {
        Locale locale = new Locale(languageCode);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        Resources resources = MapointApp.getContext().getResources();
        resources.updateConfiguration(config, resources.getDisplayMetrics());
    }

}
