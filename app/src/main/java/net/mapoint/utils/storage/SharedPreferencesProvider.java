package net.mapoint.utils.storage;

import android.content.Context;
import android.content.SharedPreferences;

import net.mapoint.ui.base.MapointApp;
import net.mapoint.utils.Singleton;

import java.util.HashSet;
import java.util.Set;

public class SharedPreferencesProvider {

    private final static String SHARED_PREFERENCES_KEY = "SPProvider";
    private final static String KEY_REQUESTING_LOCATION_UPDATES = "requesting_location_updates_key";
    private final static String KEY_LOCATION_UPDATES_SERVICE_IS_FOREGROUND = "location_updates";
    private final static String KEY_SELECTED_CATEGORIES = "selected_categories";
    private final static String KEY_FACTS_ENABLED = "facts_enabled";

    private final static String KEY_NOTIFICATION_SOUND_ENABLED = "notification_sound_enabled";

    private final static String KEY_SELECTED_RADIUS = "selected_radius";
    private final static String KEY_PERIOD_ITEM = "period_item";
    private final static String KEY_LANGUAGE_ITEM = "language_item";

    private final static String KEY_LAST_LOCATION = "last_location";
    private final static String KEY_LAST_NOTIFICATION_TIME = "last_notification_time";

    private final static String KEY_SEEN_EVENTS = "seen_events";
    private final static String KEY_CATEGORIES_CHANGED = "categories_changed";

    private final static String NOTIFICATION_EVENT_SHOWN = "notification_event_shown";

    private static SharedPreferences sharedPreferences;

    private static Singleton<SharedPreferencesProvider> _instanceHolder = new Singleton<>(SharedPreferencesProvider::new);

    public static SharedPreferencesProvider instance() {
        return _instanceHolder.instance();
    }

    private SharedPreferencesProvider() {
        sharedPreferences = MapointApp.getContext().getSharedPreferences(SHARED_PREFERENCES_KEY, Context.MODE_PRIVATE);
    }

    public boolean requestingLocationUpdates() {
        return sharedPreferences.getBoolean(KEY_REQUESTING_LOCATION_UPDATES, false);
    }

    public void setRequestingLocationUpdates(boolean requestingLocationUpdates) {
        sharedPreferences
                .edit()
                .putBoolean(KEY_REQUESTING_LOCATION_UPDATES, requestingLocationUpdates)
                .apply();
    }

    public boolean requestingLocationUpdatesServiceIsForeground() {
        return sharedPreferences.getBoolean(KEY_LOCATION_UPDATES_SERVICE_IS_FOREGROUND, false);
    }

    public void setRequestingLocationUpdatesServiceIsForeground(boolean requestingLocationUpdates) {
        sharedPreferences
                .edit()
                .putBoolean(KEY_LOCATION_UPDATES_SERVICE_IS_FOREGROUND, requestingLocationUpdates)
                .apply();
    }

    public void saveSelectedCategories(Set<String> selectedCategories) {
        sharedPreferences
                .edit()
                .putStringSet(KEY_SELECTED_CATEGORIES, selectedCategories)
                .apply();
    }

    public Set<String> readSelectedCategories() {
        return sharedPreferences.getStringSet(KEY_SELECTED_CATEGORIES, new HashSet<>());
    }

    public void saveSelectedRadius(String selectedRadius) {
        sharedPreferences
                .edit()
                .putString(KEY_SELECTED_RADIUS, selectedRadius)
                .apply();
    }

    public String readSelectedRadius(String defaultValue) {
        return sharedPreferences.getString(KEY_SELECTED_RADIUS, defaultValue);
    }

    public boolean readFactEnabledFlag() {
        return sharedPreferences.getBoolean(KEY_FACTS_ENABLED, false);
    }

    public void updateFactEnabledFlag(boolean interval) {
        sharedPreferences
                .edit()
                .putBoolean(KEY_FACTS_ENABLED, interval)
                .apply();
    }

    public boolean notificationSoundEnabled() {
        return true;//sharedPreferences.getBoolean(KEY_NOTIFICATION_SOUND_ENABLED, true);
    }

    public void updateNotificationSoundEnabled(boolean isEnabled) {
        sharedPreferences
                .edit()
                .putBoolean(KEY_NOTIFICATION_SOUND_ENABLED, isEnabled)
                .apply();
    }

    public long lastNotificationTime() {
        return sharedPreferences.getLong(KEY_LAST_NOTIFICATION_TIME, 0);
    }

    public void updateLastNotificationTime(long time) {
        sharedPreferences
                .edit()
                .putLong(KEY_LAST_NOTIFICATION_TIME, time)
                .apply();
    }

    public String lastLocation() {
        return sharedPreferences.getString(KEY_LAST_LOCATION, "");
    }

    public void updateLastLocation(String lastLocation) {
        sharedPreferences
                .edit()
                .putString(KEY_LAST_LOCATION, lastLocation)
                .apply();
    }

    public String periodItem() {
        return sharedPreferences.getString(KEY_PERIOD_ITEM, "");
    }

    public void updatePeriodItem(String periodItemValue) {
        sharedPreferences
                .edit()
                .putString(KEY_PERIOD_ITEM, periodItemValue)
                .apply();
    }

    public String languageItem() {
        return sharedPreferences.getString(KEY_LANGUAGE_ITEM, "");
    }

    public void updateLanguageItem(String languageItemValue) {
        sharedPreferences
                .edit()
                .putString(KEY_LANGUAGE_ITEM, languageItemValue)
                .apply();
    }


    public Set<String> seenEvents() {
        return sharedPreferences.getStringSet(KEY_SEEN_EVENTS, new HashSet<>());
    }

    public void addSeenEvents(String seenEvent) {
        Set<String> seenEvents = seenEvents();
        seenEvents.add(seenEvent);

        sharedPreferences
                .edit()
                .putStringSet(KEY_SEEN_EVENTS, seenEvents)
                .apply();
    }

    public void saveNumber(int number) {

        //fake, fix for save selected categories
        sharedPreferences
                .edit()
                .putInt("number", number)
                .apply();
    }

    public Boolean categoriesChanged() {
        return sharedPreferences.getBoolean(KEY_CATEGORIES_CHANGED, false);
    }

    public void categoriesChanged(Boolean changed) {

        sharedPreferences
                .edit()
                .putBoolean(KEY_CATEGORIES_CHANGED, changed)
                .apply();
    }

    public void shownNotificationEventId(String eventId) {
        sharedPreferences
                .edit()
                .putString(NOTIFICATION_EVENT_SHOWN, eventId)
                .apply();
    }

    public String shownNotificationEventId() {
        return sharedPreferences.getString(NOTIFICATION_EVENT_SHOWN, null);
    }
}
