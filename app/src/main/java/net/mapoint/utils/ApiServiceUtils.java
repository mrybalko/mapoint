package net.mapoint.utils;

import com.annimon.stream.Optional;
import com.annimon.stream.function.Function;

import okhttp3.ResponseBody;
import retrofit2.Response;

public class ApiServiceUtils {

    public static <JsonType, ResponseType> Function<Response<JsonType>, Optional<ResponseType>> for200code(final Function<JsonType, ResponseType> converter) {
        return retrofitResponse -> {
            if (retrofitResponse.code() == 200 && retrofitResponse.body() != null) {
                return Optional.ofNullable(converter.apply(retrofitResponse.body()));
            } else {
                return Optional.empty();
            }
        };
    }

    public static <JsonType, ErrorType> Function<Response<JsonType>, Optional<ErrorType>> for400code(final Function<ResponseBody, ErrorType> converter) {
        return response -> {
            if (response.code() == 400 && response.errorBody() != null) {
                return Optional.ofNullable(converter.apply(response.errorBody()));
            } else {
                return Optional.empty();
            }
        };
    }

    public static <JsonType, ErrorType> Function<Response<JsonType>, Optional<ErrorType>> for500code(final Function<ResponseBody, ErrorType> converter) {
        return response -> {
            if (response.code() == 500 && response.errorBody() != null) {
                return Optional.ofNullable(converter.apply(response.errorBody()));
            } else {
                return Optional.empty();
            }
        };
    }
}