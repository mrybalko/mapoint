package net.mapoint.utils.data;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class PhonesUtils {

    public static String phonesToString(Set<String> phones) {

        StringBuilder stringBuilder = new StringBuilder();

        for(String phone: phones) {
            if (stringBuilder.length() > 0) {
                stringBuilder.append("\n");
            }

            stringBuilder.append(phone);
        }

        return stringBuilder.toString();
    }

    public static Set<String> stringToPhones(String phones) {

        String[] phonesStrings = phones.split("\n");
        Set<String> phonesSet = new HashSet<>(Arrays.asList(phonesStrings));
        phonesSet.remove("");
        return phonesSet;
    }
}
