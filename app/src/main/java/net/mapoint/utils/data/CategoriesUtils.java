package net.mapoint.utils.data;

import net.mapoint.data.category.CategoryData;

import java.util.List;

import rx.functions.Func1;

public class CategoriesUtils {

    public static boolean isSomeSubcategoriesSelected(List<CategoryData> subcategories){
        for (CategoryData subcategoryItem : subcategories) {
            if (subcategoryItem.isSelected()) {
                return true;
            }
        }

        return false;
    }

    public static boolean isAllSelected(List<CategoryData> subcategories, Func1<String, Boolean> isSelectedProvider) {

        boolean isAllSelected = true;
        for (CategoryData subcategoryItem : subcategories) {
            if (!isSelectedProvider.call(subcategoryItem.getId())) {
                isAllSelected = false;
                break;
            }
        }

        return isAllSelected;
    }
}
