package net.mapoint.utils;

import android.util.Log;

import com.crashlytics.android.Crashlytics;

import net.mapoint.BuildConfig;

import java.util.Map;
import java.util.Set;

public class LogUtils {

    private final static boolean ENABLE_DEBUG_LOG = BuildConfig.DEBUG;
    private final static boolean ENABLE_DEBUG_LOG_CRASHLYTIC = false;

    public static void log(final String message) {
        if (ENABLE_DEBUG_LOG) {
            log("TESTING", message);
        }
        logCrashlytics(message);
    }

    public static void log(final String tag, final String message) {
        if (ENABLE_DEBUG_LOG) {
            Log.d(tag, message);
        }
        logCrashlytics(message);
    }

    public static void logWithMap(final String tag,  final String message, final Map<String, String> map) {
        if (ENABLE_DEBUG_LOG) {
            Log.d(tag, message);

            Set<String> keys = map.keySet();
            StringBuilder stringBuilder = new StringBuilder();
            for (String key : keys) {

                if (stringBuilder.length() > 0) {
                    stringBuilder.append(", ");
                }

                stringBuilder.append(key);
                stringBuilder.append(" : ");
                stringBuilder.append(map.get(key));
            }

            Log.d(tag, message + " : { " + stringBuilder.toString() + " }");
        }
        logCrashlytics(message);
    }

    public static void log(final String tag, final String message, final Throwable throwable) {
        if (ENABLE_DEBUG_LOG) {
            Log.d(tag, message, throwable);
        }
        logCrashlytics(message);
    }

    private static void logCrashlytics(String message) {
        if (ENABLE_DEBUG_LOG_CRASHLYTIC) {
            Crashlytics.log(message);
        }
    }

    public static String setToString(Set<String> set) {
        StringBuilder stringBuilder = new StringBuilder();
        for(String event : set) {
            if (stringBuilder.length() > 0) {
                stringBuilder.append(", ");
            }

            stringBuilder.append(event);
        }

        return stringBuilder.toString();
    }
}
