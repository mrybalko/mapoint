package net.mapoint.utils.list;

public interface ItemLayoutProvider {
    int layoutId();
}