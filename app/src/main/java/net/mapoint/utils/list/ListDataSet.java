package net.mapoint.utils.list;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.List;

public class ListDataSet<Type> implements DataSet<Type> {

    @NonNull
    private final List<Type> list;

    public ListDataSet(@NonNull final List<Type> list) {
        this.list = list;
    }

    @Nullable
    @Override
    public Type getItem(final int position) {
        return list.get(position);
    }

    @NonNull
    public List<Type> data() {
        return list;
    }

    @Override
    public int size() {
        return list.size();
    }
}
