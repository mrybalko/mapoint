package net.mapoint.utils.list;

import java.util.List;

public interface DataSet<Type> {
    Type getItem(final int position);
    int size();
    List<Type> data();
}