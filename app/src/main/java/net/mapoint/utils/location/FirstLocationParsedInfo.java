package net.mapoint.utils.location;

import net.mapoint.data.locations.EventType;

import java.util.List;

public class FirstLocationParsedInfo {

    private String id;
    private String title;
    private String displayedText;
    private EventType locationType;
    private List<String> subcategories;

    private FirstLocationParsedInfo(Builder builder) {
        title = builder.title;
        displayedText = builder.displayedText;
        locationType = builder.locationType;
        id = builder.id;
        subcategories = builder.subcategories;
    }

    public String title() {
        return title;
    }

    public String displayedText() {
        return displayedText;
    }

    public EventType locationType() {
        return locationType;
    }

    public String id() {
        return id;
    }

    public List<String> subcategories() {
        return subcategories;
    }

    public static final class Builder {
        private String id;
        private String title;
        private String displayedText;
        private EventType locationType;
        private List<String> subcategories;

        public Builder() {
            id = "";
            title = "";
            displayedText = "";
            locationType = EventType.Unknown;

        }

        public Builder title(String val) {
            title = val;
            return this;
        }

        public Builder displayedText(String val) {
            displayedText = val;
            return this;
        }

        public Builder eventType(final EventType locationType) {
            this.locationType = locationType;
            return this;
        }

        public Builder id(final String id) {
            this.id = id;
            return this;
        }

        public Builder subcategories(final List<String> subcategories) {
            this.subcategories = subcategories;
            return this;
        }

        public FirstLocationParsedInfo build() {
            return new FirstLocationParsedInfo(this);
        }
    }
}
