package net.mapoint.utils.location;

import net.mapoint.data.locations.EventData;
import net.mapoint.data.locations.LocationData;

import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

import rx.functions.Func1;

public class LocationDataParser {

    private LocationData locationData;

    public LocationDataParser(final LocationData locationData) {
        this.locationData = locationData;
    }

    public String id () {
        return locationData.id();
    }

    public double lat() {
        return locationData.lat();
    }

    public double lng() {
        return locationData.lng();
    }

    public String address() {
        return locationData.address();
    }

    public String name() {
        return locationData.name();
    }

    public String type() {
        return locationData.type();
    }

    public List<EventData> facts() {
        return locationData.facts();
    }

    public List<EventData> offers() {
        return locationData.offers();
    }

    @Nullable
    public EventData firstCorrectEvent(final Func1<Integer, String> resourceStringProvider) {
        return firstCorrectEvent(null, resourceStringProvider);
    }

    public List<EventData> allEvents() {
        List<EventData> eventsData = new ArrayList<>();
        eventsData.addAll(offers());
        eventsData.addAll(facts());

        return eventsData;
    }

    @Nullable
    public EventData firstCorrectEvent(final Func1<String, Boolean> eventIdFilter,
                                       final Func1<Integer, String> resourceStringProvider) {

        List<EventData> eventsData = new ArrayList<EventData>();
        eventsData.addAll(offers());
        eventsData.addAll(facts());

        for (EventData eventData : eventsData) {
            if (eventIdFilter == null || eventIdFilter.call(eventData.getId())) {
                return eventData;
            }
        }

        return null;
    }
}
