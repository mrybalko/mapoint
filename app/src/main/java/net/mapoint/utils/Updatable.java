package net.mapoint.utils;

public interface Updatable {
    void update();
}