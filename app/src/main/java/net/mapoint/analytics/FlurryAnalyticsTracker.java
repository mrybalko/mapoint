package net.mapoint.analytics;

import android.support.v4.util.Pair;

import com.flurry.android.FlurryAgent;

import net.mapoint.data.PeriodItem;
import net.mapoint.data.locations.EventType;
import net.mapoint.utils.LogUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static net.mapoint.analytics.AnalyticsTrackerConstants.*;

public class FlurryAnalyticsTracker implements AnalyticsTrackerInterface {

    private final String TAG = "FlurryAnalyticsTracker";

    @Override
    public void onNotificationEventReceived(String eventId, EventType eventType) {
        logEvent(NOTIFICATION_EVENT_RECEIVED,
                params(
                        new Pair<>(EVENT_ID, eventId),
                        new Pair<>(EVENT_TYPE, eventType.getValue())
                ));
    }

    @Override
    public void onNotificationEventClicked(String eventId, EventType eventType) {
        logEvent(NOTIFICATION_EVENT_CLICKED,
                params(
                        new Pair<>(EVENT_ID, eventId),
                        new Pair<>(EVENT_TYPE, eventType.getValue())
                ));
    }

    @Override
    public void onNotificationEventDismissed(String eventId, EventType eventType) {
        logEvent(NOTIFICATION_EVENT_DISMISSED,
                params(
                        new Pair<>(EVENT_ID, eventId),
                        new Pair<>(EVENT_TYPE, eventType.getValue())
                ));
    }

    @Override
    public void onNotificationEventRecreated(String oldEventId, String newEventId) {
        logEvent(NOTIFICATION_EVENT_RECREATED,
                params(
                        new Pair<>(OLD_EVENT_ID, oldEventId),
                        new Pair<>(NEW_EVENT_ID, newEventId)));
    }

    @Override
    public void onToEventFromCommonList(String eventId, EventType eventType) {
        logEvent(TO_EVENT_FROM_COMMON_LIST,
                params(new Pair<>(EVENT_ID, eventId),
                        new Pair<>(EVENT_TYPE, eventType.getValue())));
    }

    @Override
    public void onToLocationFromMap(String locationId) {
        logEvent(TO_LOCATION_FROM_MAP,
                params(new Pair<>(LOCATION_ID, locationId)));
    }

    @Override
    public void onToEventFromLocation(String eventId, EventType eventType) {
        logEvent(TO_EVENT_FROM_LOCATION,
                params(new Pair<>(EVENT_ID, eventId),
                        new Pair<>(EVENT_TYPE, eventType.getValue())));
    }

    @Override
    public void onLocationCallClicked(String locationId) {
        logEvent(LOCATION_CALL_CLICKED,
                params(new Pair<>(LOCATION_ID, locationId)));
    }

    @Override
    public void onEventLinkClicked(String eventId, EventType eventType) {
        logEvent(EVENT_LINK_CLICKED,
                params(new Pair<>(EVENT_ID, eventId),
                        new Pair<>(EVENT_TYPE, eventType.getValue())));
    }

    @Override
    public void onFiltersTimeChanged(PeriodItem periodItem) {
        logEvent(FILTERS_TIME_CHANGED,
                params(new Pair<>(TIME_VALUE, periodItem.realValue())));
    }

    @Override
    public void onFiltersCategoryChanged(Set<String> subcategories) {

        StringBuilder stringBuilder = new StringBuilder();
        for (String subcategory : subcategories) {

            if (stringBuilder.length() > 0) {
                stringBuilder.append(", ");
            }

            stringBuilder.append(subcategory);
        }

        logEvent(FILTERS_CATEGORY_CHANGED,
                params(new Pair<>(CATEGORY_VALUE, stringBuilder.toString())));
    }

    @Override
    public void onFiltersRadiusChanged(String radius) {
        logEvent(FILTERS_RADIUS_CHANGED,
                params(new Pair<>(RADIUS_VALUE, radius)));
    }

    @Override
    public void onLanguageInit(String language) {
        logEvent(LANGUAGE_INITED, params(new Pair<>(LANGUAGE_VALUE, language)));
    }

    private void logEvent(final String eventName, Map<String, String> params) {
        FlurryAgent.logEvent(eventName, params);
        LogUtils.logWithMap(TAG, eventName, params);
    }

    private Map<String, String> params(Pair<String, String>... pairs) {
        Map<String, String> params = new HashMap<>();

        for (Pair<String, String> pair : pairs) {
            params.put(pair.first, pair.second);
        }

        return params;
    }
}
