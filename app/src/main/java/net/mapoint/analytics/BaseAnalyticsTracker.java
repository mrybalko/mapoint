package net.mapoint.analytics;

import net.mapoint.data.PeriodItem;
import net.mapoint.data.locations.EventType;
import net.mapoint.utils.Singleton;

import java.util.Set;

public class BaseAnalyticsTracker implements AnalyticsTrackerInterface {

    private FlurryAnalyticsTracker flurryAnalyticsTracker;
    private static Singleton<BaseAnalyticsTracker> _instanceHolder = new Singleton<>(BaseAnalyticsTracker::new);

    private BaseAnalyticsTracker() {
        flurryAnalyticsTracker = new FlurryAnalyticsTracker();
    }

    public static BaseAnalyticsTracker instance() {
        return _instanceHolder.instance();
    }


    @Override
    public void onNotificationEventReceived(String eventId, EventType eventType) {
        flurryAnalyticsTracker.onNotificationEventReceived(eventId, eventType);
    }

    @Override
    public void onNotificationEventClicked(String eventId, EventType eventType) {
        flurryAnalyticsTracker.onNotificationEventClicked(eventId, eventType);
    }

    @Override
    public void onNotificationEventDismissed(String eventId, EventType eventType) {
        flurryAnalyticsTracker.onNotificationEventDismissed(eventId, eventType);
    }

    @Override
    public void onNotificationEventRecreated(String oldEventId, String newEventId) {
        flurryAnalyticsTracker.onNotificationEventRecreated(oldEventId, newEventId);
    }

    @Override
    public void onToEventFromCommonList(String eventId, EventType eventType) {
        flurryAnalyticsTracker.onToEventFromCommonList(eventId, eventType);
    }

    @Override
    public void onToLocationFromMap(String locationId) {
        flurryAnalyticsTracker.onToLocationFromMap(locationId);
    }

    @Override
    public void onToEventFromLocation(String eventId, EventType eventType) {
        flurryAnalyticsTracker.onToEventFromLocation(eventId, eventType);
    }

    @Override
    public void onLocationCallClicked(String locationId) {
        flurryAnalyticsTracker.onLocationCallClicked(locationId);
    }

    @Override
    public void onEventLinkClicked(String eventId, EventType eventType) {
        flurryAnalyticsTracker.onEventLinkClicked(eventId, eventType);
    }

    @Override
    public void onFiltersTimeChanged(PeriodItem periodItem) {
        flurryAnalyticsTracker.onFiltersTimeChanged(periodItem);
    }

    @Override
    public void onFiltersCategoryChanged(Set<String> subcategories) {
        flurryAnalyticsTracker.onFiltersCategoryChanged(subcategories);
    }

    @Override
    public void onFiltersRadiusChanged(String radius) {
        flurryAnalyticsTracker.onFiltersRadiusChanged(radius);
    }

    @Override
    public void onLanguageInit(String language) {
        flurryAnalyticsTracker.onLanguageInit(language);
    }
}
