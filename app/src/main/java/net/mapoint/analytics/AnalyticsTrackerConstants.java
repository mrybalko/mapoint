package net.mapoint.analytics;

public interface AnalyticsTrackerConstants {

    String NOTIFICATION_EVENT_RECEIVED = "notification_event_received";
    String NOTIFICATION_EVENT_CLICKED = "notification_event_clicked";
    String NOTIFICATION_EVENT_DISMISSED = "notification_event_dismissed";
    String NOTIFICATION_EVENT_RECREATED = "notification_event_recreated";

    String TO_EVENT_FROM_COMMON_LIST = "to_event_from_main_list";

    String TO_LOCATION_FROM_MAP = "to_location_from_map";

    String TO_EVENT_FROM_LOCATION = "to_event_from_location";

    String LOCATION_CALL_CLICKED = "location_call_clicked";
    String EVENT_LINK_CLICKED = "event_link_clicked";

    String FILTERS_TIME_CHANGED = "filters_time_changed";
    String FILTERS_CATEGORY_CHANGED = "filters_category_changed";
    String FILTERS_RADIUS_CHANGED = "filters_radius_changed";
    String LANGUAGE_INITED = "language_inited";

    String EVENT_ID = "event_id";
    String EVENT_TYPE = "event_type";
    String LOCATION_ID = "location_id";
    String TIME_VALUE = "time_value";
    String CATEGORY_VALUE = "category_value";
    String RADIUS_VALUE = "radius_value";
    String LANGUAGE_VALUE = "language_value";

    String NEW_EVENT_ID = "new_event_id";
    String OLD_EVENT_ID = "old_event_id";
}
