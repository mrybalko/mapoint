package net.mapoint.analytics;

import net.mapoint.data.PeriodItem;
import net.mapoint.data.locations.EventType;

import java.util.Set;

public interface AnalyticsTrackerInterface {

    void onNotificationEventReceived(String eventId, EventType eventType);
    void onNotificationEventClicked(String eventId, EventType eventType);
    void onNotificationEventDismissed(String eventId, EventType eventType);
    void onNotificationEventRecreated(String oldEventId, String newEventId);

    void onToEventFromCommonList(String eventId, EventType eventType);
    void onToLocationFromMap(String locationId);
    void onToEventFromLocation(String eventId, EventType eventType);

    void onLocationCallClicked(final String locationId);
    void onEventLinkClicked(final String eventId, EventType eventType);

    void onFiltersTimeChanged(final PeriodItem periodItem);
    void onFiltersCategoryChanged(final Set<String> subcategories);
    void onFiltersRadiusChanged(final String radius);

    void onLanguageInit(String language);
}
