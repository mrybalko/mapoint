package net.mapoint.domain.api.categories.data;

import android.graphics.Color;

import net.mapoint.data.category.CategoryData;
import net.mapoint.data.category.CategoryGroupData;
import net.mapoint.data.category.CategoryIconPathProvider;

import java.util.ArrayList;
import java.util.List;

public class CategoriesMapper {

    public List<CategoryGroupData> categoriesFromJson(List<CategoryGroupJson> jsons) {

        List<CategoryGroupData> categories = new ArrayList<>();
        for (CategoryGroupJson data : jsons) {
            categories.add(categoryFromJson(data));
        }

        return categories;
    }

    public CategoryGroupData categoryFromJson(final CategoryGroupJson json) {

        int color = Color.parseColor(json.color());

        List<CategoryData> subcategoryData = new ArrayList<>();
        for (CategoryJson subcategoryJson : json.subcategories() ) {
            subcategoryData.add(subcategoryFromJson(subcategoryJson, color));
        }

        return new CategoryGroupData(json.id(),
                json.name(),
                subcategoryData,
                color,
                true,
                new CategoryIconPathProvider(json.getIcon(), 0));
    }

    private CategoryData subcategoryFromJson(CategoryJson json, int color) {
        return new CategoryData(json.id(),
                json.name(),
                json.categoryId(),
                false,
                color);
    }

    public enum CategoriesHandledError {
    }
}
