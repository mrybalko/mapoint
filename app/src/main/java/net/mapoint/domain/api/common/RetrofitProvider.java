package net.mapoint.domain.api.common;

import com.google.gson.Gson;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitProvider {

    public static final String HOST = "http://buns-client-env.eu-west-1.elasticbeanstalk.com";
    private static final String API_ENDPOINT = HOST + "/api/";

    public static Retrofit newInstance() {
        Gson gson = GsonProvider.newInstance();
        OkHttpClient client = OkHttpProvider.instance().okHttpClient();

        return new Retrofit.Builder()
                .baseUrl(API_ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(client)
                .build();
    }

    private RetrofitProvider() {
    }
}