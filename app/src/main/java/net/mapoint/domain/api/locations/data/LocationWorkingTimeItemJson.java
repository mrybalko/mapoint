package net.mapoint.domain.api.locations.data;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LocationWorkingTimeItemJson implements Serializable {

    @SerializedName("id")
    private String id;
    @SerializedName("dayNumber")
    private Integer dayNumber;
    @SerializedName("startTime")
    private String startTime;
    @SerializedName("endTime")
    private String endTime;

    public String getId() {
        return id;
    }

    public Integer getDayNumber() {
        return dayNumber;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getEndTime() {
        return endTime;
    }
}
