package net.mapoint.domain.api.locations.data;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Set;

public class LocationRequestBodyJson implements Serializable {

    @SerializedName("subcategories")
    private Set<String> subcategories;

    @SerializedName("include_facts")
    private Boolean factsEnabled;

    @SerializedName("time_range")
    private String timeRange;

    public LocationRequestBodyJson(Set<String> subcategories,
                                   Boolean factsEnabled,
                                   String timeRange) {
        this.subcategories = subcategories;
        this.factsEnabled = factsEnabled;
        this.timeRange = timeRange;
    }
}
