package net.mapoint.domain.api.locations.data;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class EventDatesItemJson implements Serializable {

    @SerializedName("id")
    private String id;

    @SerializedName("startDate")
    private String startDate;

    @SerializedName("endDate")
    private String endDate;

    @SerializedName("sessions")
    private List<String> sessions;

    public String getId() {
        return id;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public List<String> getSessions() {
        return sessions;
    }
}
