package net.mapoint.domain.api.common;

public class ApiConstants {

    public static final String CONTENT_TYPE_JSON = "application/json";
    public static final String HEADER_ACCEPT = "Accept";
    public static final String ACCEPT_JSON_HEADER = HEADER_ACCEPT + ": " + CONTENT_TYPE_JSON;
    public static final String CONTENT_TYPE_JSON_HEADER = "Content-Type: " + CONTENT_TYPE_JSON;
    public static final String HEADER_AUTHORIZATION = "Authorization";


    public static class Method {
        public static final String CATEGORIES_PATH = "get/categories";
        public static final String LOCATIONS_PATH = "get/locations/{lat}/{lng}/{radius}/";
        public static final String LOCATIONS_PATH_BY_COORD = "get/locations/{lat}/{lng}/";
        public static final String SEARCH_LOCATIONS_PATH = "search/locations/";
        public static final String ADD_EVENT_PATH = "add/offer";
        public static final String ADD_FACT_PATH = "add/fact";
    }
}