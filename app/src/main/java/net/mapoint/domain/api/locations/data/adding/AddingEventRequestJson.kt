package net.mapoint.domain.api.locations.data.adding

data class AddingEventRequestJson (
        val locationId :Long,
        val text :String,
        val address: String,
        val lat:Float,
        val lng:Float,
        val link: String,
        val phones: Array<String>,
        val fullText: String,
        val categories: Array<Integer>,
        val name:String
)