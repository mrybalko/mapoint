package net.mapoint.domain.api.categories.data;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class CategoryGroupJson implements Serializable {

        @SerializedName("id")
        private String id;

        @SerializedName("name")
        private String name;

        @SerializedName("subcategories")
        private List<CategoryJson> subcategoryList;

        @SerializedName("color")
        private String color;

        @SerializedName("icon")
        private String icon;

        public String getIcon() {
                return icon;
        }

        public String color() {
                return color;
        }

        public List<CategoryJson> subcategories() {
                return subcategoryList;
        }

        public String name() {
                return name;
        }

        public String id() {
                return id;
        }
}
