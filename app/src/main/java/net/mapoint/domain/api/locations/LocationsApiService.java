package net.mapoint.domain.api.locations;

import android.support.annotation.NonNull;

import net.mapoint.data.adding.SearchLocationData;
import net.mapoint.data.locations.LocationData;
import net.mapoint.domain.api.common.ApiService;
import net.mapoint.domain.api.common.CommunicationError;
import net.mapoint.domain.api.locations.data.LocationMapper;
import net.mapoint.domain.api.locations.data.LocationRequestBodyJson;
import net.mapoint.domain.api.locations.data.adding.AddingEventRequestJson;
import net.mapoint.utils.ApiServiceUtils;
import net.mapoint.utils.Either;

import java.util.List;
import java.util.Set;

import retrofit2.Response;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class LocationsApiService extends ApiService {

    @NonNull
    private final LocationsApiRetrofitService api;

    @NonNull
    private final LocationMapper locationMapper;

    public LocationsApiService() {
        api = LocationsApiRetrofitService.Factory.create(retrofit());
        locationMapper = new LocationMapper();
    }

    public Observable<Either<List<SearchLocationData>, Either<CommunicationError, LocationMapper.LocationsHandledError>>>
    getLocationsByCoord(final String lat,
                        final String lng) {

        return api.getLocationsByCoord(lat, lng)
                .subscribeOn(Schedulers.io())
                .map(response -> handleResponseN(response,
                        ApiServiceUtils.for200code(jsons -> locationMapper.searchLocationFromJson(jsons))));
    }

    public Observable<Either<List<LocationData>, Either<CommunicationError, LocationMapper.LocationsHandledError>>>
    getLocationsByCategories(final String lat,
                             final String lng,
                             final String radius,
                             final Set<String> categories,
                             final boolean factsEnabled,
                             final String timeRange) {

        return api.getLocationsByCategories(lat, lng, radius, new LocationRequestBodyJson(categories, factsEnabled, timeRange))
                .subscribeOn(Schedulers.io())
                .map(response -> handleResponseN(response,
                        ApiServiceUtils.for200code(jsons -> locationMapper.locationsFromJson(jsons))));
    }

    public Observable<Either<List<SearchLocationData>, Either<CommunicationError, LocationMapper.LocationsHandledError>>>
    getLocationsByNamePart(final String namePart) {

        return api.getLocationsByNamePart(namePart)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(response -> handleResponseN(response,
                        ApiServiceUtils.for200code(jsons -> locationMapper.searchLocationFromJson(jsons))));
    }

    public Observable<Either<String, Either<CommunicationError, LocationMapper.LocationsHandledError>>>
    addEvent(final AddingEventRequestJson event) {

        return api.addEvent(event)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(response -> handleEmptyResponseLocation(response));
    }

    public Observable<Either<String, Either<CommunicationError, LocationMapper.LocationsHandledError>>>
    addFact(final AddingEventRequestJson event) {

        return api.addFact(event)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(response -> handleEmptyResponseLocation(response));
    }

    private <Type> Either<String, Either<CommunicationError, LocationMapper.LocationsHandledError>> handleEmptyResponseLocation(Response<Type> response) {
        if (response.isSuccessful()) {
            return Either.left("");
        } else {
            return Either.right(Either.left(new CommunicationError()));
        }
    }
}
