package net.mapoint.domain.api.locations.data.search;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SearchLocationJson {

    @SerializedName("id")
    private Long id;

    @SerializedName("lat")
    private Float lat;

    @SerializedName("lng")
    private Float lng;

    @SerializedName("address")
    private String address;

    @SerializedName("name")
    private String name;

    @SerializedName("type")
    private String type;

    @SerializedName("phones")
    private List<String> phones;

    public Long getId() {
        return id;
    }

    public Float getLat() {
        return lat;
    }

    public Float getLng() {
        return lng;
    }

    public String getAddress() {
        return address;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public List<String> getPhones() {
        return phones;
    }
}
