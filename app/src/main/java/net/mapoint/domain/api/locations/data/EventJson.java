package net.mapoint.domain.api.locations.data;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class EventJson implements Serializable {

    @SerializedName("id")
    private String id;

    @SerializedName("text")
    private String text;

    @SerializedName("location")
    private InnerLocationJson innerLocationJson;

    @SerializedName("fullText")
    private String fullDescription;

    @SerializedName("link")
    private String link;

    @SerializedName("type")
    private String type;

    @SerializedName("dates")
    private List<EventDatesItemJson> dates;

    @SerializedName("subcategories")
    private List<String> subcategories;

    public String getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public InnerLocationJson getInnerLocationJson() {
        return innerLocationJson;
    }

    public String getFullDescription() {
        return fullDescription;
    }

    public String getLink() {
        return link;
    }

    public String eventType() {
        return type;
    }

    public List<String> subcategories() {
        return subcategories;
    }

    public List<EventDatesItemJson> dates() {
        return dates;
    }
}
