package net.mapoint.domain.api.locations.data;

import net.mapoint.R;
import net.mapoint.data.adding.AddingEventData;
import net.mapoint.data.adding.ParameterData;
import net.mapoint.data.adding.SearchLocationData;
import net.mapoint.data.locations.EventData;
import net.mapoint.data.locations.EventDatesItemData;
import net.mapoint.data.locations.EventType;
import net.mapoint.data.locations.InnerLocationData;
import net.mapoint.data.locations.LocationData;
import net.mapoint.data.locations.LocationWorkingTimeData;
import net.mapoint.domain.api.locations.data.adding.AddingEventRequestJson;
import net.mapoint.domain.api.locations.data.search.SearchLocationJson;
import net.mapoint.ui.base.MapointApp;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class LocationMapper {

    public static List<SearchLocationData> searchLocationFromJson(final List<SearchLocationJson> jsons) {

        List<SearchLocationData> locationsData = new ArrayList<>();

        for (SearchLocationJson locationJson : jsons) {
            locationsData.add(new SearchLocationData.Builder()

                    .id(locationJson.getId())
                    .lat(locationJson.getLat())
                    .lng(locationJson.getLng())
                    .address(locationJson.getAddress())
                    .name(locationJson.getName())
                    .type(locationJson.getType())
                    .phones(locationJson.getPhones())
                    .build());
        }

        return locationsData;
    }


    public static List<LocationData> locationsFromJson(final List<LocationJson> jsons) {

        List<LocationData> locationsData = new ArrayList<>();

        for (LocationJson locationJson : jsons) {
            locationsData.add(new LocationData.Builder()
                    .id(locationJson.id())
                    .lat(locationJson.lat())
                    .lng(locationJson.lng())
                    .address(locationJson.address())
                    .name(locationJson.name())
                    .type(locationJson.type())
                    .facts(eventFromJson(locationJson.facts()))
                    .offers(eventFromJson(locationJson.offers()))
                    .phones(locationJson.phones())
                    .workingTime(locationWorkingTime(locationJson.locationWorkingTime()))
                    .distance(locationJson.distance())
            .build());
        }

        return locationsData;
    }

    private static List<EventData> eventFromJson(final List<EventJson> jsons) {
        List<EventData> eventsData = new ArrayList<>();

        if (jsons == null) {
            return eventsData;
        }

        for(EventJson eventJson : jsons) {

            EventData.Builder builder = new EventData.Builder();

            EventType eventType = eventTypeBy(eventJson.eventType());
            List<String> subcategories = new ArrayList<>();

            if (eventType == EventType.Fact) {
                String factSubcategory = MapointApp.getContext().getString(R.string.category_facts_title);
                subcategories.add(factSubcategory);
            } else {
                subcategories.addAll(eventJson.subcategories());
            }

            builder.id(eventJson.getId())
                    .text(eventJson.getText())
                    .link(eventJson.getLink())
                    .fullDescription(eventJson.getFullDescription())
                    .innerLocationData(innerLocationFromJson(eventJson.getInnerLocationJson()))
                    .subcategories(subcategories)
                    .eventType(eventType)
                    .dates(mapDates(eventJson.dates()));

            eventsData.add(builder.build());
        }

        return eventsData;
    }

    private static EventType eventTypeBy(final String stringValue) {
        for (EventType eventType : EventType.values()) {
            if (eventType.getValue().equals(stringValue)) {
                return eventType;
            }
        }

        return EventType.Unknown;
    }

    private static InnerLocationData innerLocationFromJson(final InnerLocationJson innerLocationJson) {

        InnerLocationData.Builder builder = new InnerLocationData.Builder();

        builder.id(innerLocationJson.getId())
                .lat(innerLocationJson.getLat())
                .lng(innerLocationJson.getLng())
                .address(innerLocationJson.getAddress())
                .name(innerLocationJson.getName())
                .type(innerLocationJson.getType())
                .phones(innerLocationJson.getPhones())
                .distance(innerLocationJson.distance());

        return builder.build();
    }

    private static List<LocationWorkingTimeData> locationWorkingTime(final List<LocationWorkingTimeItemJson> jsons) {
        List<LocationWorkingTimeData> workingTimeData = new ArrayList<>();

        if (jsons == null){
            return workingTimeData;
        }

        for(LocationWorkingTimeItemJson item : jsons) {

            workingTimeData.add(new LocationWorkingTimeData(
                    item.getId(),
                    item.getDayNumber(),
                    item.getStartTime(),
                    item.getEndTime()));
        }

        return workingTimeData;
    }

    private static List<EventDatesItemData> mapDates(final List<EventDatesItemJson> jsons) {

        List<EventDatesItemData> datas = new ArrayList<>();

        if (jsons != null) {
            for (EventDatesItemJson itemJson : jsons) {

                datas.add(new EventDatesItemData(
                        itemJson.getId(),
                        itemJson.getStartDate(),
                        itemJson.getEndDate(),
                        itemJson.getSessions()));
            }
        }

        return datas;
    }

    public static AddingEventRequestJson addingEventRequestJson(AddingEventData addingEventData) {

        Integer[] categoriesIds = convertCategoriesNameToIds(addingEventData.getCategories());

        return new AddingEventRequestJson(
                addingEventData.getLocationId(),
                addingEventData.getText(),
                addingEventData.getAddress(),
                addingEventData.getLat(),
                addingEventData.getLng(),
                addingEventData.getLink(),
                split(addingEventData.getPhones()),
                addingEventData.getFullText(),
                categoriesIds,
                addingEventData.getLocationNameValue()
        );
    }

    private static String[] split(String value) {
        if (value == null) {
            value = "";
        }
        return value.split(",");
    }

    private static Integer[] convertCategoriesNameToIds(List<ParameterData> categories) {

        List<Integer> ids = new ArrayList();

        for (ParameterData category : categories) {
            ids.add(Integer.parseInt(category.id()));
        }

        Integer[] idsArray = new Integer[categories.size()];
        idsArray = ids.toArray(idsArray);
        return idsArray;
    }

    public enum LocationsHandledError {
    }
}
