package net.mapoint.domain.api.locations;

import android.support.annotation.NonNull;

import net.mapoint.domain.api.common.ApiConstants;
import net.mapoint.domain.api.locations.data.LocationJson;
import net.mapoint.domain.api.locations.data.LocationRequestBodyJson;
import net.mapoint.domain.api.locations.data.adding.AddingEventRequestJson;
import net.mapoint.domain.api.locations.data.adding.AddingFactRequestJson;
import net.mapoint.domain.api.locations.data.search.SearchLocationJson;

import java.util.List;

import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

public interface LocationsApiRetrofitService {

    @Headers({ApiConstants.ACCEPT_JSON_HEADER})
    @GET(ApiConstants.Method.LOCATIONS_PATH)
    Observable<Response<List<LocationJson>>> getLocations(@Path("lat") @NonNull String lat,
                                                          @Path("lng") @NonNull String lng,
                                                          @Path("radius") @NonNull String radius);

    @Headers({ApiConstants.ACCEPT_JSON_HEADER})
    @GET(ApiConstants.Method.LOCATIONS_PATH_BY_COORD)
    Observable<Response<List<SearchLocationJson>>> getLocationsByCoord(@Path("lat") @NonNull String lat,
                                                                       @Path("lng") @NonNull String lng);

    @Headers({ApiConstants.ACCEPT_JSON_HEADER})
    @POST(ApiConstants.Method.LOCATIONS_PATH)
    Observable<Response<List<LocationJson>>> getLocationsByCategories(@Path("lat") @NonNull String lat,
                                                                      @Path("lng") @NonNull String lng,
                                                                      @Path("radius") @NonNull String radius,
                                                                      @Body LocationRequestBodyJson locationRequestBodyJson);

    @Headers({ApiConstants.ACCEPT_JSON_HEADER})
    @GET(ApiConstants.Method.SEARCH_LOCATIONS_PATH)
    Observable<Response<List<SearchLocationJson>>> getLocationsByNamePart(@Query("q") @NonNull String q);

    @Headers({ApiConstants.ACCEPT_JSON_HEADER})
    @POST(ApiConstants.Method.ADD_EVENT_PATH)
    Observable<Response<Void>> addEvent(@Body @NonNull AddingEventRequestJson body);

    @Headers({ApiConstants.ACCEPT_JSON_HEADER})
    @POST(ApiConstants.Method.ADD_FACT_PATH)
    Observable<Response<Void>> addFact(@Body @NonNull AddingEventRequestJson body);

    class Factory {
        public static LocationsApiRetrofitService create(@NonNull final Retrofit retrofit) {
            return retrofit.create(LocationsApiRetrofitService.class);
        }
    }
}
