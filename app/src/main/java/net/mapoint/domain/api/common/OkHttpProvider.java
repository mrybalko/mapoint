package net.mapoint.domain.api.common;

import net.mapoint.BuildConfig;
import net.mapoint.ui.base.MapointApp;
import net.mapoint.utils.Singleton;
import net.mapoint.utils.http.LangInterceptor;
import net.mapoint.utils.http.SetHeaderInterceptor;
import net.mapoint.utils.storage.SharedPreferencesProvider;

import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

public class OkHttpProvider {

    private static Singleton<OkHttpProvider> _instanceHolder = new Singleton<>(OkHttpProvider::new);

    private OkHttpClient okHttpClient;

    public static OkHttpProvider instance() {
        return _instanceHolder.instance();
    }

    private OkHttpProvider() {
        final OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.addInterceptor(new HttpLoggingInterceptor());

        if (BuildConfig.DEBUG) {
            try {
                final X509TrustManager trustManager = getTrustManager();
                final SSLContext sslContext = SSLContext.getInstance("TLS");

                sslContext.init(null, new TrustManager[] { trustManager }, null);
                builder.sslSocketFactory(sslContext.getSocketFactory(), trustManager);
                builder.hostnameVerifier((hostname, session) -> true);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        builder.connectTimeout(30, TimeUnit.SECONDS);
        builder.readTimeout(0, TimeUnit.SECONDS);
        builder.writeTimeout(0, TimeUnit.SECONDS);
        builder.addInterceptor(new SetHeaderInterceptor("User-Agent", MapointApp.userAgent()));
        builder.addInterceptor(new LangInterceptor(SharedPreferencesProvider.instance()));

        builder.addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY));
        okHttpClient = builder.build();
    }

    private X509TrustManager getTrustManager() throws NoSuchAlgorithmException, KeyStoreException {
        final TrustManagerFactory trustManagerFactory =
                TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        trustManagerFactory.init((KeyStore) null);
        final TrustManager[] trustManagers = trustManagerFactory.getTrustManagers();

        if (trustManagers.length != 1 || !(trustManagers[0] instanceof X509TrustManager)) {
            throw new IllegalStateException("Unexpected default trust managers:"
                    + Arrays.toString(trustManagers));
        }

        return (X509TrustManager) trustManagers[0];
    }

    public OkHttpClient okHttpClient() {
        return okHttpClient;
    }
}
