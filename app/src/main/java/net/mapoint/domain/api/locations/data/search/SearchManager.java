package net.mapoint.domain.api.locations.data.search;

import com.google.android.gms.maps.model.LatLng;

import net.mapoint.data.adding.ParameterData;
import net.mapoint.data.adding.SearchLocationData;
import net.mapoint.domain.api.common.CommunicationError;
import net.mapoint.domain.api.locations.LocationsApiService;
import net.mapoint.domain.api.locations.data.LocationMapper;
import net.mapoint.utils.Either;
import net.mapoint.utils.Singleton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

public class SearchManager {

    private static Singleton<SearchManager> _instanceHolder = new Singleton<>(SearchManager::new);

    private LocationsApiService locationsApiService;
    Map<String, SearchLocationData> searchLocationData;

    private SearchManager() {
        locationsApiService = new LocationsApiService();
    }

    public static SearchManager instance() {
        return _instanceHolder.instance();
    }

    public Observable<Either<List<ParameterData>, Either<CommunicationError, LocationMapper.LocationsHandledError>>>
    getLocationsByNamePart(String requestString) {
        return locationsApiService.getLocationsByNamePart(requestString)
                .map(either -> {

                    List<ParameterData> parameters = new ArrayList<>();
                    if (either.isLeft()) {
                        searchLocationData = new HashMap<>();
                        List<SearchLocationData> locations = either.left().get();

                        for (SearchLocationData locationData : locations) {

                            Long id = locationData.getId();
                            parameters.add(new ParameterData(id.toString(), locationData.getName()));
                            searchLocationData.put(id.toString(), locationData);
                        }

                        return Either.left(parameters);
                    }

                    return Either.left(new ArrayList<ParameterData>());
                });
    }

    public SearchLocationData getLocationData(String id) {
        return searchLocationData.get(id);
    }

    public Observable<List<SearchLocationData>> getLocationsByCoord(LatLng latLng) {
        double lat = latLng.latitude;
        double lng = latLng.longitude;

        return locationsApiService.getLocationsByCoord(
                String.valueOf(lat),
                String.valueOf(lng))
                .map(either -> {

                    List<SearchLocationData> list = new ArrayList<>();
                    if (either.isLeft()) {
                        list = either.left().get();
                    }

                    return list;
                })
                .observeOn(AndroidSchedulers.mainThread());
    }
}
