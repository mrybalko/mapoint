package net.mapoint.domain.api.common;

import android.support.annotation.NonNull;

import com.annimon.stream.Optional;
import com.annimon.stream.function.Function;

import net.mapoint.utils.Either;

import retrofit2.Response;
import retrofit2.Retrofit;

public class ApiService {

    protected Retrofit retrofit() {
        return RetrofitProvider.newInstance();
    }

    @NonNull
    protected <JsonType, ResponseType, ErrorType> Either<ResponseType, Either<CommunicationError, ErrorType>>
    handleResponseN(final Response<JsonType> response,
                    final Function<Response<JsonType>, Optional<ResponseType>> convertSuccess) {

        Optional<ResponseType> success = convertSuccess.apply(response);

        if (success.isPresent()) {
            return Either.left(success.get());
        } else {
            return Either.right(Either.left(new CommunicationError()));
        }
    }

    @NonNull
    protected <JsonType, ResponseType, ErrorType> Either<ResponseType, Either<CommunicationError, ErrorType>>
    handleResponseN(final Response<JsonType> response,
                    final Function<Response<JsonType>, Optional<ResponseType>> convertSuccess,
                    final Function<Response<JsonType>, Optional<ErrorType>> errorConverter) {

        Optional<ResponseType> success = convertSuccess.apply(response);

        if (success.isPresent()) {
            return Either.left(success.get());
        } else {
            Optional<ErrorType> error = errorConverter.apply(response);
            if (error.isPresent()) {
                return Either.right(Either.right(error.get()));
            } else {
                return Either.right(Either.left(new CommunicationError()));
            }
        }
    }
}