package net.mapoint.domain.api.locations.data;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class InnerLocationJson {

    @SerializedName("id")
    private String id;

    @SerializedName("lat")
    private double lat;

    @SerializedName("lng")
    private double lng;

    @SerializedName("address")
    private String address;

    @SerializedName("type")
    private String type;

    @SerializedName("name")
    private String name;

    @SerializedName("phones")
    private List<String> phones;

    @SerializedName("distance")
    private int distance;

    public String getId() {
        return id;
    }

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }

    public String getAddress() {
        return address;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public List<String> getPhones() {
        return phones;
    }

    public int distance() {
        return distance;
    }
}
