package net.mapoint.domain.api.categories;

import android.support.annotation.NonNull;

import net.mapoint.domain.api.categories.data.CategoryGroupJson;
import net.mapoint.domain.api.common.ApiConstants;

import java.util.List;

import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import rx.Observable;

public interface CategoriesApiRetrofitService {

    @Headers({ApiConstants.ACCEPT_JSON_HEADER})
    @GET(ApiConstants.Method.CATEGORIES_PATH)
    Observable<Response<List<CategoryGroupJson>>> getCategories();

    class Factory {
        public static CategoriesApiRetrofitService create(@NonNull final Retrofit retrofit) {
            return retrofit.create(CategoriesApiRetrofitService.class);
        }
    }
}