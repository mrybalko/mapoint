package net.mapoint.domain.api.categories;

import android.support.annotation.NonNull;

import net.mapoint.data.category.CategoryGroupData;
import net.mapoint.domain.api.categories.data.CategoriesMapper;
import net.mapoint.domain.api.common.ApiService;
import net.mapoint.domain.api.common.CommunicationError;
import net.mapoint.utils.ApiServiceUtils;
import net.mapoint.utils.Either;

import java.util.List;

import rx.Observable;
import rx.schedulers.Schedulers;

public class CategoriesApiService extends ApiService {

    @NonNull
    private final CategoriesApiRetrofitService api;

    @NonNull
    private final CategoriesMapper categoriesMapper;

    public CategoriesApiService() {
        api = CategoriesApiRetrofitService.Factory.create(retrofit());
        categoriesMapper = new CategoriesMapper();
    }


    public Observable<Either<List<CategoryGroupData>, Either<CommunicationError, CategoriesMapper.CategoriesHandledError>>>
    getCategories() {
        return api.getCategories()
                .subscribeOn(Schedulers.io())
                .map(response -> handleResponseN(response,
                        ApiServiceUtils.for200code(jsons -> categoriesMapper.categoriesFromJson(jsons))));
    }
}
