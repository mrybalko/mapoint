package net.mapoint.domain.api.locations.data;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class LocationJson implements Serializable {

    @SerializedName("id")
    private String id;

    @SerializedName("lat")
    private double lat;

    @SerializedName("lng")
    private double lng;

    @SerializedName("address")
    private String address;

    @SerializedName("name")
    private String name;

    @SerializedName("type")
    private String type;

    @SerializedName("facts")
    private List<EventJson> facts;

    @SerializedName("offers")
    private List<EventJson> offers;

    @SerializedName("phones")
    private List<String> phones;

    @SerializedName("workingTimes")
    private List<LocationWorkingTimeItemJson> locationWorkingTime;

    @SerializedName("distance")
    private int distance;

    public String id() {
        return id;
    }

    public double lat() {
        return lat;
    }

    public double lng() {
        return lng;
    }

    public String address() {
        return address;
    }

    public String name() {
        return name;
    }

    public String type() {
        return type;
    }

    public List<EventJson> facts() {
        return facts;
    }

    public List<EventJson> offers() {
        return offers;
    }

    public List<String> phones() {
        return phones;
    }

    public List<LocationWorkingTimeItemJson> locationWorkingTime() {
        return locationWorkingTime;
    }

    public int distance() {
        return distance;
    }
}
