package net.mapoint.domain.api.common;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class GsonProvider {

    public static Gson newInstance() {
        return new GsonBuilder().create();
    }

    private GsonProvider() {
    }
}
