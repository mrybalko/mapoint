package net.mapoint.domain.api.locations.data.adding

data class AddingFactRequestJson(
        val text :String,
        val address: String,
        val lat:Float,
        val lng:Float,
        val link: String
)
