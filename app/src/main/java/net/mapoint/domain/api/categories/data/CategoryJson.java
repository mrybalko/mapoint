package net.mapoint.domain.api.categories.data;

import com.google.gson.annotations.SerializedName;

public class CategoryJson {

    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;

    @SerializedName("categoryId")
    private String categoryId;

    public String id() {
        return id;
    }

    String name() {
        return name;
    }

    public String categoryId() {
        return categoryId;
    }
}
