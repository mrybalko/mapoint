package net.mapoint.managers;

import android.location.Location;
import android.text.TextUtils;
import android.util.Pair;

import net.mapoint.data.LocationsDetailsData;
import net.mapoint.data.adding.AddingEventData;
import net.mapoint.data.adding.ParameterData;
import net.mapoint.data.locations.InnerLocationData;
import net.mapoint.data.locations.LocationData;
import net.mapoint.domain.api.common.CommunicationError;
import net.mapoint.domain.api.locations.LocationsApiService;
import net.mapoint.domain.api.locations.data.LocationMapper;
import net.mapoint.domain.api.locations.data.adding.AddingEventRequestJson;
import net.mapoint.utils.Either;
import net.mapoint.utils.LogUtils;
import net.mapoint.utils.Singleton;
import net.mapoint.utils.storage.SharedPreferencesProvider;

import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.subjects.BehaviorSubject;
import rx.subjects.PublishSubject;

public class LocationManager {

    private LocationsApiService locationsApiService;
    private SettingsManager settingsManager;
    private CategoriesManager categoriesManager;

    private SharedPreferencesProvider sharedPreferencesProvider;

    private static Singleton<LocationManager> _instanceHolder = new Singleton<>(LocationManager::new);

    private BehaviorSubject<Location> currentLocationSubject = BehaviorSubject.create();
    private BehaviorSubject<LocationsDetailsData> locationsDetailsDataWithPreviousBehaviorSubject = BehaviorSubject.create();
    private PublishSubject<LocationsDetailsData> locationsDetailsDataCleanObservable = PublishSubject.create();

    private LocationManager() {
        sharedPreferencesProvider = SharedPreferencesProvider.instance();
        locationsApiService = new LocationsApiService();
        locationsMap = new HashMap<>();
        settingsManager = SettingsManager.instance();
        categoriesManager = CategoriesManager.instance();
    }

    private Map<String, LocationData> locationsMap;

    public static LocationManager instance() {
        return _instanceHolder.instance();
    }

    @Nullable
    public LocationData getLocation(final String locationId) {
        return locationsMap.get(locationId);
    }

    public void updateCurrentLocation(final Location newLocation) {
        currentLocationSubject.onNext(newLocation);
    }

    @Nullable
    public Set<String> seenEvents() {
        return sharedPreferencesProvider.seenEvents();
    }

    public void addSeenEvents(String seenEvent) {
        sharedPreferencesProvider.addSeenEvents(seenEvent);
    }

    public Observable<Void> locationRadiusChangedObservable() {

        LocationsDetailsData.Builder locationsDetailsDataBuilder = new LocationsDetailsData.Builder();

        return Observable.combineLatest(
                currentLocationSubject,
                settingsManager.onRadiusChanged(),
                (location, newRadius) -> new Pair<>(location, newRadius))
                .flatMap(locationStringPair -> getLocationsByCategories(locationStringPair, locationsDetailsDataBuilder))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(throwable -> {
                    locationsDetailsDataWithPreviousBehaviorSubject.onNext(null);
                })
                .map(either -> {
                    if (either.isLeft()) {

                        List<LocationData> locations = either.left().get();

                        String text = "locations.size : empty";
                        if (locations != null) {
                            int size = locations.size();
                            text = "locations.size : " + String.valueOf(size);
                        }

                        LoggingManager.instance().logMessage(text);

                        locationsMap.clear();
                        for (LocationData locationData : locations) {
                            if (locationData != null && locationData.id() != null) {
                                locationsMap.put(locationData.id(), locationData);
                            }
                        }

                        locationsDetailsDataBuilder.locationData(locations);
                        LocationsDetailsData locationsDetailsData = locationsDetailsDataBuilder.build();
                        locationsDetailsDataWithPreviousBehaviorSubject.onNext(locationsDetailsData);
                        locationsDetailsDataCleanObservable.onNext(locationsDetailsData);
                    }

                    return null;
                });
    }

    private Observable<Either<List<LocationData>, Either<CommunicationError, LocationMapper.LocationsHandledError>>>
    getLocationsByCategories(
            Pair<Location, String> locationStringPair,
            LocationsDetailsData.Builder locationsDetailsDataBuilder) {
        Location location = locationStringPair.first;
        double lat = location.getLatitude();
        double lng = location.getLongitude();

        int radiusInt = Integer.parseInt(locationStringPair.second);
        float radius = (float)  radiusInt/ 1000.0f;

        locationsDetailsDataBuilder.clear();
        locationsDetailsDataBuilder.currentLocation(location);
        locationsDetailsDataBuilder.radius(radiusInt);

        String text = "start request for : lat : " + String.valueOf(lat) +
                " : lng : " + String.valueOf(lng) +
                " : radiusInt : " + String.valueOf(radiusInt) +
                " : selected categories : " + LogUtils.setToString(categoriesManager.readSelectedCategories());

        LoggingManager.instance().logMessage(text);

        return locationsApiService.getLocationsByCategories(
                String.valueOf(lat),
                String.valueOf(lng),
                String.valueOf(radius),
                categoriesManager.readSelectedCategories(),
                categoriesManager.readFactsEnabled(),
                settingsManager.periodItem().realValue());
    }

    private List<ParameterData> converLocationToParameters(List<InnerLocationData> locations) {
        List<ParameterData> parameters = new ArrayList<>();
        for (InnerLocationData locationData : locations) {
            parameters.add(new ParameterData(locationData.getId(), locationData.getName()));
        }

        return parameters;
    }

    private List<ParameterData> filteredParameters(String enteredText, List<ParameterData> parameters) {
        List<ParameterData> filteredParameters = new ArrayList<>();

        if (TextUtils.isEmpty(enteredText)) {
            return filteredParameters;
        }

        for (ParameterData parameter : parameters) {

            String parameterValueLow = parameter.value().toLowerCase();
            String enteredTextLow = enteredText.toLowerCase();
            if (parameterValueLow.contains(enteredTextLow)) {
                filteredParameters.add(parameter);
            }
        }

        return filteredParameters;
    }

    public Observable<LocationsDetailsData> locationsDetailsDataWithPreviousObservable() {
        return locationsDetailsDataWithPreviousBehaviorSubject
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<LocationsDetailsData> locationsDetailsDataCleanObservable() {
        return locationsDetailsDataCleanObservable
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<Location> currentLocation() {
        return currentLocationSubject;
    }

    public Observable<Either<String, Either<CommunicationError, LocationMapper.LocationsHandledError>>> addEvent(AddingEventData data) {

        AddingEventRequestJson event = LocationMapper.addingEventRequestJson(data);

        return locationsApiService.addEvent(event)
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<Either<String, Either<CommunicationError, LocationMapper.LocationsHandledError>>> addFact(AddingEventData data) {

        AddingEventRequestJson event = LocationMapper.addingEventRequestJson(data);

        return locationsApiService.addFact(event)
                .observeOn(AndroidSchedulers.mainThread());
    }

}
