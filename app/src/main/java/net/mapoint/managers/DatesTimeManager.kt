package net.mapoint.managers

import net.mapoint.data.adding.AddingEventDateTimeData

object DatesTimeManager {

    private val datesTime: MutableList<AddingEventDateTimeData> = mutableListOf()

    fun datesTime(): MutableList<AddingEventDateTimeData> {
        return datesTime.toMutableList()
    }

    fun setDatesTime(newDatesTime: MutableList<AddingEventDateTimeData>) {
        this.datesTime.clear()
        this.datesTime.addAll(newDatesTime.toMutableList())
    }

    fun resetDatesTime() {
        this.datesTime.clear()
    }
}