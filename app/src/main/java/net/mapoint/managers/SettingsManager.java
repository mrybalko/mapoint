package net.mapoint.managers;

import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import net.mapoint.R;
import net.mapoint.analytics.BaseAnalyticsTracker;
import net.mapoint.data.LanguageItem;
import net.mapoint.data.PeriodItem;
import net.mapoint.data.locations.SimpleLocation;
import net.mapoint.ui.base.MapointApp;
import net.mapoint.utils.Singleton;
import net.mapoint.utils.storage.SharedPreferencesProvider;

import java.util.Locale;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.subjects.BehaviorSubject;

public class SettingsManager {

    private static Singleton<SettingsManager> _instanceHolder = new Singleton<>(SettingsManager::new);

    private BehaviorSubject<String> radiusLocationSubject = BehaviorSubject.create();

    private SharedPreferencesProvider sharedPreferencesProvider;

    private SettingsManager() {
        sharedPreferencesProvider = SharedPreferencesProvider.instance();

        Resources res = MapointApp.getContext().getResources();
        final String defaultRadius = res.getStringArray(R.array.markers_area_radiuses)[3];

        PeriodItem periodItem = periodItem();
        if (periodItem == null) {
            updatePeriodItem(PeriodItem.Day);
        }

        radiusLocationSubject.onNext(sharedPreferencesProvider.readSelectedRadius(defaultRadius));
    }

    public static SettingsManager instance() {
        return _instanceHolder.instance();
    }

    public Observable<String> onRadiusChanged(){
        return radiusLocationSubject
                .observeOn(AndroidSchedulers.mainThread());
    }

    public void updateRadiusLocation(final String radius) {
        radiusLocationSubject.onNext(radius);
        sharedPreferencesProvider.saveSelectedRadius(radius);
    }

    public boolean notificationSoundEnabled() {
        return sharedPreferencesProvider.notificationSoundEnabled();
    }

    public void updateNotificationSoundEnabled(boolean isEnabled) {
        sharedPreferencesProvider.updateNotificationSoundEnabled(isEnabled);
    }

    public SimpleLocation lastLocation() {
        String lastLocation = sharedPreferencesProvider.lastLocation();
        if (!TextUtils.isEmpty(lastLocation)) {
            String[] locationParts = lastLocation.split(":");
            return new SimpleLocation(Double.parseDouble(locationParts[0]), Double.parseDouble(locationParts[1]));
        }

        return null;
    }

    public void updateLastLocation(SimpleLocation simpleLocation) {
        String lastLocation = String.valueOf(simpleLocation.getLat()) + ":" + String.valueOf(simpleLocation.getLon());
        sharedPreferencesProvider.updateLastLocation(lastLocation);
    }

    public long lastNotificationTime() {
        return sharedPreferencesProvider.lastNotificationTime();
    }

    public void updateLastNotificationTime(long time) {
        sharedPreferencesProvider.updateLastNotificationTime(time);
    }

    public PeriodItem periodItem() {
        String realValue = sharedPreferencesProvider.periodItem();
        return PeriodItem.byRealValue(realValue);
    }

    public void updatePeriodItem(PeriodItem periodItem) {
        sharedPreferencesProvider.updatePeriodItem(periodItem.realValue());
    }

    @NonNull
    public LanguageItem languageItem() {
        String code = sharedPreferencesProvider.languageItem();

        if (TextUtils.isEmpty(code)) {
            code = Locale.getDefault().getLanguage();
        }

        LanguageItem languageItem = LanguageItem.contains(code) ? LanguageItem.byRealValue(code) : LanguageItem.Ru;

        BaseAnalyticsTracker.instance().onLanguageInit(languageItem.realValue());
        return languageItem;
    }

    public void updateLanguageItem(LanguageItem periodItem) {
        sharedPreferencesProvider.updateLanguageItem(periodItem.realValue());
    }
}
