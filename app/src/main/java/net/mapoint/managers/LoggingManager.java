package net.mapoint.managers;

import android.content.Context;

import com.crashlytics.android.Crashlytics;

import net.mapoint.ui.base.MapointApp;
import net.mapoint.utils.LogUtils;
import net.mapoint.utils.Singleton;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class LoggingManager {

    private final static String TAG = "LoggingManager";
    private final static String FILE_NAME = "logs.txt";

    private static Singleton<LoggingManager> _instanceHolder = new Singleton<>(LoggingManager::new);

    private LoggingManager() {
        File file = new File(MapointApp.getContext().getFilesDir(), FILE_NAME);
        if(!file.exists()) {
            clearLogs();
        }
    }

    public static LoggingManager instance() {
        return _instanceHolder.instance();
    }

    public void clearLogs() {
        writeFile("", false);
    }

    public void logMessage(String value){
        String newContent = " ::: "  + value;
        writeFile(newContent, true);

        //Crashlytics.log(newContent);

        LogUtils.log(TAG, "logged : " + newContent);
    }

    public void writeFile(final String value, boolean canAppend) {
        try {
            int mode = canAppend ? Context.MODE_APPEND : Context.MODE_PRIVATE;
            FileOutputStream outputStream = MapointApp.getContext().openFileOutput(FILE_NAME, mode);
            OutputStreamWriter outputWriter = new OutputStreamWriter(outputStream);

            //LogUtils.log(TAG, "write : " + value);
            outputWriter.write(value);
            outputWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String readFile() {
        try {
            FileInputStream fis = MapointApp.getContext().openFileInput(FILE_NAME);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader bufferedReader = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line);
            }
            isr.close();

            String content = sb.toString();

            LogUtils.log(TAG, "read : " + content);
            return content;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    public void sendLoggedDataToServer() {
        try {
            String dataFromFile = readFile();
            LogUtils.log("LoggingManager", "dataFromFile : " + dataFromFile);
            Crashlytics.log(dataFromFile);
            clearLogs();
            throw new RuntimeException("test exception");
        } catch (RuntimeException e) {
            Crashlytics.logException(e);
        }
    }
}