package net.mapoint.managers;

import android.graphics.Bitmap;

import net.mapoint.R;
import net.mapoint.data.category.CategoryData;
import net.mapoint.data.category.CategoryGroupData;
import net.mapoint.data.category.CategoryIconPathProvider;
import net.mapoint.domain.api.categories.CategoriesApiService;
import net.mapoint.domain.api.categories.data.CategoriesMapper;
import net.mapoint.domain.api.common.CommunicationError;
import net.mapoint.ui.base.MapointApp;
import net.mapoint.utils.Either;
import net.mapoint.utils.MarkerBitmapProvider;
import net.mapoint.utils.Singleton;
import net.mapoint.utils.storage.SharedPreferencesProvider;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.subjects.PublishSubject;

public class CategoriesManager {

    public static final String FACT_CATEGORY_ID = "-10001";
    public static final String FACT_CATEGORY_ITEM_ID = "-100011";
    public static final String ADD_EVENT_CATEGORY_ITEM_ID = "-100012";
    private final Integer FACTS_COLOR = 0xff45d4b8;
    public static final Integer ADD_EVENT_COLOR = 0xff093348;

    private CategoriesApiService categoriesApiService;
    private SharedPreferencesProvider sharedPreferencesProvider;
    private List<CategoryGroupData> categoriesGroup;
    private PublishSubject<Set<String>> onSelectedCategoriesChanged = PublishSubject.create();

    private Map<String, Integer> subcategoryColors;

    private static Singleton<CategoriesManager> _instanceHolder = new Singleton<>(CategoriesManager::new);

    private CategoriesManager() {
        categoriesApiService = new CategoriesApiService();
        sharedPreferencesProvider = SharedPreferencesProvider.instance();

        categoriesGroup = new ArrayList<>();
        subcategoryColors = new HashMap<>();
    }

    private CategoryGroupData factsCategoryGroupData(){

        String factsName = MapointApp.getContext().getResources().getString(R.string.category_facts_title);
        boolean isFactsEnabled = readFactsEnabled();
        CategoryData categoryData = new CategoryData(FACT_CATEGORY_ITEM_ID, factsName, FACT_CATEGORY_ID, isFactsEnabled, FACTS_COLOR);

        List<CategoryData> items = new ArrayList<>();
        items.add(categoryData);

        return new CategoryGroupData(FACT_CATEGORY_ID,
                factsName,
                items,
                FACTS_COLOR,
                false,
                new CategoryIconPathProvider("", R.drawable.ic_fact));
    }

    public static CategoriesManager instance() {
        return _instanceHolder.instance();
    }

    public Observable<Either<List<CategoryGroupData>, Either<CommunicationError, CategoriesMapper.CategoriesHandledError>>>
    updateCategories() {
        return categoriesApiService.getCategories()
                .observeOn(AndroidSchedulers.mainThread())
                .map(listEitherEither -> {
                    if (listEitherEither.isLeft()) {
                        List<CategoryGroupData> groups = listEitherEither.left().get();
                        Set<String> selectedCategories = readSelectedCategories();
                        actualizeSelectedCategories(groups, selectedCategories);
                    }
                    return listEitherEither;
                });
    }

    public List<CategoryGroupData> getLocalCategories() {
        return categoriesGroup;
    }

    private void actualizeSelectedCategories(List<CategoryGroupData> allCategories, Set<String> selectedCategories) {
        Set<String> actualSelectedCategories = new HashSet<>();

        List<Integer> colors = new ArrayList<>();
        colors.add(ADD_EVENT_COLOR);

        subcategoryColors = new HashMap<>();
        subcategoryColors.put(ADD_EVENT_CATEGORY_ITEM_ID, ADD_EVENT_COLOR);

        allCategories.add(0, factsCategoryGroupData());

        for (CategoryGroupData groupData : allCategories) {
            for (CategoryData categoryData : groupData.getSubcategoryList()) {
                String categoryDataId = categoryData.getId();
                if (selectedCategories.contains(categoryDataId)) {
                    actualSelectedCategories.add(categoryDataId);
                    categoryData.setSelected(true);
                }

                subcategoryColors.put(categoryData.getName(), categoryData.getColor());
            }

            colors.add(groupData.getColor());
        }

        categoriesGroup = new ArrayList<>(allCategories);
        saveSelectedCategories(actualSelectedCategories);

        MarkerBitmapProvider.instance().recreateBitmapsByColors(colors);
    }

    public List<CategoryData> itemsInGroup(String groupId) {
        for (CategoryGroupData group : categoriesGroup) {
            if (group.getId().equals(groupId)) {
                return group.getSubcategoryList();
            }
        }

        return new ArrayList<>();
    }

    public Set<String> readSelectedCategories() {
        return sharedPreferencesProvider.readSelectedCategories();
    }

    public void saveSelectedCategories(final Set<String> selectedCategories) {
        sharedPreferencesProvider.saveSelectedCategories(selectedCategories);
        sharedPreferencesProvider.saveNumber(selectedCategories.size());
        onSelectedCategoriesChanged.onNext(selectedCategories);
    }

    public Boolean readFactsEnabled(){
        return sharedPreferencesProvider.readFactEnabledFlag();
    }

    public void saveFactsEnabled(final boolean factsEnabled) {
        sharedPreferencesProvider.updateFactEnabledFlag(factsEnabled);
        onSelectedCategoriesChanged.onNext(null);
    }

    public Observable<Set<String>> onSelectedCategoriesChanged() {
        return onSelectedCategoriesChanged;
    }

    public void updateLocaleSubcategory(String subcategoryId, boolean isChecked) {
        for (CategoryGroupData group : categoriesGroup) {

            List<CategoryData> subcategories = group.getSubcategoryList();
            for (CategoryData subcategoryData : subcategories) {
                if(subcategoryData.getId().equals(subcategoryId)) {
                    subcategoryData.setSelected(isChecked);
                }
            }
        }
    }

    public Bitmap coloredBitmapForSubcategories(List<String> subcategories) {
        int size = subcategories.size();

        Integer subcategoryColor = 0;
        if (size == 1) {
            String id = subcategories.get(0);
            Integer subcategoryColorNew = subcategoryColors.get(id);
            subcategoryColor = subcategoryColorNew != null ? subcategoryColorNew : 0;
        }

        return MarkerBitmapProvider.getBitmapByColor(subcategoryColor);
    }
}
