package net.mapoint.tracker;

import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;

import com.evernote.android.job.Job;
import com.evernote.android.job.JobRequest;

import net.mapoint.BuildConfig;
import net.mapoint.utils.Constants;
import net.mapoint.utils.LogUtils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class UpdateEventsNotificationJob extends Job {

    public static final String TAG = "update_notification_job";
    static SimpleDateFormat localShortDateFormat = new SimpleDateFormat("hh:mm:ss");

    @Override
    @NonNull
    protected Result onRunJob(Params params) {
        Context context = getContext();
        NewEventNotificator newEventNotificator = new NewEventNotificator(context);

        String text = "on run job : " + params.getId() + " : " + localShortDateFormat.format(new Date());

        newEventNotificator.sendDebugNotification(getContext(),
                Constants.NOTIFICATION_ID_ON_JOB_TEST,
                "0001",
                text);

        new GpsTracker().trackNewLocation(context, newLocation -> newEventNotificator.updateNotificationForLocation(newLocation));

        return Result.SUCCESS;
    }

    public static void scheduleJob() {

        long min = JobRequest.MIN_INTERVAL;
        long flex = JobRequest.MIN_FLEX;

        if (BuildConfig.DEBUG && Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            min = 2 * 60 * 1000;
            flex = 30 * 1000;
        }

        LogUtils.log("TESTING", "scheduleJob id : min : " + String.valueOf(min)
        + " : flex : " + String.valueOf(flex));

        int jobId = new JobRequest.Builder(UpdateEventsNotificationJob.TAG)
                .setPeriodic(min, flex)
                .setUpdateCurrent(true)
                .build()
                .schedule();

        LogUtils.log("TESTING", "scheduleJob id : " + jobId);
    }
}
