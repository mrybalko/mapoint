package net.mapoint.tracker;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import net.mapoint.utils.Constants;

import rx.functions.Action1;

public class GpsTracker {

    private static final String TAG = "GpsTracker";

    private LocationListener locationListener;

    public void trackNewLocation(Context context, Action1<Location> newLocationProvider) {
        locationListener = newLocation -> {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, locationListener);
            if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
                Log.v(TAG, "GoogleApiClient was disconnect.");
                mGoogleApiClient.disconnect();
            }

            newLocationProvider.call(newLocation);
        };

        startGoogleApiClient(context);
    }

    private GoogleApiClient mGoogleApiClient;

    private synchronized void startGoogleApiClient(Context context) {
        if(mGoogleApiClient == null){
            mGoogleApiClient = new GoogleApiClient.Builder(context)
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks(){

                        @Override
                        public void onConnected(@Nullable Bundle bundle) throws SecurityException {
                            Log.i(TAG, "Connection connected");
                            registerRequestUpdate();
                        }

                        @Override
                        public void onConnectionSuspended(int i) {
                            Log.i(TAG, "Connection suspended");
                            mGoogleApiClient.connect();
                        }
                    })
                    .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                        @Override
                        public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                            Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + connectionResult.getErrorCode());
                        }
                    })
                    .addApi(LocationServices.API)
                    .build();
            mGoogleApiClient.connect();
        }else{
            mGoogleApiClient.connect();
        }
    }

    private void registerRequestUpdate() throws SecurityException {
        LocationRequest mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(Constants.LOCATION_UPDATE_INTERVAL_IN_MILLISECONDS_BACKGROUND)
                .setFastestInterval(Constants.LOCATION_UPDATE_INTERVAL_IN_MILLISECONDS_BACKGROUND)
                .setSmallestDisplacement(Constants.LOCATION_UPDATE_DISTANCE_IN_METERS)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, locationListener);
    }
}
