package net.mapoint.tracker;

import android.app.ActivityManager;
import android.app.KeyguardManager;
import android.content.Context;
import android.location.Location;
import android.util.Log;

import com.crashlytics.android.Crashlytics;

import net.mapoint.data.LocationsDetailsData;
import net.mapoint.data.locations.EventData;
import net.mapoint.data.locations.EventType;
import net.mapoint.data.locations.InnerLocationData;
import net.mapoint.data.locations.LocationData;
import net.mapoint.data.locations.SimpleLocation;
import net.mapoint.managers.LocationManager;
import net.mapoint.managers.LoggingManager;
import net.mapoint.managers.SettingsManager;
import net.mapoint.ui.main.location.LocationUtils;
import net.mapoint.utils.Constants;
import net.mapoint.utils.LogUtils;
import net.mapoint.utils.notification.NotificationHelper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class NewEventNotificator {

    private static final String TAG = "NewEventNotificator";

    private NotificationHelper notificationHelper;
    private Context context;

    private Subscription mainThreadEmulationSubscription;
    private Subscription locationsDetailsSubscription;
    private Subscription locationsRadiusChangedSubscription;

    public NewEventNotificator() {
    }

    public NewEventNotificator(Context context) {
        notificationHelper = new NotificationHelper(context);
        this.context = context;
    }

    public void updateNotificationForLocation(Location newLocation) {
        if (newLocation != null) {
            String text = "on location changed : " + newLocation.toString();

            sendDebugNotification(context,
                    Constants.NOTIFICATION_ID_ON_LOCATION_CHANGED,
                    "0002",
                    text);

            mainThreadEmulationSubscription =
                    Observable.just("")
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(AndroidSchedulers.mainThread())
                            .subscribe((value) -> {
                                        SettingsManager settingsManager = SettingsManager.instance();

                                        GPSTrackerData gpsTrackerData = new GPSTrackerData(
                                                settingsManager.lastLocation(),
                                                settingsManager.lastNotificationTime(),
                                                settingsManager.notificationSoundEnabled(),
                                                Constants.LOCATION_UPDATE_DISTANCE_IN_METERS);
                                        tryUpdateNotification(gpsTrackerData, newLocation);

                                        mainThreadEmulationSubscription.unsubscribe();
                                        mainThreadEmulationSubscription = null;
                                    },
                                    throwable -> {
                                        mainThreadEmulationSubscription.unsubscribe();
                                        mainThreadEmulationSubscription = null;
                                        LogUtils.log(TAG, ".updateNotificationForLocation", throwable);
                                    });

        } else {
            Log.e(TAG, "Location no detected.");
        }
    }

    private void tryUpdateNotification(GPSTrackerData gpsTrackerData, Location newLocation) {
        if (gpsTrackerData != null) {
            SimpleLocation lastLocation = gpsTrackerData.lastLocation();
            int minDistance = gpsTrackerData.minDistance();
            long lastTime = gpsTrackerData.lastTime();
            boolean isNotificationSoundEnabled = gpsTrackerData.isNotificationSoundEnabled();

            boolean isInBackground = serviceIsRunningInForeground(context);

            boolean isLocationChanged = notificationHelper.isLocationChanged(newLocation,
                    lastLocation,
                    minDistance);

            boolean needShowNotification = notificationHelper.needShowNotification(newLocation, lastLocation, lastTime, minDistance);

            String text = "check can show notification" +
                    "\n\r : last location : " + ((lastLocation != null) ? lastLocation.toString() : " null ") +
                    "\n\r : min distance : " + String.valueOf(minDistance) +
                    "\n\r : last time : " + String.valueOf(lastTime) +
                    "\n\r : isInBackground : " + String.valueOf(isInBackground) +
                    "\n\r : isLocationChanged : " + String.valueOf(isLocationChanged) +
                    "\n\r : needShowNotification : " + String.valueOf(needShowNotification);

            sendDebugNotification(context,
                    Constants.NOTIFICATION_ID_CHECK_CAN_SHOW_NOTIFICAION,
                    "0003",
                    text);

            if (isInBackground && needShowNotification) {
                String getLocationsText = "get locations : for : " + newLocation.toString();
                sendDebugNotification(context,
                        Constants.NOTIFICATION_ID_CAN_SHOW_NOTIFICAION,
                        "0004",
                        getLocationsText);

                getNewPlaces(newLocation,
                        locations -> {
                            updateNotification(locations, isNotificationSoundEnabled);
                            SettingsManager settingsManager = SettingsManager.instance();
                            settingsManager.updateLastNotificationTime(System.currentTimeMillis());
                            settingsManager.updateLastLocation(new SimpleLocation(newLocation.getLatitude(), newLocation.getLongitude()));
                        });
            }
        }
    }

    private void clearNewLocationDetailsSubscription() {
        locationsDetailsSubscription.unsubscribe();
        locationsDetailsSubscription = null;
    }

    private void clearLocationsRadiusChangedSubscription() {
        locationsRadiusChangedSubscription.unsubscribe();
        locationsRadiusChangedSubscription = null;
    }

    public class LocationDetailsSubscriber extends Subscriber<LocationsDetailsData> {

        private Action1<List<LocationData>> showNotificationAction;

        public LocationDetailsSubscriber(final Action1<List<LocationData>> showNotificationAction) {
            this.showNotificationAction = showNotificationAction;
        }

        @Override
        public void onCompleted() {
            clearNewLocationDetailsSubscription();
        }

        @Override
        public void onError(Throwable throwable) {
            LogUtils.log(TAG, ".tryGetNewPlaces", throwable);
            clearNewLocationDetailsSubscription();
        }

        @Override
        public void onNext(LocationsDetailsData data) {
            List<LocationData> locations = data.locationData();

            if (showNotificationAction != null) {
                showNotificationAction.call(locations);
            }
            clearNewLocationDetailsSubscription();
        }
    }

    private void getNewPlaces(final Location location,
                              final Action1<List<LocationData>> showNotificationAction) {

        LocationManager locationManager = LocationManager.instance();

        locationsDetailsSubscription =
                locationManager.locationsDetailsDataCleanObservable()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .first()
                        .subscribe(new LocationDetailsSubscriber(showNotificationAction));

        locationsRadiusChangedSubscription = locationManager.locationRadiusChangedObservable()
                .subscribe(new Subscriber<Void>() {
                    @Override
                    public void onCompleted() {
                        clearLocationsRadiusChangedSubscription();
                    }

                    @Override
                    public void onError(Throwable e) {
                        LogUtils.log(TAG, ".locationRadiusChangedObservable", e);
                        clearLocationsRadiusChangedSubscription();
                    }

                    @Override
                    public void onNext(Void aVoid) {
                        clearLocationsRadiusChangedSubscription();
                    }
                });

        LocationManager.instance().updateCurrentLocation(location);
    }

    private void updateNotification(final List<LocationData> locations,
                                    final boolean isNotificationSoundEnabled) {

        Func1<Integer, String> stringResourcesProvider = resId -> context.getResources().getString(resId);
        if (locations != null && locations.size() > 0) {
            EventData eventData = LocationUtils.findInfoForEvent(locations, stringResourcesProvider);

            logTestingData(eventData, locations);

            notificationHelper.updateNotification(eventData,
                    isNotificationSoundEnabled,
                    stringResourcesProvider);
        }
    }

    private void logTestingData(EventData eventData, final List<LocationData> locations) {
        if (eventData != null) {
            String eventDataDescription = " event id : " + eventData.getId() +
                    "\n\r : text : " + eventData.getText();

            Set<String> seenEvents = LocationManager.instance().seenEvents();

            String getLocationsText = "locations size : " + String.valueOf(locations.size()) +
                    "\n\r seen events : " + LogUtils.setToString(seenEvents) +
                    eventDataDescription;
            sendDebugNotification(context,
                    Constants.NOTIFICATION_ID_FIND_EVENT_DATA,
                    "0005",
                    getLocationsText);
        }
    }

    boolean serviceIsRunningInForeground(Context context) {
        ActivityManager.RunningAppProcessInfo myProcess = new ActivityManager.RunningAppProcessInfo();
        ActivityManager.getMyMemoryState(myProcess);
        if (myProcess.importance != ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND)
            return true;

        KeyguardManager km = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
        // app is in foreground, but if screen is locked show notification anyway
        return km.inKeyguardRestrictedInputMode();
    }


    public void sendDebugNotification(Context context,
                                      int notificationId,
                                      String eventId,
                                      String eventText) {
        sendDebugNotification(context,
                notificationId,
                "test location name",
                "test location type",
                eventId,
                eventText);
    }

    public void sendDebugNotification(Context context,
                                      int notificationId,
                                      String locationName,
                                      String locationType,
                                      String eventId,
                                      String eventText) {

        Subscription subscription =
                Observable.just("")
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(AndroidSchedulers.mainThread())
                        .subscribe((value) -> {
                                    LoggingManager.instance().logMessage(eventText);

                                    NotificationHelper notificationHelper = new NotificationHelper(context);

                                    InnerLocationData innerLocationDataTest = new InnerLocationData.Builder()
                                            .name(locationName)
                                            .type(locationType)
                                            .build();

                                    EventData.Builder builder = new EventData.Builder()
                                            .id(eventId)
                                            .text(eventText)
                                            .innerLocationData(innerLocationDataTest)
                                            .eventType(EventType.Test);

                                   /* notificationHelper.updateNotification(
                                            notificationId,
                                            builder.build(),
                                            true,
                                            (integer -> context.getResources().getString(integer)));*/

                                    int tempNotificationId = notificationId;
                                    clearNotificationFor(tempNotificationId);
                                },
                                throwable -> {
                                    int tempNotificationId = notificationId;
                                    clearNotificationFor(tempNotificationId);
                                    LogUtils.log(TAG, ".updateNotificationForLocation", throwable);
                                });

        addSubscription(notificationId, subscription);
    }

    private void clearNotificationFor(int id) {
        Subscription subscription = subcriptions.remove(id);
        try {
            subscription.unsubscribe();
            subscription = null;
            LogUtils.log("TESTING", "clear notification : " + String.valueOf(id));

        } catch (Exception exception) {
            Crashlytics.logException(exception);
        }
    }

    private void addSubscription(int notificationId, Subscription subscription) {
        subcriptions.put(notificationId, subscription);
    }

    private static Map<Integer, Subscription> subcriptions = new HashMap<>();
}
