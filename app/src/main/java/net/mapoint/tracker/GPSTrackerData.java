package net.mapoint.tracker;

import net.mapoint.data.locations.SimpleLocation;

import java.io.Serializable;

public class GPSTrackerData implements Serializable {

    private SimpleLocation lastLocation;
    private long lastTime;
    private boolean isNotificationSoundEnabled;
    private int minDistance;

    public GPSTrackerData(SimpleLocation lastLocation,
                          long lastTime,
                          boolean isNotificationSoundEnabled,
                          int minDistance) {
        this.lastLocation = lastLocation;
        this.lastTime = lastTime;
        this.isNotificationSoundEnabled = isNotificationSoundEnabled;
        this.minDistance = minDistance;
    }

    public SimpleLocation lastLocation() {
        return lastLocation;
    }

    public long lastTime() {
        return lastTime;
    }

    public boolean isNotificationSoundEnabled() {
        return isNotificationSoundEnabled;
    }

    public int minDistance() {
        return minDistance;
    }

    @Override
    public String toString() {
        return "GPSTrackerData{" +
                "lastLocation=" + lastLocation +
                ", lastTime=" + lastTime +
                ", isNotificationSoundEnabled=" + isNotificationSoundEnabled +
                ", minDistance=" + minDistance +
                '}';
    }
}
