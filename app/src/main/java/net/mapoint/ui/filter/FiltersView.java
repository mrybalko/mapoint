package net.mapoint.ui.filter;

import android.content.DialogInterface;

import net.mapoint.data.LanguageItem;
import net.mapoint.data.PeriodItem;
import net.mapoint.data.category.CategoryData;
import net.mapoint.utils.Updatable;

import java.util.List;

public interface FiltersView extends Updatable {

    void showRadiusChooser(final String value);

    void updateRadiusValue(final String value);

    void updateEnableNotificationSoundView(boolean isEnable);

    void goToCategories();

    void updateSelectedCategories(final List<CategoryData> categories);

    void updatePeriodItem(PeriodItem item);
    void updateLanguageItem(LanguageItem item);

    void showInfoDialog(final String positiveText,
                        final DialogInterface.OnClickListener onPositiveListener);

    void onLanguageChanged();
}
