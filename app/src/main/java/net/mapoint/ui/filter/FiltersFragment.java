package net.mapoint.ui.filter;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;
import android.widget.TextView;

import com.nex3z.flowlayout.FlowLayout;

import net.mapoint.R;
import net.mapoint.data.LanguageItem;
import net.mapoint.data.PeriodItem;
import net.mapoint.data.category.CategoryData;
import net.mapoint.ui.TextViewStroke;
import net.mapoint.ui.base.BaseFragment;
import net.mapoint.utils.ToggleGroup;

import java.util.Arrays;
import java.util.List;

import rx.functions.Action1;
import rx.functions.Func1;

public class FiltersFragment extends BaseFragment<FiltersPresenter> implements FiltersView {

    private View rootView;

    private TextView radiusValueTv;
    private CheckedTextView enableNotificationSound;

    private FlowLayout selectedCategoriesFl;
    private View categoriesContentHint;

    private TextView notificationSettingsBtn;

    private ToggleGroup<PeriodItem> periodItems;
    private ToggleGroup<LanguageItem> languageItems;

    @Override
    public int getLayoutResId() {
        return R.layout.fr_filters;
    }

    @Override
    public FiltersPresenter presenterProvider() {
        return new FiltersPresenter(this);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        rootView = super.onCreateView(inflater, container, savedInstanceState);
        initViews();
        return rootView;
    }

    void initViews() {
        radiusValueTv = rootView.findViewById(R.id.settings_radius_value);
        radiusValueTv.setOnClickListener(view -> presenter().onChooseRadiusClicked());

        enableNotificationSound = rootView.findViewById(R.id.sound_enabled);
        enableNotificationSound.setOnClickListener(view -> {
            boolean isChecked = enableNotificationSound.isChecked();
            presenter().onEnableNotificationSoundChanged(!isChecked);
        });

        notificationSettingsBtn = (TextView)rootView.findViewById(R.id.notification_settings_change);
        notificationSettingsBtn.setOnClickListener(v -> {
            Context context = getActivity();
            Intent intent = new Intent();
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                intent.setAction("android.settings.APP_NOTIFICATION_SETTINGS");
                intent.putExtra("android.provider.extra.APP_PACKAGE", context.getPackageName());

            }else if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
                intent.setAction("android.settings.APP_NOTIFICATION_SETTINGS");
                intent.putExtra("app_package", context.getPackageName());
                intent.putExtra("app_uid", context.getApplicationInfo().uid);

            }else {
                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                intent.addCategory(Intent.CATEGORY_DEFAULT);
                intent.setData(Uri.parse("package:" + context.getPackageName()));
            }

            context.startActivity(intent);
        });

        notificationSettingsBtn.setVisibility(View.VISIBLE);

        initCategories();
        categoriesContentHint = rootView.findViewById(R.id.categories_content_hint);
        initPeriodItems();
        intiLanguageItems();
    }

    private void initCategories() {
        rootView.findViewById(R.id.categories_content_holder).setOnClickListener(view -> {
            presenter().onCategoriesClicked();
        });

        selectedCategoriesFl = rootView.findViewById(R.id.categories_content);
    }

    private void initPeriodItems() {
        periodItems = new ToggleGroup<>();
        ViewGroup holder = (ViewGroup) rootView.findViewById(R.id.period_items_chooser_holder);
        holder.removeAllViews();

        periodItems.itemSidePadding(getResources().getDimensionPixelSize(R.dimen.padding_smaller));
        periodItems.updateTypes(
                holder,
                Arrays.asList(
                        PeriodItem.Day,
                        PeriodItem.Week,
                        PeriodItem.Month),
                null);
        periodItems.setSingleSelection(true);
        periodItems.setOnSelectionChangedListener(() -> presenter().onPeriodItemChanged(periodItems.getSingleSelection()));
    }

    private void intiLanguageItems() {
        languageItems = new ToggleGroup<>();
        ViewGroup holder = (ViewGroup) rootView.findViewById(R.id.language_items_chooser_holder);
        holder.removeAllViews();

        languageItems.itemSidePadding(getResources().getDimensionPixelSize(R.dimen.padding_smaller));
        languageItems.updateTypes(
                holder,
                Arrays.asList(
                        LanguageItem.Ru,
                        LanguageItem.En),
                null);
        languageItems.setSingleSelection(true);
        languageItems.setOnSelectionChangedListener(() -> presenter().onLanguageChanged(languageItems.getSingleSelection()));
    }

    private float stringMetersToFloatKm(final String value) {
        if (TextUtils.isEmpty(value)) {
            return 0;
        }
        return (float) Integer.parseInt(value)/1000;
    }

    @Override
    public void showRadiusChooser(final String radiusValue) {
        String[] valuesArray = getResources().getStringArray(R.array.markers_area_radiuses);
        showSingleSelectionAlert(
                getString(R.string.radius_title),
                radiusValue,
                valuesArray,
                (newValue) -> presenter().onRadiusChanged(newValue),
                (itemValue) -> {
                    float valueForUI = stringMetersToFloatKm(itemValue);
                    return String.valueOf(valueForUI > 1 ? (int)valueForUI : valueForUI);
                });
    }

    @Override
    public void updateRadiusValue(String value) {
        radiusValueTv.setText(getString(R.string.settings_radius_value, String.valueOf(stringMetersToFloatKm(value))));
    }

    @Override
    public void updateEnableNotificationSoundView(boolean isEnable) {
        if (enableNotificationSound != null) {
            enableNotificationSound.setChecked(isEnable);
        }
    }

    @Override
    public void goToCategories() {
        navigator().goToCategories(getActivity());
    }

    private void showSingleSelectionAlert(
            final String title,
            final String selectedValue,
            final String[] items,
            final Action1<String> onSelectedItem,
            final Func1<String, String> itemConverter) {
        int stringsArraySize = items.length;
        String[] newItems = new String[stringsArraySize];

        int startSelectedPosition = 0;
        for (int i=0; i<stringsArraySize; i++) {

            if(items[i].equals(selectedValue)) {
                startSelectedPosition = i;
            }

            if (itemConverter != null) {
                newItems[i] = itemConverter.call(items[i]);
            }
        }

        new AlertDialog.Builder(getActivity())
                .setTitle(title)
                .setSingleChoiceItems(newItems, startSelectedPosition, null)
                .setPositiveButton(R.string.OK, (dialog, whichButton) -> {
                    int selectedPosition = ((AlertDialog)dialog).getListView().getCheckedItemPosition();

                    onSelectedItem.call(items[selectedPosition]);
                    dialog.dismiss();
                })
                .setNegativeButton(R.string.cancel, (dialogInterface, i) -> dialogInterface.dismiss())
                .show();
    }

    @Override
    public void updateSelectedCategories(final List<CategoryData> categories) {

        selectedCategoriesFl.removeAllViews();
        for(CategoryData category : categories) {
            selectedCategoriesFl.addView(createTextView(category));
        }
        categoriesContentHint.setVisibility(categories.size() > 0 ? View.GONE : View.VISIBLE);
    }

    private TextViewStroke createTextView(CategoryData categoryData) {
        TextViewStroke textViewStroke = new TextViewStroke(getContext());
        textViewStroke.setTextColor(getResources().getColor(R.color.text_color_dark));
        textViewStroke.setText(categoryData.getName());
        textViewStroke.setSelected(categoryData.isSelected());
        textViewStroke.setColors(categoryData.getColor(), categoryData.getColor());
        return textViewStroke;
    }

    @Override
    public void updatePeriodItem(PeriodItem item) {
        periodItems.updateSelections(Arrays.asList(item));
    }

    @Override
    public void updateLanguageItem(LanguageItem item) {
        languageItems.updateSelections(Arrays.asList(item));
    }

    @Override
    public void update() {
        presenter().updateModel();
    }

    @Override
    public void showInfoDialog( final String positiveText,
                                final DialogInterface.OnClickListener onPositiveListener) {

        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
        builder.setView(R.layout.v_invitation);

        if (onPositiveListener != null) {
            builder.setPositiveButton(positiveText, onPositiveListener);
        }

        Dialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onLanguageChanged() {
        getActivity().recreate();
        initPeriodItems();
    }
}
