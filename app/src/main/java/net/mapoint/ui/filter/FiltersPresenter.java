package net.mapoint.ui.filter;

import net.mapoint.R;
import net.mapoint.analytics.BaseAnalyticsTracker;
import net.mapoint.data.LanguageItem;
import net.mapoint.data.PeriodItem;
import net.mapoint.data.category.CategoryData;
import net.mapoint.data.category.CategoryGroupData;
import net.mapoint.managers.CategoriesManager;
import net.mapoint.managers.SettingsManager;
import net.mapoint.ui.base.BasePresenter;
import net.mapoint.utils.LocaleUtils;
import net.mapoint.utils.LogUtils;
import net.mapoint.utils.storage.SharedPreferencesProvider;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class FiltersPresenter extends BasePresenter<FiltersView> {

    private static final String TAG = "FiltersPresenter";

    private String selectedRadius;

    private SettingsManager settingsManager;
    private CategoriesManager categoriesManager;
    private SharedPreferencesProvider preferencesProvider;

    public FiltersPresenter(FiltersView view) {
        super(view);
        settingsManager = SettingsManager.instance();
        categoriesManager = CategoriesManager.instance();
        preferencesProvider = SharedPreferencesProvider.instance();
    }

    @Override
    public void updateModel() {
        initRadiusSelection();
        initSoundEnableView();
        updateSelectedCategories();
        updatePeriodItem();
        updateLanguageItem();

        tryShowInviation();
    }

    /////// radius
    private void initRadiusSelection() {
        updateRadiusView();
        addSubscription(settingsManager.onRadiusChanged()
                .subscribe(newRadius -> {
                            selectedRadius = newRadius;
                            updateRadiusView();
                        },
                        throwable -> LogUtils.log(TAG, ".onRadiusChanged", throwable)));
    }

    void onChooseRadiusClicked() {
        runIfView(view -> view.showRadiusChooser(selectedRadius));
    }

    void onRadiusChanged(final String value) {
        settingsManager.updateRadiusLocation(value);
        BaseAnalyticsTracker.instance().onFiltersRadiusChanged(value);
    }

    private void updateRadiusView() {
        runIfView(view -> view.updateRadiusValue(selectedRadius != null ? selectedRadius : ""));
    }

    private void initSoundEnableView() {
        boolean isEnable = settingsManager.notificationSoundEnabled();
        runIfView(view -> view.updateEnableNotificationSoundView(isEnable));
    }

    void onEnableNotificationSoundChanged(boolean isEnable) {
        settingsManager.updateNotificationSoundEnabled(isEnable);
        runIfView(view -> view.updateEnableNotificationSoundView(isEnable));
    }

    void onCategoriesClicked() {
        runIfView(view -> view.goToCategories());
    }

    void updateSelectedCategories() {
        addSubscription(categoriesManager.onSelectedCategoriesChanged()
                .subscribe(selectedCategoriesIds -> updateSelectedCategoriesViews(selectedCategoriesIds),
                        throwable -> LogUtils.log(TAG, ".getLocalCategories", throwable)));

        addSubscription(categoriesManager
                .updateCategories()
                .subscribe(either -> {} ,
                        throwable -> LogUtils.log(TAG, ".updateCategories", throwable)));
    }

    private void updateSelectedCategoriesViews(final Set<String> selectedCategoriesIds) {
        List<CategoryData> selectedCategories = new ArrayList<>();
        List<CategoryGroupData> categoryGroupData = categoriesManager.getLocalCategories();

        for (String selectedId : selectedCategoriesIds) {
            for (CategoryGroupData groupData : categoryGroupData) {
                List<CategoryData> subcategories = groupData.getSubcategoryList();
                if (subcategories != null) {
                    for (CategoryData categoryData : subcategories) {
                        if (selectedId.equals(categoryData.getId())) {
                            selectedCategories.add(categoryData);
                        }
                    }
                }
            }
        }

        runIfView(view -> view.updateSelectedCategories(selectedCategories));
    }

    private void updatePeriodItem() {
        PeriodItem item = settingsManager.periodItem();
        runIfView(view -> view.updatePeriodItem(item));
    }

    private void updateLanguageItem() {
        LanguageItem item = settingsManager.languageItem();
        runIfView(view -> view.updateLanguageItem(item));
    }

    void onPeriodItemChanged(PeriodItem item) {
        runIfView(view -> view.updatePeriodItem(item));
        settingsManager.updatePeriodItem(item);
        BaseAnalyticsTracker.instance().onFiltersTimeChanged(item);
    }

    void onLanguageChanged(LanguageItem languageItem) {
        settingsManager.updateLanguageItem(languageItem);
        LocaleUtils.changeAppLanguage(languageItem.realValue());
        runIfView(view -> {
            view.onLanguageChanged();
            view.updateLanguageItem(languageItem);
        });
    }

    private void tryShowInviation() {
        if (!preferencesProvider.categoriesChanged()) {
            runIfView(view -> view.showInfoDialog(resources().getString(R.string.OK),
                    (dialog, which) -> preferencesProvider.categoriesChanged(true)
            ));
            preferencesProvider.categoriesChanged(true);

        }
    }
}
