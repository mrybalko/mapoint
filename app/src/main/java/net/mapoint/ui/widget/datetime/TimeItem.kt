package net.mapoint.ui.widget.datetime

import android.app.TimePickerDialog
import android.content.Context
import android.support.constraint.ConstraintLayout
import android.text.TextUtils
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.TimePicker
import kotlinx.android.synthetic.main.nav_header_main.view.*
import kotlinx.android.synthetic.main.v_time_item.view.*
import net.mapoint.R
import java.text.SimpleDateFormat
import java.util.*

class TimeItem : ConstraintLayout {

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    private var inited: Boolean = false
    lateinit var onRemoveAction: () -> Unit
    lateinit var onAddAction: (String) -> Unit

    internal var localTimeFormat = SimpleDateFormat("HH:mm")

    init {
        if (!inited) {
            val inflater = LayoutInflater.from(context)
            inflater.inflate(R.layout.v_time_item, this, true)
            inited = true

            timeView.setOnClickListener({showTimePicker{dateValue -> timeView.text = dateValue}})

            addAction.setOnClickListener {
                val timeValue = timeView.text.toString()
                if (!TextUtils.isEmpty(timeValue)) {
                    timeView.text = ""
                    onAddAction.invoke(timeValue)
                } }

            removeAction.setOnClickListener { onRemoveAction.invoke() }
        }
    }

    fun showTimePicker(onTimeSet:(String) -> Unit) {
        val localCalendar = Calendar.getInstance()
        val hour = localCalendar.get(Calendar.HOUR_OF_DAY)
        val minute = localCalendar.get(Calendar.MINUTE)

        val timePicker = TimePickerDialog (context, object: TimePickerDialog.OnTimeSetListener {
            override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) {
                localCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
                localCalendar.set(Calendar.MINUTE, minute)
                onTimeSet.invoke(localTimeFormat.format(localCalendar.time))
            }
        }, hour, minute, true)

        timePicker.setTitle(resources.getString(R.string.select_time));
        timePicker.show();
    }

    fun showAddRemoveBtn(needShowAdd:Boolean, needShowRemove:Boolean) {
        addAction.visibility = if (needShowAdd) View.VISIBLE else View.GONE
        removeAction.visibility = if (needShowRemove) View.VISIBLE else View.GONE
    }

    fun setActionsActions(onRemoveAction: () -> Unit, onAddAction: (String) -> Unit) {
        this.onRemoveAction = onRemoveAction
        this.onAddAction = onAddAction
    }

    var value: String
        get() = timeView.text.toString()
        set(value) { timeView.text = value }
}