package net.mapoint.ui.widget.datetime

import android.app.DatePickerDialog
import android.content.Context
import android.support.constraint.ConstraintLayout
import android.text.TextUtils
import android.util.AttributeSet
import android.view.LayoutInflater
import net.mapoint.R
import kotlinx.android.synthetic.main.v_adding_date_time.view.*
import net.mapoint.data.adding.AddingEventDateTimeData
import java.text.SimpleDateFormat
import java.util.*

class AddingDateTimeWidget: ConstraintLayout {

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    private var inited: Boolean = false
    private lateinit var onAddClickedAction:(AddingEventDateTimeData) -> Unit

    internal var localDateFormat = SimpleDateFormat("dd MMM yyyy")
    var calendar:Calendar = Calendar.getInstance()

    init {
        if (!inited) {
            val inflater = LayoutInflater.from(context)
            inflater.inflate(R.layout.v_adding_date_time, this, true)
            inited = true

            startDateView.setOnClickListener({showDatePicker{dateValue -> startDateView.text = dateValue}})
            endDateView.setOnClickListener({showDatePicker{dateValue -> endDateView.text = dateValue}})

            addTimeAction.showAddRemoveBtn(true, false)
            addTimeAction.setActionsActions({}, {
                it -> run {
                    timesContainer.addView(addEditableTimeItem(it))
                }
            })

            addActionView.setOnClickListener { view -> run {
                val addingEventDateTimeData = collectData()

                if (!TextUtils.isEmpty(addingEventDateTimeData.startDate)) {
                    onAddClickedAction.invoke(addingEventDateTimeData)
                    clearViews()
                }
            }}
        }
    }

    fun clearViews() {
        timesContainer.removeAllViews()
        startDateView.text = ""
        endDateView.text = ""
        addTimeAction.value=""
    }

    fun addEditableTimeItem(value:String): TimeItem {
        val timeItem = TimeItem(context)
        timeItem.setActionsActions({timesContainer.removeView(timeItem)}, {})
        timeItem.showAddRemoveBtn(false, true)
        timeItem.value = value
        return timeItem
    }

    fun showDatePicker(onDataSet:(String) -> Unit) {
        val localCalendar = Calendar.getInstance()
        val year = localCalendar.get(Calendar.YEAR)
        val month = localCalendar.get(Calendar.MONTH)
        val day = localCalendar.get(Calendar.DAY_OF_MONTH)

        val datePickerDialog = DatePickerDialog(context,

                DatePickerDialog.OnDateSetListener { view, selectedYear, selectedMonth, selectedDay ->
                    localCalendar.set(selectedYear, selectedMonth, selectedDay)
                    onDataSet.invoke(localDateFormat.format(localCalendar.time))

                }, year, month, day)
        datePickerDialog.show()
    }

    fun collectData(): AddingEventDateTimeData {

        val startDateValue = startDateView.text.toString()
        val endDateValue = endDateView.text.toString()

        val times:MutableList<String> = mutableListOf()

        val count = timesContainer.childCount
        for (i in 0 until count) {
            val child = timesContainer.getChildAt(i) as TimeItem
            times.add(child.value)
        }

        if (!TextUtils.isEmpty(addTimeAction.value)){
            times.add(addTimeAction.value)
        }

        return AddingEventDateTimeData(startDate = startDateValue, endDate = endDateValue, times = times)
    }

    fun setOnAddClickedAction(onAddClickedAction:(AddingEventDateTimeData) -> Unit) {
        this.onAddClickedAction = onAddClickedAction
    }
}


