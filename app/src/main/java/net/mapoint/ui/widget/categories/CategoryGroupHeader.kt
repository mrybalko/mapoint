package net.mapoint.ui.widget.categories

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import net.mapoint.R
import net.mapoint.data.category.CategoryIconPathProvider
import net.mapoint.ui.CircleImageView

class CategoryGroupHeader : ConstraintLayout {

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    private var inited: Boolean = false

    private lateinit var imageView: CircleImageView
    private lateinit var titleView: TextView
    private lateinit var cursorView: ImageView

    private var isExpandedValue:Boolean = false

    init {
        if (!inited) {
            val inflater = LayoutInflater.from(context)
            inflater.inflate(R.layout.v_category_group_header, this, true)
            imageView = findViewById(R.id.headerImage)
            titleView = findViewById(R.id.headerTitle)
            cursorView = findViewById(R.id.cursorView)
            inited = true
        }
    }

    fun setTitle(message: String) {
        titleView.text = message
    }

    fun setIcon(iconProvider: CategoryIconPathProvider) {
        imageView.setIcon(iconProvider)
    }

    fun setSelectedColor(selectedColor: Int) {
        imageView.setSelectedColor(selectedColor)
    }

    fun showExpandedIndicators(selected: Boolean){
        cursorView.visibility = if(selected) View.VISIBLE else View.INVISIBLE
    }

    fun showCheckedIndicators(selected: Boolean) {
        imageView.isSelected = selected
        titleView.isSelected = selected
    }

    var isExpanded: Boolean
        get() = isExpandedValue
        set(value) {
            isExpandedValue = value
        }
}
