package net.mapoint.ui.main.location;


import com.google.android.gms.maps.OnMapReadyCallback;

import net.mapoint.data.locations.EventData;
import net.mapoint.data.locations.LocationData;

import java.util.List;

public interface LocationDetailsView extends OnMapReadyCallback {
    void updateInfo(LocationData locationData);
    void showContactsList(final List<String> phones);
    void goToEventDetails(EventData eventData);
}
