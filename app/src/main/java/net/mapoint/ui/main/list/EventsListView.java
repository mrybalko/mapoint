package net.mapoint.ui.main.list;

import android.location.Location;

import net.mapoint.data.locations.EventData;
import net.mapoint.utils.Updatable;

import java.util.List;

public interface EventsListView extends Updatable {

    void showEvents(final List<EventData> locations,
                    final Location centerLocation,
                    final float radius);

    void goToEventDetails(EventData locationData);

    void updateEmptyContentStub(boolean isEmptyContent);

    void goToAddingEvent();
}
