package net.mapoint.ui.main.map;

import android.location.Location;

import com.google.android.gms.maps.OnMapReadyCallback;

import net.mapoint.data.locations.LocationData;
import net.mapoint.utils.Updatable;

import java.util.List;

public interface MapFragmentView extends OnMapReadyCallback, Updatable {

    void showLocations(final List<LocationData> locations,
                       final Location centerLocation,
                       final float radius);

    void goToLocationDetails(LocationData locationData);

    void setCenterMapToLocation(Location location);

    void goToAddingEvent();
}
