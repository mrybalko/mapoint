package net.mapoint.ui.main.map;

import android.annotation.SuppressLint;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterManager;

import net.mapoint.R;
import net.mapoint.data.locations.EventData;
import net.mapoint.data.locations.LocationData;
import net.mapoint.ui.base.BaseFragment;
import net.mapoint.utils.location.LocationDataParser;

import java.util.List;

public class MapFragment extends BaseFragment<MapPresenter> implements MapFragmentView {

    private static final String TAG = "MapFragment";

    private MapView mapView;
    private GoogleMap googleMap;
    private ClusterManager<MarkerItem> clusterManager;
    private View rootView;

    private FloatingActionButton addingBtn;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public MapPresenter presenterProvider() {
        return new MapPresenter(this);
    }

    @Override
    public int getLayoutResId() {
        return R.layout.fr_map;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        rootView = super.onCreateView(inflater, container, savedInstanceState);
        initMap(rootView, savedInstanceState);
        addingBtn = rootView.findViewById(R.id.add_event);
        addingBtn.setOnClickListener(v -> presenter().onAddingEventClick());
        return rootView;
    }

    @Override
    public void goToAddingEvent() {
        navigator().goToAddingEvent(getActivity());
    }

    private void initMap(View rootView, @Nullable Bundle savedInstanceState) {
        mapView = rootView.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        mapView.getMapAsync(this);
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;

        int padding = (int)getResources().getDimension(R.dimen.map_controls_padding);

        googleMap.setPadding(0, padding, 0, padding);
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setCompassEnabled(false);
        googleMap.getUiSettings().setZoomGesturesEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        googleMap.getUiSettings().setMapToolbarEnabled(false);

        clusterManager = new ClusterManager<>(getActivity(), googleMap);
        googleMap.setOnCameraIdleListener(clusterManager);
        googleMap.setOnMarkerClickListener(clusterManager);
        googleMap.setOnInfoWindowClickListener(clusterManager);

        final MarkerRender renderer = new MarkerRender(getActivity(), googleMap, clusterManager,
                subcategories -> presenter().coloredBitmapForSubcategories(subcategories));
        clusterManager.setRenderer(renderer);

        clusterManager.setOnClusterClickListener(cluster -> false);
        clusterManager.setOnClusterItemClickListener(clusterItem -> false);

        clusterManager.getMarkerCollection().setOnInfoWindowAdapter(
                new MarkerInfoAdapter(resID -> getLayoutInflater().inflate(resID, null, false)));
        googleMap.setInfoWindowAdapter(clusterManager.getMarkerManager());

        clusterManager.setOnClusterItemInfoWindowClickListener(
                stringClusterItem -> {
                    String locationId = stringClusterItem.locationId();
                    if (!TextUtils.isEmpty(locationId)) {
                        presenter().onMarkerClicked(locationId);
                    }
                });

        googleMap.setOnInfoWindowClickListener(clusterManager);
        updateLocationEnabling(true);
        presenter().onMapReady();
    }

    @Override
    public void onResume() {
        super.onResume();
        updateLocationEnabling(true);
        mapView.onResume();
    }

    @Override
    public void showLocations(final List<LocationData> locations,
                              final Location centerLocation,
                              final float radius) {
        googleMap.clear();

        updateLocationMarkers(locations);
        updateCurrentLocationUI(centerLocation, radius);
    }

    @Override
    public void goToLocationDetails(LocationData locationData) {
        navigator().goToLocationDetails(getActivity(), locationData);
    }

    private void updateLocationMarkers(List<LocationData> locations) {

        if (locations != null && locations.size() > 0) {
            clusterManager.clearItems();
            for (LocationData locationData : locations) {
                LocationDataParser locationDataParser = new LocationDataParser(locationData);
                EventData eventData = locationDataParser.firstCorrectEvent(integer -> getString(integer));

                MarkerItem markerItem = new MarkerItem(eventData, resId -> getString(resId));
                clusterManager.addItem(markerItem);
            }

            clusterManager.cluster();
        }
    }

    private void updateCurrentLocationUI(Location location, final float radius) {
        LatLng myLocation = new LatLng(location.getLatitude(), location.getLongitude());
        CircleOptions circleOptions = new CircleOptions().center(myLocation)
                .radius(radius)
                .strokeColor(getResources().getColor(R.color.locations_radius))
                .strokeWidth(2)
                .fillColor(getResources().getColor(R.color.locations_radius_fill));

        googleMap.addCircle(circleOptions);
    }

    @Override
    public void setCenterMapToLocation(Location location) {
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                new LatLng(location.getLatitude(), location.getLongitude()
                ), 10.0f));
    }

    private void updateLocationEnabling(boolean enable) {
        checkAndRequestLocationPermission(rootView,
                () -> {
                    if (googleMap != null) {
                        googleMap.setMyLocationEnabled(enable);
                    }
                });
    }

    @Override
    public void onPause() {
        updateLocationEnabling(false);
        super.onPause();
    }

    @Override
    public void update() {
        presenter().updateModel();
        updateLocationEnabling(true);
    }
}