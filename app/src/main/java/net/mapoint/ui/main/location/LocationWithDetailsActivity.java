package net.mapoint.ui.main.location;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import net.cachapa.expandablelayout.ExpandableLayout;
import net.mapoint.R;
import net.mapoint.data.locations.EventData;
import net.mapoint.data.locations.EventDatesItemData;
import net.mapoint.data.locations.LocationData;
import net.mapoint.data.locations.LocationWorkingTimeData;
import net.mapoint.ui.base.BaseActivity;
import net.mapoint.ui.base.MapointApp;
import net.mapoint.utils.Constants;

import java.text.DateFormatSymbols;
import java.util.List;

public class LocationWithDetailsActivity extends BaseActivity<LocationDetailsPresenter> implements LocationDetailsView {

    private GoogleMap googleMap;

    private TextView locationTitleTv;
    private TextView nameTv;
    private TextView addressTv;
    private TextView typeTv;
    private ImageView contactsIv;
    private View locationCommonInfoV;
    private TextView workingTimeTv;
    private ExpandableLayout workingTimeContentHolderEl;

    private final DateFormatSymbols dfs = DateFormatSymbols.getInstance(MapointApp.locale());

    private static final String KEY_LOCATION_DETAILS = "location_details";
    private static final String KEY_EVENT_ID_FROM_NOTIFICATION = "event_id_from_notification";

    public static Intent newIntent(final Context context,
                                   LocationData locationDetails) {
        Intent intent = newIntent(context, locationDetails, null);
        return intent;
    }

    public static Intent newIntent(final Context context,
                                   LocationData locationDetails,
                                   String eventIdFromNotification) {
        Intent intent = new Intent(context, LocationWithDetailsActivity.class);
        intent.putExtra(KEY_LOCATION_DETAILS, locationDetails);

        if (eventIdFromNotification != null) {
            intent.putExtra(KEY_EVENT_ID_FROM_NOTIFICATION, eventIdFromNotification);
        }
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        initViews();

        tryMarkSeenEvent();
    }

    private void tryMarkSeenEvent() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String eventIdFromNotification = (String)extras.getSerializable(KEY_EVENT_ID_FROM_NOTIFICATION);
            if (eventIdFromNotification != null) {
                presenter().openedFromNotification(eventIdFromNotification);
            }
        }
    }

    private void initViews() {
        locationTitleTv = (TextView) findViewById(R.id.location_name);
        nameTv = (TextView) findViewById(R.id.name);
        addressTv = (TextView) findViewById(R.id.address);
        typeTv = (TextView) findViewById(R.id.type);
        contactsIv = (ImageView) findViewById(R.id.contacts);

        workingTimeContentHolderEl = (ExpandableLayout)findViewById(R.id.working_time_content_holder);
        workingTimeTv = findViewById(R.id.working_time_content);
        locationCommonInfoV = findViewById(R.id.location_common_info);

        findViewById(R.id.back_btn).setOnClickListener(view -> finish());
    }

    @Override
    public void updateInfo(final LocationData locationData) {
        updateMarker(locationData);
        updateLocationInfo(locationData);
    }

    private void updateMarker(final LocationData locationData) {
        MarkerOptions markerOptions = LocationUtils.markerOptionsFromLocationData(locationData,
                integer -> getString(integer),
                subcategories -> presenter().coloredBitmapForSubcategories(subcategories));

        markerOptions.anchor(0.5f, 0.5f);
        googleMap.clear();
        googleMap.addMarker(markerOptions);

        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                new LatLng(locationData.lat(), locationData.lng()
                ), 13.0f));
        googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                LinearLayout info = new LinearLayout(MapointApp.getContext());
                info.setOrientation(LinearLayout.VERTICAL);

                TextView title = new TextView(MapointApp.getContext());
                title.setTextColor(Color.BLACK);
                title.setGravity(Gravity.CENTER);
                title.setTypeface(null, Typeface.BOLD);
                title.setText(marker.getTitle());

                TextView snippet = new TextView(MapointApp.getContext());
                snippet.setTextColor(Color.GRAY);
                snippet.setText(marker.getSnippet());

                info.addView(title);
                info.addView(snippet);

                return info;
            }
        });
    }

    @Override
    public void showContactsList(List<String> phones) {

        permissionHandler().requestWithChecking(this,
                Manifest.permission.CALL_PHONE,
                Constants.REQUEST_CALL_PERMISSION,
                () -> navigator().callNumber(this, phones.get(0)),
                () -> {});
    }

    @Override
    public void goToEventDetails(EventData eventData) {
        navigator().goToEventDetails(this, eventData);
    }

    @Override
    public int getLayoutResId() {
        return R.layout.ac_location_details;
    }

    @Override
    public LocationDetailsPresenter presenterProvider() {
        return new LocationDetailsPresenter(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            LocationData locationData = (LocationData)extras.getSerializable(KEY_LOCATION_DETAILS);
            presenter().updateModel(locationData);
        }

        googleMap.getUiSettings().setZoomControlsEnabled(true);

        checkAndRequestLocationPermission(findViewById(R.id.root_view),
                () -> {
                    if (googleMap != null) {
                        googleMap.setMyLocationEnabled(true);
                    }
                });
    }

    private void updateLocationInfo(final LocationData locationData) {
        fillLocationCommonInfo(locationData);
        fillDetails(locationData);
    }

    private void fillLocationCommonInfo(final LocationData locationData) {

        if (!TextUtils.isEmpty(locationData.name())) {
            locationTitleTv.setText(locationData.type() + " " + locationData.name());
            fillField(nameTv, locationData.name());
        } else {
            locationTitleTv.setText("");
        }

        fillField(addressTv, locationData.address());
        fillField(typeTv, locationData.type());

        initContactsField(locationData.phones());

        List<LocationWorkingTimeData> locationWorkingTimeData = locationData.locationWorkingTimes();
        if (locationWorkingTimeData.size() > 0) {
            fillWorkingTime(locationWorkingTimeData);
            locationCommonInfoV.setOnClickListener(view -> {
                workingTimeContentHolderEl.toggle();
            });
        }
    }

    private void initContactsField(List<String> phones) {

        if (phones != null && phones.size() > 0) {
            contactsIv.setVisibility(View.VISIBLE);
            contactsIv.setOnClickListener(view -> presenter().onContactsClick(phones));
        }
    }

    private void fillWorkingTime(final List<LocationWorkingTimeData> times) {
        StringBuilder workingTimes = new StringBuilder();

        for (LocationWorkingTimeData time : times) {
            if (workingTimes.length() > 0) {
                workingTimes.append("\n");
            }

            String dayName = dfs.getShortWeekdays()[time.getDayNumber() % 7 + 1];

            workingTimes.append(getString(R.string.working_time, dayName, time.getStartTime(), time.getEndTime()));
        }

        workingTimeTv.setText(workingTimes.toString());
    }

    private void fillDetails(final LocationData locationData) {

        ViewGroup detailsHolder = findViewById(R.id.details_holder);

        List<EventData> offers = locationData.offers();
        if (offers != null && offers.size() > 0) {
            for (EventData offerData : offers) {
                detailsHolder.addView(createOfferView(offerData));
            }
        }

        List<EventData> facts = locationData.facts();
        if (facts != null && facts.size() > 0) {
            for (EventData factData : facts) {
                detailsHolder.addView(createFactView(factData));
            }
        }
    }

    private void fillField(final TextView textView,
                           final String value) {

        if (!TextUtils.isEmpty(value)) {
            textView.setText(value);
            textView.setVisibility(View.VISIBLE);
        }
    }

    private View createOfferView(final EventData offerData) {
        View offerView = getLayoutInflater().inflate(R.layout.v_offer_item, null);

        StringBuilder subcategories = new StringBuilder();
        for (String item : offerData.getSubcategories()) {
            if (subcategories.length() > 0) {
                subcategories.append("/");
            }

            subcategories.append(item);
        }

        TextView subcategoriesTv = offerView.findViewById(R.id.subcategories);
        fillField(subcategoriesTv, subcategories.toString());

        TextView description = offerView.findViewById(R.id.description);
        fillField(description, offerData.getText());

        TextView timeTv = offerView.findViewById(R.id.time);
        fillWorkingTime(timeTv, offerData.dates());

        offerView.setOnClickListener(view -> presenter().goToOfferDetails(offerData));

        return offerView;
    }

    private View createFactView(final EventData factData) {
        View factView = getLayoutInflater().inflate(R.layout.v_fact_item, null);
        TextView subcategoriesTv = factView.findViewById(R.id.subcategories);
        fillField(subcategoriesTv, getString(R.string.fact));

        TextView description = factView.findViewById(R.id.description);
        fillField(description, factData.getText());

        factView.setOnClickListener(view -> presenter().goToFactDetails(factData));

        return factView;
    }

    private void fillWorkingTime(TextView textView, List<EventDatesItemData> dates) {
        String workingTimes = LocationUtils.workingTimeIntervalFromDatesList(dates);

        if (!TextUtils.isEmpty(workingTimes)) {
            textView.setText(workingTimes);
            textView.setVisibility(View.VISIBLE);
        } else {
            textView.setVisibility(View.GONE);
        }
    }
}
