package net.mapoint.ui.main.categories;

import net.mapoint.data.category.CategoryGroupData;

import java.util.List;

public interface CategoriesView {

    void updateData(List<CategoryGroupData> groups, String selectedGroupId, int columnsNumber);
}
