package net.mapoint.ui.main;

import net.mapoint.managers.CategoriesManager;
import net.mapoint.managers.LocationManager;
import net.mapoint.ui.base.BasePresenter;
import net.mapoint.utils.LogUtils;
import net.mapoint.utils.storage.SharedPreferencesProvider;

public class MainPresenter extends BasePresenter<MainView> {

    private static final String TAG = "MainPresenter";

    private SharedPreferencesProvider preferencesProvider;
    private LocationManager locationManager;
    private CategoriesManager categoriesManager;

    public MainPresenter(MainView view) {
        super(view);
        preferencesProvider = SharedPreferencesProvider.instance();
        locationManager = LocationManager.instance();
        categoriesManager = CategoriesManager.instance();
    }

    public void onLayoutUpdated() {
        if (!preferencesProvider.categoriesChanged()) {
            runIfView(view -> view.startWithSettings());
        }
    }

    @Override
    public void updateModel() {
        super.updateModel();
        addSubscription(locationManager.locationRadiusChangedObservable()
                .subscribe((aVoid) -> {},
                        throwable -> LogUtils.log(TAG, ".locationRadiusChangedObservable", throwable)));

        addSubscription(categoriesManager.updateCategories()
                .subscribe(
                        (either) -> {},
                        throwable -> LogUtils.log(TAG, ".updateCategories", throwable)));
    }
}
