package net.mapoint.ui.main.categories.list;

import android.view.View;
import android.widget.Checkable;
import android.widget.CheckedTextView;

import com.thoughtbot.expandablecheckrecyclerview.viewholders.CheckableChildViewHolder;

import net.mapoint.R;

public class MultiCategoryVH extends CheckableChildViewHolder {

    private CheckedTextView childCheckedTextView;

    public MultiCategoryVH(View itemView) {
        super(itemView);
        childCheckedTextView = (CheckedTextView) itemView.findViewById(R.id.category_name);
    }

    @Override
    public Checkable getCheckable() {
        return childCheckedTextView;
    }

    public void setChecked(final boolean flag) {
        getCheckable().setChecked(flag);
    }

    public void setCategoryName(String name) {
        childCheckedTextView.setText(name);
    }
}
