package net.mapoint.ui.main.map;

import android.content.Context;
import android.graphics.Bitmap;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;

import net.mapoint.data.locations.MapMarkerData;

import java.util.List;

import rx.functions.Func1;

public class MarkerRender extends DefaultClusterRenderer<MarkerItem> {

    private Func1<List<String>, Bitmap> subcategoriesColorProvider;

    public MarkerRender(final Context context,
                        final GoogleMap map,
                        final ClusterManager<MarkerItem> clusterManager,
                        final Func1<List<String>, Bitmap> subcategoriesColorProvider) {
        super(context, map, clusterManager);
        this.subcategoriesColorProvider = subcategoriesColorProvider;
    }

    @Override
    protected void onBeforeClusterItemRendered(MarkerItem item, MarkerOptions markerOptions) {
        super.onBeforeClusterItemRendered(item, markerOptions);

        markerOptions
                .icon(BitmapDescriptorFactory.fromBitmap(subcategoriesColorProvider.call(item.subcategories())));

    }

    @Override
    protected void onClusterItemRendered(MarkerItem clusterItem, Marker marker) {
        super.onClusterItemRendered(clusterItem, marker);

        marker.setTag(new MapMarkerData(
                clusterItem.getSnippet(),
                clusterItem.getTitle(),
                clusterItem.subcategories(),
                clusterItem.dates()));
        marker.setAnchor(0.5f, 0.5f);
    }
}