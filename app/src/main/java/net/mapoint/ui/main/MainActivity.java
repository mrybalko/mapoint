package net.mapoint.ui.main;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import net.mapoint.R;
import net.mapoint.data.locations.EventData;
import net.mapoint.managers.LoggingManager;
import net.mapoint.tracker.UpdateEventsNotificationJob;
import net.mapoint.ui.base.BaseActivity;
import net.mapoint.ui.base.BaseFragment;
import net.mapoint.ui.filter.FiltersFragment;
import net.mapoint.ui.main.list.EventsListFragment;
import net.mapoint.ui.main.map.MapFragment;
import net.mapoint.ui.settings.SettingsFragment;
import net.mapoint.utils.Updatable;
import net.mapoint.utils.location.LocationUpdatesService;
import net.mapoint.utils.notification.NotificationHelper;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseActivity<MainPresenter> implements MainView {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ViewPagerAdapter adapter;

    private boolean isBounded = false;

    private ServiceConnection serviceConnection;

    @Override
    public int getLayoutResId() {
        return R.layout.activity_main;
    }

    @Override
    public MainPresenter presenterProvider() {
        return new MainPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        setupViewPager(viewPager);
        viewPager.addOnLayoutChangeListener((v, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom) -> presenter().onLayoutUpdated());

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                updateTab(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        initServiceConnection();

        UpdateEventsNotificationJob.scheduleJob();
    }

    private void updateTab(int position) {
        viewPager.setCurrentItem(position);
    }

    @Override
    protected void onStart() {
        super.onStart();

        checkAndRequestLocationPermission(findViewById(R.id.main_root),
                () -> bindService(new Intent(this, LocationUpdatesService.class),
                        serviceConnection,
                        Context.BIND_AUTO_CREATE));

        LoggingManager.instance().sendLoggedDataToServer();
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
    }

    //method for testing notification settings
    private void showTestNotification() {
        NotificationHelper notificationHelper = new NotificationHelper(getApplicationContext());

        notificationHelper.updateNotification(EventData.forTest(),
                true,
                (integer -> getString(integer)));
    }

    @Override
    protected void onStop() {

        if (isBounded) {
            unbindService(serviceConnection);
            isBounded = false;
        }

        viewPager.clearOnPageChangeListeners();
        super.onStop();
    }

    private void initServiceConnection() {
        serviceConnection = new ServiceConnection() {

            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                LocationUpdatesService.LocalBinder binder = (LocationUpdatesService.LocalBinder) service;
                LocationUpdatesService locationUpdatesService = binder.getService();
                isBounded = true;

                checkAndRequestLocationPermission(findViewById(R.id.main_root),
                        () -> locationUpdatesService.requestLocationUpdates());
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                isBounded = false;
            }
        };
    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new EventsListFragment(), getString(R.string.list_title), R.drawable.ic_tab_list_selector);
        adapter.addFragment(new MapFragment(), getString(R.string.map_title), R.drawable.ic_tab_location_selector);
        adapter.addFragment(new FiltersFragment(), getString(R.string.filter_title), R.drawable.ic_tab_filter_selector);
        //adapter.addFragment(new SettingsFragment(), getString(R.string.filters_title), R.drawable.ic_tab_person_selector);
        viewPager.setAdapter(adapter);
    }

    @Override
    public void startWithSettings() {
        updateTab(2);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> fragmentList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getItemPosition(Object object) {
            if (object instanceof Updatable) {
                ((Updatable) object).update();
            }
            return super.getItemPosition(object);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        public void addFragment(Fragment fragment, String title, int iconResId) {
            fragmentList.add(fragment);

            TabLayout.Tab tab = tabLayout.newTab();
            tab.setIcon(iconResId);
            tabLayout.addTab(tab);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabLayout.getTabAt(position).getText();
        }
    }

    @Override
    public void onBackPressed() {

        int currentIndex = viewPager.getCurrentItem();
        Fragment fragment = adapter.getItem(currentIndex);

        if (!(fragment instanceof BaseFragment && ((BaseFragment) fragment).handleBackPressed())) {
            super.onBackPressed();
        }
    }
}
