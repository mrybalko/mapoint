package net.mapoint.ui.main.list;

import android.graphics.Bitmap;
import android.location.Location;

import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;

import net.mapoint.analytics.BaseAnalyticsTracker;
import net.mapoint.data.LocationsDetailsData;
import net.mapoint.data.locations.EventData;
import net.mapoint.data.locations.LocationData;
import net.mapoint.managers.CategoriesManager;
import net.mapoint.managers.LocationManager;
import net.mapoint.ui.base.BasePresenter;
import net.mapoint.utils.LogUtils;
import net.mapoint.utils.location.LocationDataParser;

import java.util.List;

public class EventsListPresenter extends BasePresenter<EventsListView> {

    private static final String TAG = "EventsListPresenter";

    private final LocationManager locationManager;
    private final CategoriesManager categoriesManager;

    public EventsListPresenter(EventsListView view) {
        super(view);

        locationManager = LocationManager.instance();
        categoriesManager = CategoriesManager.instance();
    }

    @Override
    public void updateModel() {
        super.updateModel();
        addSubscription(locationManager
                .locationsDetailsDataWithPreviousObservable()
                .subscribe(locationsDetails -> {
                            if (locationsDetails != null) {
                                updateLocations(locationsDetails);
                            } else {
                                runIfView(view -> view.updateEmptyContentStub(true));
                            }
                        },
                        throwable -> {
                            LogUtils.log(TAG, ".locationsDetailsDataWithPreviousObservable", throwable);
                            runIfView(view -> view.updateEmptyContentStub(true));
                        }));
    }

    private void updateLocations(final LocationsDetailsData locationsDetails) {
        if (locationsDetails != null) {
            Location currentLocation = locationsDetails.currentLocation();
            runIfView(view -> view.showEvents(
                    convertLocationsToEvents(locationsDetails.locationData()),
                    currentLocation,
                    locationsDetails.radius()));
        }
    }

    private List<EventData> convertLocationsToEvents(final List<LocationData> locations) {

        return Stream.of(locations)
                .flatMap(locationData -> {
                    LocationDataParser locationDataParser = new LocationDataParser(locationData);
                    return Stream.of(locationDataParser.allEvents());
                })
                .collect(Collectors.toList());
    }

    void onItemClicked(EventData eventData) {
        if (eventData != null) {
            runIfView(view -> view.goToEventDetails(eventData));
            BaseAnalyticsTracker.instance().onToEventFromCommonList(eventData.getId(), eventData.eventType());
        }
    }

    public Bitmap coloredBitmapForSubcategories(List<String> subcategories){
        return categoriesManager.coloredBitmapForSubcategories(subcategories);
    }

    void onAddingEventClick() {
        runIfView(view -> view.goToAddingEvent());
    }
}
