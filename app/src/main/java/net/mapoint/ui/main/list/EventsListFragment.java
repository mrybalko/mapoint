package net.mapoint.ui.main.list;

import android.graphics.Bitmap;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.mapoint.R;
import net.mapoint.data.locations.EventData;
import net.mapoint.data.locations.EventDatesItemData;
import net.mapoint.data.locations.EventType;
import net.mapoint.data.locations.InnerLocationData;
import net.mapoint.ui.base.BaseFragment;
import net.mapoint.ui.main.location.LocationUtils;
import net.mapoint.utils.list.BaseViewHolder;
import net.mapoint.utils.list.ListAdapter;
import net.mapoint.utils.list.ListDataSet;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EventsListFragment extends BaseFragment<EventsListPresenter> implements EventsListView {

    private static final String TAG = "EventsListFragment";

    private RecyclerView recyclerView;
    private ListAdapter<EventData> adapter;
    private LinearLayout eventsListEmptyContentHolder;

    private View rootView;

    SimpleDateFormat serverDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat localDateFormat = new SimpleDateFormat("dd MMMM");

    private FloatingActionButton addingBtn;

    @Override
    public EventsListPresenter presenterProvider() {
        return new EventsListPresenter(this);
    }

    @Override
    public int getLayoutResId() {
        return R.layout.fr_locations_list;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        rootView = super.onCreateView(inflater, container, savedInstanceState);
        initViews(rootView, savedInstanceState);

        addingBtn = rootView.findViewById(R.id.add_event);
        addingBtn.setOnClickListener(v -> presenter().onAddingEventClick());

        return rootView;
    }

    @Override
    public void goToAddingEvent() {
        navigator().goToAddingEvent(getActivity());
    }

    private void initViews(View rootView, @Nullable Bundle savedInstanceState) {
        eventsListEmptyContentHolder = rootView.findViewById(R.id.events_list_empty_content_holder);

        recyclerView = rootView.findViewById(R.id.recycler);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        adapter = new ListAdapter(
                (context, view) -> new LocationListItemHolder(view),
                () -> R.layout.li_events_list,
                new ListDataSet(new ArrayList<EventData>()));

        adapter.setOnItemClickListener((adapterView, view, position, id) -> {
            EventData data = adapter.getItem(position);
            presenter().onItemClicked(data);
        });

        recyclerView.setAdapter(adapter);
    }

    @Override
    public void showEvents(final List<EventData> events,
                              final Location centerLocation,
                              final float radius) {
        updateLocations(events);
    }

    @Override
    public void goToEventDetails(EventData event) {
        navigator().goToEventDetails(getActivity(), event);
    }

    @Override
    public void updateEmptyContentStub(boolean isEmptyContent) {
        recyclerView.setVisibility(isEmptyContent ? View.GONE : View.VISIBLE);
        eventsListEmptyContentHolder.setVisibility(isEmptyContent ? View.VISIBLE : View.GONE);
    }

    private void updateLocations(List<EventData> events) {

        boolean isEmptyContent = events.size() == 0;

        if (!isEmptyContent) {
            adapter.updateData(new ListDataSet(events));
            adapter.notifyDataSetChanged();
        }

        updateEmptyContentStub(isEmptyContent);
    }

    @Override
    public void update() {
        presenter().updateModel();
    }

    private final class LocationListItemHolder extends BaseViewHolder<EventData> {

        private TextView titleTv;
        private TextView distanceTv;
        private TextView subcriptionTv;
        private TextView timeTv;
        private ImageView typeImage;
        private TextView locationTv;

        public LocationListItemHolder(View view) {
            super(view);

            typeImage = view.findViewById(R.id.type_image);
            subcriptionTv = view.findViewById(R.id.subcategories);
            titleTv = view.findViewById(R.id.title);
            distanceTv = view.findViewById(R.id.distance);
            timeTv = view.findViewById(R.id.time);
            locationTv = view.findViewById(R.id.location);
        }

        @Override
        public void onBind(EventData data) {
            super.onBind(data);

            EventType eventType = data.eventType();
            String subcategoriesValue = (eventType == EventType.Fact) ? getString(R.string.fact) : LocationUtils.subcategoriesUIString(data.getSubcategories());

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Bitmap bitmap = presenter().coloredBitmapForSubcategories(data.getSubcategories());
                typeImage.setImageBitmap(bitmap);
            }

            if (!TextUtils.isEmpty(subcategoriesValue)) {
                subcriptionTv.setText(subcategoriesValue);
                subcriptionTv.setVisibility(View.VISIBLE);
            } else {
                subcriptionTv.setVisibility(View.GONE);
            }

            titleTv.setText(data.getText());
            int distance = data.getInnerLocationData().getDistance();
            distanceTv.setText(distanceString(distance));

            String eventTime = eventTime(data.dates());
            if (!TextUtils.isEmpty(eventTime)) {
                timeTv.setVisibility(View.VISIBLE);
                timeTv.setText(eventTime);
            } else {
                timeTv.setVisibility(View.INVISIBLE);
            }

            InnerLocationData locationData = data.getInnerLocationData();
            if (locationData != null) {
                String locationName = locationData.getName();

                if (!TextUtils.isEmpty(locationName)) {
                    locationTv.setVisibility(View.VISIBLE);
                    locationTv.setText(locationName);
                } else {
                    locationTv.setVisibility(View.GONE);
                }
            }
        }

        private String eventTime(List<EventDatesItemData> dates) {

            if (dates != null && dates.size() > 0) {

                StringBuilder builder = new StringBuilder();

                EventDatesItemData itemData = dates.get(0);
                builder.append(startDate(itemData.getStartDate()));

                String time = concatTime(itemData.getSessions());
                if (!TextUtils.isEmpty(time)) {
                    builder.append(", ");
                    builder.append(time);
                }

                return builder.toString();
            }

            return "";
        }

        private String startDate(String startDateString) {
            String date = "";
            try {
                Date startDate = serverDateFormat.parse(startDateString);
                date = localDateFormat.format(startDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            return date;
        }

        private String concatTime(List<String> times) {
            StringBuilder timeItem = new StringBuilder();
            for (String session : times) {
                if (timeItem.length() > 0) {
                    timeItem.append(", ");
                }

                timeItem.append(session);
            }

            return timeItem.toString();
        }

        private String distanceString(final int distance) {

            StringBuilder builder = new StringBuilder();

            String postfix = "";
            String value = "";

            if (distance > 1000) {
                value = String.format("%.2f", distance/1000.0f);
                postfix = getString(R.string.km);
            } else {
                value = String.valueOf(distance);
                postfix = getString(R.string.m);
            }

            builder.append(value);
            builder.append(" ");
            builder.append(postfix);

            return builder.toString();
        }
    }
}