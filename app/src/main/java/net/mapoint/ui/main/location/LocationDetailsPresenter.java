package net.mapoint.ui.main.location;

import android.graphics.Bitmap;

import net.mapoint.analytics.BaseAnalyticsTracker;
import net.mapoint.data.locations.EventData;
import net.mapoint.data.locations.EventType;
import net.mapoint.data.locations.LocationData;
import net.mapoint.managers.CategoriesManager;
import net.mapoint.managers.LocationManager;
import net.mapoint.ui.base.BasePresenter;
import net.mapoint.utils.storage.SharedPreferencesProvider;

import java.util.List;

public class LocationDetailsPresenter extends BasePresenter<LocationDetailsView> {

    private LocationData locationData;

    public LocationDetailsPresenter(LocationDetailsView view) {
        super(view);
    }

    public void updateModel(LocationData locationData) {
        this.locationData = locationData;
        runIfView(view -> view.updateInfo(locationData));
    }

    public void onContactsClick(final List<String> phones) {
        runIfView(view -> view.showContactsList(phones));
        BaseAnalyticsTracker.instance().onLocationCallClicked(locationData.id());
    }

    public void goToOfferDetails(EventData offerData) {
        runIfView(view -> view.goToEventDetails(offerData));
        BaseAnalyticsTracker.instance().onToEventFromLocation(offerData.getId(), EventType.Offer);
    }

    public void goToFactDetails(EventData factData) {
        runIfView(view -> view.goToEventDetails(factData));
        BaseAnalyticsTracker.instance().onToEventFromLocation(factData.getId(), EventType.Fact);
    }

    public void openedFromNotification(String eventId) {
        LocationManager.instance().addSeenEvents(eventId);
        SharedPreferencesProvider.instance().shownNotificationEventId(null);
    }

    public Bitmap coloredBitmapForSubcategories(List<String> subcategories){
        return CategoriesManager.instance().coloredBitmapForSubcategories(subcategories);
    }
}
