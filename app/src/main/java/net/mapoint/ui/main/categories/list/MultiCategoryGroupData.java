package net.mapoint.ui.main.categories.list;

import com.thoughtbot.expandablecheckrecyclerview.models.MultiCheckExpandableGroup;

import net.mapoint.data.category.CategoryData;

import java.util.List;

public class MultiCategoryGroupData extends MultiCheckExpandableGroup {

    public MultiCategoryGroupData(String title, List<CategoryData> items) {
        super(title, items);
    }
}