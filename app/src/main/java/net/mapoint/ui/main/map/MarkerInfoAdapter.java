package net.mapoint.ui.main.map;

import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import net.mapoint.R;
import net.mapoint.data.locations.MapMarkerData;
import net.mapoint.ui.main.location.LocationUtils;

import rx.functions.Func1;

public class MarkerInfoAdapter implements GoogleMap.InfoWindowAdapter {

    private View view;
    private TextView titleTv;
    private TextView datesTv;
    private TextView locationNameTv;
    private TextView subcategoriesTv;

    public MarkerInfoAdapter(Func1<Integer, View> viewProvider) {
        view = viewProvider.call(R.layout.v_map_info_window);
        subcategoriesTv = view.findViewById(R.id.subcategories);
        titleTv = view.findViewById(R.id.title);
        datesTv = view.findViewById(R.id.time);
        locationNameTv = view.findViewById(R.id.location);
    }

    @Override public View getInfoWindow(Marker marker) {
        MapMarkerData mapMarkerData = (MapMarkerData)marker.getTag();

        setText(subcategoriesTv, LocationUtils.subcategoriesUIString(mapMarkerData.getSubcategories()));
        setText(titleTv, mapMarkerData.getTitle());
        setText(datesTv, LocationUtils.workingTimeFromDatesList(mapMarkerData.getDate()));
        setText(locationNameTv, mapMarkerData.getLocationName());

        return view;
    }

    @Override public View getInfoContents(Marker marker) {
        return null;
    }

    private void setText(TextView textView, String value) {
        if (!TextUtils.isEmpty(value)) {
            textView.setText(value);
            textView.setVisibility(View.VISIBLE);
        } else {
            textView.setVisibility(View.GONE);
        }
    }
}