package net.mapoint.ui.main.map;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

import net.mapoint.data.locations.EventData;
import net.mapoint.data.locations.EventDatesItemData;
import net.mapoint.data.locations.EventType;
import net.mapoint.data.locations.InnerLocationData;
import net.mapoint.ui.main.location.LocationUtils;

import java.util.List;

import rx.functions.Func1;

public class MarkerItem implements ClusterItem {

    private final EventData eventData;
    private final LatLng position;
    private Func1<Integer, String> resourceStringProvider;

    public MarkerItem(EventData eventData,
                      final Func1<Integer, String> resourceStringProvider) {
        this.eventData = eventData;
        this.resourceStringProvider = resourceStringProvider;


        InnerLocationData innerLocationData = eventData.getInnerLocationData();
        position = new LatLng(innerLocationData.getLat(), innerLocationData.getLng());
    }

    @Override
    public LatLng getPosition() {
        return position;
    }

    public String locationId() {
        String locationId = eventData.getInnerLocationData().getId();
        return locationId;
    }

    @Override
    public String getTitle() {
        return LocationUtils.eventDataTitle(eventData, resourceStringProvider);
    }

    public List<String> subcategories() {
        return eventData.getSubcategories();
    }

    public List<EventDatesItemData> dates() {
        return eventData.dates();
    }

    @Override
    public String getSnippet() {
        return eventData.getText();
    }

    public EventType eventType(){
        return eventData.eventType();
    }
}