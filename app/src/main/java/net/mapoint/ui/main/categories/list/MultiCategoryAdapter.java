package net.mapoint.ui.main.categories.list;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thoughtbot.expandablecheckrecyclerview.CheckableChildRecyclerViewAdapter;
import com.thoughtbot.expandablecheckrecyclerview.models.CheckedExpandableGroup;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import net.mapoint.R;
import net.mapoint.data.category.CategoryData;

import java.util.List;

import rx.functions.Func1;

public class MultiCategoryAdapter extends
        CheckableChildRecyclerViewAdapter<MultiCategoryGroupVH, MultiCategoryVH> {

    private Func1<String, Boolean> selectionStateProvider;

    public MultiCategoryAdapter(final List<MultiCategoryGroupData> groups,
                                final Func1<String, Boolean> selectionStateProvider) {
        super(groups);
        this.selectionStateProvider = selectionStateProvider;
    }

    @Override
    public MultiCategoryVH onCreateCheckChildViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.li_category, parent, false);
        return new MultiCategoryVH(view);
    }

    @Override
    public void onBindCheckChildViewHolder(MultiCategoryVH holder,
                                           int position,
                                           CheckedExpandableGroup group,
                                           int childIndex) {

        final CategoryData categoryData = (CategoryData) group.getItems().get(childIndex);
        holder.setChecked(selectionStateProvider.call(categoryData.getId()));
        holder.setCategoryName(categoryData.getName());
    }

    @Override
    public MultiCategoryGroupVH onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.li_categories_group, parent, false);
        return new MultiCategoryGroupVH(view);
    }

    @Override
    public void onBindGroupViewHolder(MultiCategoryGroupVH holder,
                                      int flatPosition,
                                      ExpandableGroup group) {
        holder.setCategoryGroupTitle(group);
    }
}
