package net.mapoint.ui.main.categories;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.mapoint.R;
import net.mapoint.data.category.CategoryData;
import net.mapoint.data.category.CategoryGroupData;
import net.mapoint.ui.GroupsRow;
import net.mapoint.ui.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

public class CategoriesActivity extends BaseActivity<CategoriesPresenter> implements CategoriesView {

    private TextView titleTv;
    private LinearLayout categoriesGroupsHolder;

    public static Intent newIntent(final Context context) {
        Intent intent = new Intent(context, CategoriesActivity.class);
        return intent;
    }

    @Override
    public CategoriesPresenter presenterProvider() {
        return new CategoriesPresenter(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        titleTv = findViewById(R.id.title);
        titleTv.setText(R.string.categories_title);

        categoriesGroupsHolder = findViewById(R.id.categories_groups_holder);

        findViewById(R.id.back_btn).setOnClickListener(view -> finish());
    }

    @Override
    public int getLayoutResId() {
        return R.layout.ac_categories;
    }

    @Override
    public void updateData(List<CategoryGroupData> groups, String selectedGroupId, int columnsNumber) {
        categoriesGroupsHolder.removeAllViews();
        newDesignCategories(groups, selectedGroupId, columnsNumber);
    }

    private void newDesignCategories(List<CategoryGroupData> groups, String selectedGroupId, int columnsNumber) {
        List<CategoryGroupData> newGroupsRowHeaders = new ArrayList();

        List<CategoryGroupData> newGroups = new ArrayList<>(groups);

        while(newGroups.size() > 0) {
            CategoryGroupData groupData = newGroups.remove(0);

            List<CategoryData> items = new ArrayList<>();

            for (CategoryData subcategory : groupData.getSubcategoryList()) {
                String id = subcategory.getId();
                items.add(new CategoryData(id, subcategory.getName(), subcategory.getGroupCategoryId(), presenter().isSelected(id), subcategory.getColor()));
            }

            newGroupsRowHeaders.add(new CategoryGroupData(
                    groupData.getId(),
                    groupData.getName(),
                    items,
                    groupData.getColor(),
                    groupData.getNeedExpandItems(),
                    groupData.getIcon()));

            if(newGroupsRowHeaders.size() == columnsNumber) {
                categoriesGroupsHolder.addView(createGroupsRow(newGroupsRowHeaders, selectedGroupId, columnsNumber));
                newGroupsRowHeaders = new ArrayList<>();
            }
        }

        if (newGroups.size() == 0 && newGroupsRowHeaders.size() > 0) {
            categoriesGroupsHolder.addView(createGroupsRow(newGroupsRowHeaders, selectedGroupId, columnsNumber));
        }
    }

    private GroupsRow createGroupsRow(List<CategoryGroupData> newGroups, String selectedGroupId, int columnsNumber) {
        GroupsRow groupsRow = new GroupsRow(getBaseContext());
        groupsRow.setSelectionListener(new GroupsRow.SelectionListener() {
            @Override
            public void onItemSelected(String itemId) {
                presenter().onSelectedItem(itemId);
            }

            @Override
            public void onAllItemSelectedInGroup(String groupId) {
                presenter().onSelectedAllItemsFor(groupId);
            }

            @Override
            public void onGroupSelected(String groupId, boolean isChecked) {
                presenter().onGroupSelected(groupId, isChecked);
            }
        });
        groupsRow.updateGroups(newGroups, selectedGroupId, columnsNumber);

        return groupsRow;
    }
}
