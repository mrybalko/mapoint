package net.mapoint.ui.main.map;

import android.graphics.Bitmap;
import android.location.Location;

import net.mapoint.analytics.BaseAnalyticsTracker;
import net.mapoint.data.LocationsDetailsData;
import net.mapoint.data.locations.LocationData;
import net.mapoint.managers.CategoriesManager;
import net.mapoint.managers.LocationManager;
import net.mapoint.ui.base.BasePresenter;
import net.mapoint.utils.LogUtils;

import java.util.List;

public class MapPresenter extends BasePresenter<MapFragmentView> {

    private static final String TAG = "MapPresenter";

    private final LocationManager locationManager;
    private boolean inited = false;

    public MapPresenter(MapFragmentView view) {
        super(view);

        locationManager = LocationManager.instance();
    }

    @Override
    public void updateModel() {
        super.updateModel();
        addSubscription(locationManager
                .locationsDetailsDataWithPreviousObservable()
                .subscribe((locationsDetails) -> updateLocations(locationsDetails),
                        throwable -> LogUtils.log(TAG, ".locationsDetailsDataWithPreviousObservable", throwable)));
        }

    private void updateLocations(final LocationsDetailsData locationsDetails) {
        if (locationsDetails != null) {

            Location currentLocation = locationsDetails.currentLocation();
            runIfView(view -> {
                view.showLocations(
                        locationsDetails.locationData(),
                        currentLocation,
                        locationsDetails.radius());
                if (!inited) {
                    view.setCenterMapToLocation(currentLocation);
                    inited = true;
                }
            });
        }
    }

    void onAddingEventClick() {
        runIfView(view -> view.goToAddingEvent());
    }

    void onMarkerClicked(final String locationId) {

        LocationData locationData = locationManager.getLocation(locationId);
        if (locationData != null) {
            runIfView(view -> view.goToLocationDetails(locationData));
            BaseAnalyticsTracker.instance().onToLocationFromMap(locationData.id());
        }
    }

    Bitmap coloredBitmapForSubcategories(List<String> subcategories){
        return CategoriesManager.instance().coloredBitmapForSubcategories(subcategories);
    }

    void onMapReady() {
        addSubscription(locationManager.currentLocation()
                .first()
                .subscribe(location -> runIfView(view -> view.setCenterMapToLocation(location))));
    }
}
