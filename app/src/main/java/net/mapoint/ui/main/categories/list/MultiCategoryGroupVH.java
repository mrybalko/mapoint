package net.mapoint.ui.main.categories.list;

import android.view.View;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

import net.mapoint.R;

import static android.view.animation.Animation.RELATIVE_TO_SELF;

public class MultiCategoryGroupVH extends GroupViewHolder {

    private TextView groupName;
    private ImageView arrow;

    public MultiCategoryGroupVH(View itemView) {
        super(itemView);
        groupName = (TextView) itemView.findViewById(R.id.category_group_name);
        arrow = (ImageView) itemView.findViewById(R.id.category_arrow);
    }

    public void setCategoryGroupTitle(ExpandableGroup genre) {
        if (genre instanceof MultiCategoryGroupData) {
            groupName.setText(genre.getTitle());
        }
    }

    @Override
    public void expand() {
        animateExpand();
    }

    @Override
    public void collapse() {
        animateCollapse();
    }

    private void animateExpand() {
        RotateAnimation rotate =
                new RotateAnimation(360, 180, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(300);
        rotate.setFillAfter(true);
        arrow.setAnimation(rotate);
    }

    private void animateCollapse() {
        RotateAnimation rotate =
                new RotateAnimation(180, 360, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(300);
        rotate.setFillAfter(true);
        arrow.setAnimation(rotate);
    }
}