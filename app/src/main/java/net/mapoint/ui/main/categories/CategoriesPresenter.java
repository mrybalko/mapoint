package net.mapoint.ui.main.categories;

import net.mapoint.analytics.BaseAnalyticsTracker;
import net.mapoint.data.category.CategoryData;
import net.mapoint.data.category.CategoryGroupData;
import net.mapoint.managers.CategoriesManager;
import net.mapoint.ui.base.BasePresenter;
import net.mapoint.utils.LogUtils;
import net.mapoint.utils.data.CategoriesUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CategoriesPresenter extends BasePresenter<CategoriesView> {

    private static final String TAG = "CategoriesPresenter";
    private final int COLUMNS_NUMBER = 4;

    private Set<String> selectedCategories;
    private CategoriesManager categoriesManager;
    private String lastExpandedGroupId;

    public CategoriesPresenter(CategoriesView view) {
        super(view);
        categoriesManager = CategoriesManager.instance();
        selectedCategories = new HashSet<>();
    }

    @Override
    public void updateModel() {
        selectedCategories = categoriesManager.readSelectedCategories();

        updateCategoriesView();

        addSubscription(categoriesManager
                .updateCategories()
                .subscribe(either -> {
                            if (either.isLeft()) {
                                updateCategoriesView();
                            }},
                        throwable -> LogUtils.log(TAG, ".updateCategories", throwable)));
    }

    private void updateCategoriesView() {
        List<CategoryGroupData> data = categoriesManager.getLocalCategories();
        updateData(data, "", COLUMNS_NUMBER);
    }

    boolean isSelected(String id) {
        return selectedCategories.contains(id);
    }

    void onSelectedItem(final String id) {
        updateSubcategorySelectedState(id);
    }

    void onSelectedAllItemsFor(String groupId) {
        List<CategoryData> subcategories = categoriesManager.itemsInGroup(groupId);
        boolean isAllSelected = CategoriesUtils.isAllSelected(subcategories, subcategoryId -> selectedCategories.contains(subcategoryId));
        for (CategoryData subcategoryData : subcategories) {
            selectSubcategories(subcategoryData.getId(), !isAllSelected);
        }

        updateSubcategoriesViews(groupId);
    }

    private void updateSubcategoriesViews(String groupId) {
        List<CategoryGroupData> localCategories = categoriesManager.getLocalCategories();
        updateData(localCategories, groupId, COLUMNS_NUMBER);
    }

    private void updateData(List<CategoryGroupData> groups, String selectedGroupId, int columnsNumber) {
        List<CategoryGroupData> categoryWithFact = new ArrayList<>(groups);
        runIfView(view -> view.updateData(categoryWithFact, selectedGroupId, columnsNumber));
}

    private void selectSubcategories(String subcategoriesId, boolean needSelect) {
        if (needSelect) {
            selectedCategories.add(subcategoriesId);
        } else {
            selectedCategories.remove(subcategoriesId);
        }
    }

    private void updateSubcategorySelectedState(String id) {

        boolean isSelected;
        if (selectedCategories.contains(id)) {
            selectedCategories.remove(id);
            isSelected = false;
        } else {
            selectedCategories.add(id);
            isSelected = true;
        }

        categoriesManager.updateLocaleSubcategory(id, isSelected);
    }

    void onGroupSelected(String groupId, boolean isChecked) {
        if (CategoriesManager.FACT_CATEGORY_ID.equals(groupId)) {
            boolean isNewFactsEnabled = !categoriesManager.readFactsEnabled();
            categoriesManager.saveFactsEnabled(isNewFactsEnabled);
            updateSubcategorySelectedState(CategoriesManager.FACT_CATEGORY_ITEM_ID);

            lastExpandedGroupId = "";
        } else {
            lastExpandedGroupId = isChecked ? groupId : "";
        }

        updateSubcategoriesViews(lastExpandedGroupId);
    }

    @Override
    public void onStop() {
        categoriesManager.saveSelectedCategories(selectedCategories);
        BaseAnalyticsTracker.instance().onFiltersCategoryChanged(selectedCategories);
        super.onStop();
    }
}
