package net.mapoint.ui.main.location;

import android.graphics.Bitmap;
import android.os.Build;
import android.text.TextUtils;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import net.mapoint.BuildConfig;
import net.mapoint.R;
import net.mapoint.data.locations.EventData;
import net.mapoint.data.locations.EventDatesItemData;
import net.mapoint.data.locations.EventType;
import net.mapoint.data.locations.InnerLocationData;
import net.mapoint.data.locations.LocationData;
import net.mapoint.managers.LocationManager;
import net.mapoint.ui.base.MapointApp;
import net.mapoint.utils.MarkerBitmapProvider;
import net.mapoint.utils.location.LocationDataParser;

import org.jetbrains.annotations.Nullable;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Set;

import rx.functions.Func1;

public class LocationUtils {

    private final DateFormatSymbols dfs = DateFormatSymbols.getInstance(MapointApp.locale());

    static SimpleDateFormat serverDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    static SimpleDateFormat localDateFormat = new SimpleDateFormat("dd MMMM");
    static SimpleDateFormat localShortDateFormat = new SimpleDateFormat("dd MMM");

    private static final String SUBCATEGORIES_DIVIDER = " / ";

    public static MarkerOptions markerOptionsFromLocationData(final LocationData locationData,
                                                              final Func1<Integer, String> resourceStringProvider,
                                                              final Func1<List<String>, Bitmap> bitmapProvider) {

        LocationDataParser locationDataParser = new LocationDataParser(locationData);
        EventData eventData = locationDataParser.firstCorrectEvent(resourceStringProvider);

        return createMarkerOption(
                locationData.lat(),
                locationData.lng(),
                eventDataTitle(eventData, resourceStringProvider),
                eventData.getText(),
                bitmapProvider.call(eventData.getSubcategories()));
    }

    public static MarkerOptions markerOptionsFromEventData(final EventData eventData,
                                                           final Func1<Integer, String> resourceStringProvider,
                                                           final Func1<List<String>, Bitmap> bitmapProvider) {

        final InnerLocationData locationData = eventData.getInnerLocationData();
        String title = eventDataTitle(eventData, resourceStringProvider);

        return createMarkerOption(
                locationData.getLat(),
                locationData.getLng(),
                title,
                eventData.getText(),
                bitmapProvider.call(eventData.getSubcategories()));
    }

    private static MarkerOptions createMarkerOption(final double lat,
                                                    final double lng,
                                                    final String title,
                                                    final String displayedText,
                                                    final Bitmap bitmap) {

        MarkerOptions markerOptions = new MarkerOptions()
                .position(new LatLng(lat, lng))
                .title(title)
                .snippet(displayedText);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(bitmap));
        }

        return markerOptions;
    }

    public static ArrayList<String> subcategoriesFromUIString(final String value) {
        return new ArrayList<>(Arrays.asList(value.split(SUBCATEGORIES_DIVIDER)));
    }

    public static String subcategoriesUIString(final List<String> subcategories) {
        StringBuilder subcategoriesString = new StringBuilder();
        if (subcategories != null) {
            for (String item : subcategories) {
                if (subcategoriesString.length() > 0) {
                    subcategoriesString.append(SUBCATEGORIES_DIVIDER);
                }

                subcategoriesString.append(item);
            }
        }

        return subcategoriesString.toString();
    }

    public static String eventDataTitle(final EventData eventData,
                                        final Func1<Integer, String> resourceStringProvider) {

        String title = "";

        InnerLocationData locationData = eventData.getInnerLocationData();
        String locationType = locationData.getType();
        String locationName = locationData.getName();
        EventType eventType = eventData.eventType();

        if (eventType == EventType.Fact) {
            title = resourceStringProvider.call(R.string.fact);
        } else if (eventType == EventType.Offer) {
            title = compileTitle(locationType, locationName);
        }

        return title;
    }

    public static String compileTitle(String type, String name) {
        StringBuilder title = new StringBuilder();

        if (!TextUtils.isEmpty(type)) {
            title.append(type);
        }

        if (!TextUtils.isEmpty(name)) {
            if (title.length() > 0) {
                title.append("\n");
            }

            title.append(name);
        }

        return title.toString();
    }

    @Nullable
    public static EventData findInfoForEvent(final List<LocationData> locations, Func1<Integer, String> stringResourceProvider) {
        Set<String> seenEvents = LocationManager.instance().seenEvents();

        EventData eventData = null;
        for(LocationData locationData : locations) {

            LocationDataParser locationDataParser = new LocationDataParser(locationData);
            eventData = locationDataParser.firstCorrectEvent(
                    eventId -> {
                        if (seenEvents != null) {
                            return !seenEvents.contains(eventId);
                        }

                        return false;
                    },
                    stringResourceProvider);

            if (eventData != null) {
                return eventData;
            }
        }

        return eventData;
    }

    public static String workingTimeFromDatesList(List<EventDatesItemData> dates) {
        StringBuilder workingTimes = new StringBuilder();

        if (dates.size() > 0) {
            for (EventDatesItemData date : dates) {
                if (workingTimes.length() > 0) {
                    workingTimes.append("\n");
                }

                StringBuilder dateItem = new StringBuilder();

                String startDateString = date.getStartDate();
                String endDateString = date.getEndDate();

                try {
                    Date startDate = serverDateFormat.parse(startDateString);
                    Date endDate = serverDateFormat.parse(endDateString);

                    dateItem.append(localDateFormat.format(startDate));

                    if (!startDateString.equals(endDateString)) {
                        dateItem.append(" - ");
                        dateItem.append(localDateFormat.format(endDate));
                    }

                    dateItem.append(" ");
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                StringBuilder timeItem = new StringBuilder();
                for (String session : date.getSessions()) {
                    if (timeItem.length() > 0) {
                        timeItem.append(", ");
                    }

                    timeItem.append(session);
                }

                dateItem.append(timeItem);

                workingTimes.append(dateItem);
            }
        }

        return workingTimes.toString();
    }

    public static String workingTimeIntervalFromDatesList(List<EventDatesItemData> dates) {

        String firstDateString = "";
        String lastDateString = "";

        int datesSize = dates.size();
        if (datesSize > 0) {
            EventDatesItemData firstDateItem = dates.get(0);

            try {
                Date startDate = serverDateFormat.parse(firstDateItem.getStartDate());
                firstDateString = localShortDateFormat.format(startDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (datesSize > 1) {
                try {
                    EventDatesItemData lastDateItem = dates.get(datesSize - 1);
                    Date lastDate = serverDateFormat.parse(lastDateItem.getEndDate());
                    lastDateString = localShortDateFormat.format(lastDate);

                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }

        StringBuilder workingTimes = new StringBuilder();
        workingTimes.append(firstDateString);
        if (!TextUtils.isEmpty(lastDateString) && !firstDateString.equals(lastDateString)) {
            workingTimes.append(" - ");
            workingTimes.append(lastDateString);
        }

        return workingTimes.toString();
    }
}
