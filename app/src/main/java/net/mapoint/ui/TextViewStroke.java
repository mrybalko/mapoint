package net.mapoint.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;

import net.mapoint.R;

public class TextViewStroke extends android.support.v7.widget.AppCompatTextView {

    private boolean inited;

    private Paint paint;
    private int strokeWidth = 5;
    private int radius;

    int selectedColor = 0;
    int unselectedColor = 0;

    public TextViewStroke(Context context) {
        super(context);
        init(null);
    }

    public TextViewStroke(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public TextViewStroke(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    private void init(final AttributeSet attrs) {
        if (!inited) {
            paint = new Paint(Paint.ANTI_ALIAS_FLAG);
            paint.setStyle(Paint.Style.STROKE);
            paint.setColor(getResources().getColor(R.color.category_item_stroke_color_default));
            radius = (int)getResources().getDimension(R.dimen.category_item_radius);
            strokeWidth = (int)getResources().getDimension(R.dimen.category_item_stroke_width);
            paint.setStrokeWidth(strokeWidth);

            int paddingVert = (int)getResources().getDimension(R.dimen.padding_small);
            int paddingHor = (int)getResources().getDimension(R.dimen.category_item_padding_horizontal);
            setPadding(paddingHor, paddingVert, paddingHor, paddingVert);

            inited = true;
        }
    }

    public void setColors(int selectedColor, int unselectedColor) {
        this.selectedColor = selectedColor;
        this.unselectedColor = unselectedColor;
        paint.setColor(isSelected() ? selectedColor : unselectedColor);
    }

    @Override
    public void setSelected(boolean selected) {
        super.setSelected(selected);
        paint.setColor(selected ? selectedColor : unselectedColor);
    }

    protected void onDraw(Canvas canvas) {

        int width = getWidth();
        int height = getHeight();

        canvas.drawRoundRect(0 + strokeWidth,0 + strokeWidth,
                width - strokeWidth,
                height - strokeWidth,
                radius, radius, paint);
        super.onDraw(canvas);
    }
}
