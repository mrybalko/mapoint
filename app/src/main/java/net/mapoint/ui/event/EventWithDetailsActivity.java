package net.mapoint.ui.event;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import net.mapoint.R;
import net.mapoint.data.locations.EventData;
import net.mapoint.data.locations.EventDatesItemData;
import net.mapoint.data.locations.InnerLocationData;
import net.mapoint.data.locations.LocationData;
import net.mapoint.ui.DislikeMenu;
import net.mapoint.ui.base.BaseActivity;
import net.mapoint.ui.base.MapointApp;
import net.mapoint.ui.main.location.LocationUtils;
import net.mapoint.utils.Constants;
import net.mapoint.utils.LogUtils;

import java.util.List;

public class EventWithDetailsActivity extends BaseActivity<EventDetailsPresenter> implements EventDetailsView {

    private static final String KEY_EVENT_ID_FROM_NOTIFICATION = "event_id_from_notification";

    private GoogleMap googleMap;

    private TextView subcategoriesTv;
    private TextView eventNameTv;
    private BottomPanel bottomPanel;

    private TextView locationNameTv;
    private TextView addressTv;
    private TextView typeTv;
    private ImageView contactsIv;

    private TextView workingTimeTv;
    private TextView linkTv;
    private TextView fullDescriptionTv;

    private static final String KEY_EVENT_DETAILS = "event_details";

    public static Intent newIntent(final Context context,
                                   final EventData eventData) {
        Intent intent = new Intent(context, EventWithDetailsActivity.class);
        intent.putExtra(KEY_EVENT_DETAILS, eventData);
        return intent;
    }

    public static Intent newIntent(final Context context,
                                   final EventData eventData,
                                   final String eventId) {

        Intent intent = newIntent(context, eventData);

        if (eventId != null) {
            intent.putExtra(KEY_EVENT_ID_FROM_NOTIFICATION, eventId);
        }

        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        initViews();
        tryMarkSeenEvent();
    }

    private void tryMarkSeenEvent() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String eventIdFromNotification = (String)extras.getSerializable(KEY_EVENT_ID_FROM_NOTIFICATION);
            LogUtils.log("TESTING", " tryMarkSeenEvent : " + eventIdFromNotification);
            if (eventIdFromNotification != null) {
                presenter().openedFromNotification(eventIdFromNotification);
            }
        }
    }

    private void initViews() {
        subcategoriesTv = (TextView) findViewById(R.id.subcategories);
        eventNameTv = (TextView)findViewById(R.id.event_name);

        locationNameTv = (TextView) findViewById(R.id.name);
        addressTv = (TextView) findViewById(R.id.address);
        typeTv = (TextView) findViewById(R.id.type);
        contactsIv = (ImageView) findViewById(R.id.contacts);

        //titleTv = (TextView) findViewById(R.id.title);
        workingTimeTv = findViewById(R.id.working_time_content);
        linkTv = findViewById(R.id.link);
        fullDescriptionTv = findViewById(R.id.full_description);

        findViewById(R.id.back_btn).setOnClickListener(view -> finish());

        findViewById(R.id.location_common_info).setOnClickListener(view -> presenter().onLocationInfoClicked());
        bottomPanel = new BottomPanel();
    }

    @Override
    public void updateInfo(final EventData eventData) {
        updateMarker(eventData);
        updateEventInfo(eventData);
    }

    private void updateMarker(final EventData eventData) {
        MarkerOptions markerOptions = LocationUtils.markerOptionsFromEventData(eventData,
                integer -> getString(integer),
                subcategories -> presenter().coloredBitmapBySubcategory(subcategories));

        markerOptions.anchor(0.5f, 0.5f);
        googleMap.clear();
        googleMap.addMarker(markerOptions);

        InnerLocationData innerLocationData = eventData.getInnerLocationData();

        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                new LatLng(innerLocationData.getLat(), innerLocationData.getLng()),
                13.0f));
        googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                return null;
            }
        });

        googleMap.getUiSettings().setZoomControlsEnabled(true);
    }

    @Override
    public void goToDetails(String url) {
        navigator().goToBrowser(this, url);
    }

    @Override
    public void showContactsList(List<String> phones) {

        permissionHandler().requestWithChecking(this,
                Manifest.permission.CALL_PHONE,
                Constants.REQUEST_CALL_PERMISSION,
                () -> navigator().callNumber(this, phones.get(0)),
                () -> {});
    }

    @Override
    public void goToLocationDetails(LocationData locationData) {
        navigator().goToLocationDetails(this, locationData);
    }

    @Override
    public void showDislikeMenu(boolean needShow) {
        bottomPanel.showDislikeMenu(needShow);
    }

    @Override
    public void share() {
        navigator().share(EventWithDetailsActivity.this);
    }

    @Override
    public int getLayoutResId() {
        return R.layout.ac_event_details;
    }

    @Override
    public EventDetailsPresenter presenterProvider() {
        return new EventDetailsPresenter(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            EventData eventData = (EventData) extras.getSerializable(KEY_EVENT_DETAILS);
            presenter().updateModel(eventData);
        }

        checkAndRequestLocationPermission(findViewById(R.id.root_view),
                () -> {
                    if (googleMap != null) {
                        googleMap.setMyLocationEnabled(true);
                    }
                });
    }

    private void updateEventInfo(final EventData eventData) {
        fillLocationCommonInfo(eventData.getInnerLocationData());
        fillDetails(eventData);
    }

    private void fillLocationCommonInfo(final InnerLocationData locationData) {
        fillField(locationNameTv, locationData.getName());
        fillField(addressTv, locationData.getAddress());
        fillField(typeTv, locationData.getType());

        initContactsField(locationData.getPhones());
    }

    private void initContactsField(List<String> phones) {

        if (phones != null && phones.size() > 0) {
            contactsIv.setVisibility(View.VISIBLE);
            contactsIv.setOnClickListener(view -> presenter().onContactsClick(phones));
        }
    }

    private void fillDetails(final EventData eventData) {

        fillField(subcategoriesTv, LocationUtils.subcategoriesUIString(eventData.getSubcategories()));
        fillField(eventNameTv, eventData.getText());
        //fillField(titleTv, eventData.getText());
        fillField(fullDescriptionTv, eventData.getFullDescription());
        fillLink(eventData.getLink());
        fillWorkingTime(eventData.dates());
    }

    private void fillLink(final String link) {
        if (!TextUtils.isEmpty(link)) {
            linkTv.setVisibility(View.VISIBLE);
            linkTv.setOnClickListener(view -> presenter().goToDetails());
        }
    }

    private void fillWorkingTime(List<EventDatesItemData> dates) {
        if (dates != null) {
            String workingTimes = LocationUtils.workingTimeFromDatesList(dates);

            if (!TextUtils.isEmpty(workingTimes)) {
                workingTimeTv.setText(workingTimes);
                workingTimeTv.setVisibility(View.VISIBLE);
            } else {
                workingTimeTv.setVisibility(View.GONE);
            }
        }
    }

    private void fillField(final TextView textView,
                           final String value) {

        if (!TextUtils.isEmpty(value)) {
            textView.setText(value);
            textView.setVisibility(View.VISIBLE);
        }
    }

    private class BottomPanel {

        private static final String DISLIKE_MENU_KEY = "DISLIKE_MENU_KEY";

        private View likeBtn;
        private View dislikeBtn;
        private View shareBtn;
        private DislikeMenu dislikeMenu;

        public BottomPanel() {
            init();
        }

        public void init() {
            likeBtn = findViewById(R.id.likeBtn);
            likeBtn.setOnClickListener(v -> {
                presenter().onLikeClicked();
            });

            dislikeBtn = findViewById(R.id.dislikeBtn);
            dislikeBtn.setOnClickListener(v -> {
                presenter().onDislikeClicked();
            });

            shareBtn = findViewById(R.id.shareBtn);
            shareBtn.setOnClickListener(v -> {
                presenter().onShareClicked();
            });

            dislikeMenu = DislikeMenu.Companion.newInstance();
        }

        public void showDislikeMenu(boolean needShow) {
            if (needShow) {
                dislikeMenu.show(getSupportFragmentManager(), DISLIKE_MENU_KEY);
            } else {
                dislikeMenu.dismiss();
            }
        }
    }
}
