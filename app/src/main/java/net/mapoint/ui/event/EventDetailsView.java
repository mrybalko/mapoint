package net.mapoint.ui.event;

import com.google.android.gms.maps.OnMapReadyCallback;

import net.mapoint.data.locations.EventData;
import net.mapoint.data.locations.LocationData;

import java.util.List;

public interface EventDetailsView extends OnMapReadyCallback {
    void updateInfo(EventData eventData);
    void goToDetails(final String url);
    void showContactsList(final List<String> phones);
    void goToLocationDetails(LocationData locationData);

    void showDislikeMenu(boolean needShow);

    void share();
}
