package net.mapoint.ui.event;

import android.graphics.Bitmap;
import android.widget.Toast;

import net.mapoint.analytics.BaseAnalyticsTracker;
import net.mapoint.data.locations.EventData;
import net.mapoint.data.locations.LocationData;
import net.mapoint.managers.CategoriesManager;
import net.mapoint.managers.LocationManager;
import net.mapoint.managers.LoggingManager;
import net.mapoint.ui.base.BasePresenter;
import net.mapoint.ui.base.MapointApp;

import java.util.List;

public class EventDetailsPresenter extends BasePresenter<EventDetailsView> {

    private EventData eventData;
    private boolean openedFromNotification = false;

    public EventDetailsPresenter(EventDetailsView view) {
        super(view);
    }

    public void updateModel(EventData eventData) {
        this.eventData = eventData;
        runIfView(view -> view.updateInfo(eventData));

        if (openedFromNotification) {
            BaseAnalyticsTracker.instance().onNotificationEventClicked(eventData.getId(), eventData.eventType());
            openedFromNotification = false;
        }
    }

    public void onContactsClick(final List<String> phones) {
        runIfView(view -> view.showContactsList(phones));
    }

    public void goToDetails() {
        runIfView(view -> {
            view.goToDetails(eventData.getLink());
            BaseAnalyticsTracker.instance().onEventLinkClicked(eventData.getId(), eventData.eventType());
        });
    }

    public void onLocationInfoClicked() {
        String locationId = eventData.getInnerLocationData().getId();
        LocationData locationData = LocationManager.instance().getLocation(locationId);
        runIfView(view -> view.goToLocationDetails(locationData));
    }

    public void openedFromNotification(String eventId) {
        LoggingManager.instance().logMessage("openedFromNotification : " + eventId);
        LocationManager.instance().addSeenEvents(eventId);
        openedFromNotification = true;
    }

    public void onLikeClicked(){
        Toast.makeText(MapointApp.getContext(), "Like", Toast.LENGTH_SHORT).show();
    }

    public void onDislikeClicked(){
        runIfView(view -> view.showDislikeMenu(true));
    }

    public void onShareClicked(){
        runIfView(view->view.share());
    }

    public Bitmap coloredBitmapBySubcategory(List<String> subcategories){
        return CategoriesManager.instance().coloredBitmapForSubcategories(subcategories);
    }
}