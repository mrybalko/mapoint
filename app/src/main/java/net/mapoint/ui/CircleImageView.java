package net.mapoint.ui;

import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.PictureDrawable;
import android.net.Uri;
import android.support.annotation.ColorInt;
import android.support.v7.widget.AppCompatImageView;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.util.Preconditions;

import net.mapoint.R;
import net.mapoint.data.category.CategoryIconPathProvider;
import net.mapoint.utils.image.GlideApp;
import net.mapoint.utils.image.GlideRequest;
import net.mapoint.utils.image.GlideRequests;
import net.mapoint.utils.image.SvgSoftwareLayerSetter;

import java.io.File;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class CircleImageView extends AppCompatImageView {

    private boolean inited;

    private Paint paintBg;
    private Paint paintStroke;
    private int strokeWidth = 5;
    @ColorInt private int selectedColor;
    @ColorInt private int unselectedColor;
    private GlideRequest requestBuilder;

    public CircleImageView(Context context) {
        super(context);
        init(null);
    }

    public CircleImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public CircleImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    private void init(final AttributeSet attrs) {
        if (!inited) {
            paintStroke = new Paint(Paint.ANTI_ALIAS_FLAG);
            paintStroke.setStyle(Paint.Style.STROKE);
            paintStroke.setColor(getResources().getColor(R.color.category_item_stroke_color_default));

            strokeWidth = (int)getResources().getDimension(R.dimen.category_item_stroke_width);
            paintStroke.setStrokeWidth(strokeWidth);

            paintBg = new Paint(Paint.ANTI_ALIAS_FLAG);
            paintBg.setStyle(Paint.Style.FILL);
            paintBg.setColor(Color.WHITE);

            selectedColor = getResources().getColor(R.color.tab_bg);
            unselectedColor = getResources().getColor(R.color.category_item_stroke_color_default);

            clearCache(this);

            requestBuilder = GlideApp.with(this)
                    .as(PictureDrawable.class)
                    .transition(withCrossFade())
                    .listener(new SvgSoftwareLayerSetter());

            setScaleType(ScaleType.CENTER_CROP);

            inited = true;
        }
    }

    public void setSelectedColor(Integer colorValue) {
        selectedColor = colorValue;
    }

    public void setUnSelectedColor(String colorValue) {
        unselectedColor = Color.parseColor(colorValue);
    }

    public void clearCache(View v) {
        GlideRequests glideRequests = GlideApp.with(this);
        glideRequests.clear(this);
        GlideApp.get(getContext()).clearMemory();
        File cacheDir = Preconditions.checkNotNull(Glide.getPhotoCacheDir(getContext()));
        if (cacheDir.isDirectory()) {
            for (File child : cacheDir.listFiles()) {
                if (!child.delete()) {
                    Log.w("CircleImageView", "cannot delete: " + child);
                }
            }
        }
    }

    protected void onDraw(Canvas canvas) {

        int width = getWidth();

        int horizontalCenter = width/2;
        int radius = horizontalCenter - strokeWidth;

        @ColorInt int color = isSelected() ? selectedColor : unselectedColor;
        paintStroke.setColor(color);

        canvas.drawCircle(horizontalCenter, horizontalCenter, radius, paintBg);
        canvas.drawCircle(horizontalCenter, horizontalCenter, radius, paintStroke);

        super.onDraw(canvas);
    }

    public void setIcon(CategoryIconPathProvider iconPathProvider) {

        String iconUrl = iconPathProvider.getRemoteUrl();//"http://www.clker.com/cliparts/u/Z/2/b/a/6/android-toy-h.svg";//iconPathProvider.getRemoteUrl();
        int iconResId = iconPathProvider.getResourceIconId();

        if (!TextUtils.isEmpty(iconUrl)) {
            loadNet(iconUrl);

        } else if (iconResId > 0) {
            loadRes(iconResId);
        }
    }

    private void loadRes(int resId) {
        setImageResource(resId);
    }

    private void loadNet(String url) {
        Uri uri = Uri.parse(url);
        load(uri);
    }

    private void load(Uri uri) {
        requestBuilder
                .load(uri)
                .diskCacheStrategy(DiskCacheStrategy.DATA)
                .into(this);
    }
}
