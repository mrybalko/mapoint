package net.mapoint.ui.base;

import rx.Subscription;

public interface IPresenter {
    void addSubscription(final Subscription subscription);
    void unsubscribe();
    void updateModel();
    void onStop();
}