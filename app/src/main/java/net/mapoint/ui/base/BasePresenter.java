package net.mapoint.ui.base;

import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.widget.Toast;

import rx.Subscription;
import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

public abstract class BasePresenter<ViewType> implements IPresenter {

    @NonNull
    private CompositeSubscription subscriptions = new CompositeSubscription();

    private ViewType view;

    public BasePresenter(ViewType view) {
        this.view = view;
    }

    public Resources resources() {
        return MapointApp.getContext().getResources();
    }

    public void runIfView(@NonNull final Action1<ViewType> action) {
        final ViewType view = this.view;
        if (view != null) {
            action.call(view);
        }
    }

    @Override
    public void updateModel() {
    }

    @Override
    public void onStop() {
        unsubscribe();
    }

    @Override
    public void addSubscription(Subscription subscription) {
        subscriptions.add(subscription);
    }

    @Override
    public void unsubscribe() {
        subscriptions.unsubscribe();
        subscriptions = new CompositeSubscription();
    }

    protected void showSystemMessage(int resId) {
        Toast.makeText(MapointApp.getContext(), resources().getString(resId), Toast.LENGTH_SHORT).show();
    }
}
