package net.mapoint.ui.base;

import android.app.Application;
import android.content.Context;
import android.os.Build;

import com.crashlytics.android.Crashlytics;
import com.evernote.android.job.JobConfig;
import com.evernote.android.job.JobManager;
import com.flurry.android.FlurryAgent;

import net.mapoint.BuildConfig;
import net.mapoint.R;
import net.mapoint.data.LanguageItem;
import net.mapoint.managers.SettingsManager;
import net.mapoint.tracker.UpdateEventsNotificationJobCreator;
import net.mapoint.utils.LocaleUtils;
import net.mapoint.utils.MarkerBitmapProvider;

import java.util.Locale;

import io.fabric.sdk.android.Fabric;

import static android.util.Log.VERBOSE;

public class MapointApp extends Application {

    private static Context context;

    public void onCreate() {
        super.onCreate();

        Fabric.with(this, new Crashlytics());
        context = getApplicationContext();

        MarkerBitmapProvider.instance().init();

        new FlurryAgent.Builder()
                .withLogEnabled(true)
                .withCaptureUncaughtExceptions(true)
                .withContinueSessionMillis(10)
                .withLogLevel(VERBOSE)
                .build(this, "HTS46YNN33GMSJTYMV3Z");

        updateLanguage();
        startJob();
    }

    private void updateLanguage() {
        SettingsManager settingsManager = SettingsManager.instance();
        LanguageItem languageItem = settingsManager.languageItem();
        LocaleUtils.changeAppLanguage(languageItem.realValue());

    }

    private void startJob(){
        JobManager.create(this).addJobCreator(new UpdateEventsNotificationJobCreator());

        if (BuildConfig.DEBUG && Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            JobConfig.setAllowSmallerIntervalsForMarshmallow(true);
        }
    }

    public static Context getContext() {
        return context;
    }

    public static String userAgent() {
        return String.format(
                "%s/%s (Android/%s; %s %s)",
                MapointApp.getContext().getResources().getString(R.string.app_name),
                BuildConfig.VERSION_NAME,
                android.os.Build.VERSION.RELEASE,
                android.os.Build.MANUFACTURER,
                android.os.Build.MODEL
        );
    }

    public static Locale locale() {
        return context.getResources().getConfiguration().locale;
    }
}