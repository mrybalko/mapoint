package net.mapoint.ui.base;

import android.Manifest;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.mapoint.R;
import net.mapoint.utils.Constants;
import net.mapoint.utils.LogUtils;
import net.mapoint.utils.PermissionHandler;

import rx.functions.Action0;
import rx.functions.Action1;

public abstract class BaseFragment<PresenterType extends IPresenter> extends Fragment {

    private static final String TAG = "Base";

    private void logBase(final String message) {
        LogUtils.log(TAG, message);
    }

    @Nullable
    private PresenterType presenter;
    private Navigator navigator;
    private PermissionHandler permissionHandler;

    public abstract PresenterType presenterProvider();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        logBase(getClass().getName() + " : onCreate");
    }

    public static <T> T findFragmentCallback(@NonNull Fragment fragment, @NonNull final Context context, @NonNull final Class<T> clazz) {
        final Fragment parentFragment = fragment.getParentFragment();
        if (clazz.isInstance(context)) {
            return clazz.cast(context);
        } else if (clazz.isInstance((parentFragment))) {
            return clazz.cast(parentFragment);
        } else {
            throw new RuntimeException(context.toString()
                    + (parentFragment != null ? " or " + parentFragment : "")
                    + " must implement " + clazz.getName());
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        logBase(getClass().getName() + " : onAttach");
        navigator = Navigator.instance();
        presenter = presenterProvider();
        permissionHandler = PermissionHandler.instance();
    }

    @Override
    public void onDetach() {
        logBase(getClass().getName() + " : onDetach");
        super.onDetach();
    }

    protected Navigator navigator() {
        return navigator;
    }

    protected PermissionHandler permissionHandler() {
        return permissionHandler;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        logBase(getClass().getName() + " : onCreateView");

        View rootView = inflater.inflate(getLayoutResId(), null);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        logBase(getClass().getName() + " : onViewCreated");
        super.onViewCreated(view, savedInstanceState);
    }

    public boolean handleBackPressed() {
        return false;
    }

    public PresenterType presenter() {
        return presenter;
    }

    @Override
    public void onDestroy() {
        if (presenter != null) {
            presenter.unsubscribe();
        }
        logBase(getClass().getName() + " : onDestroy");

        super.onDestroy();
    }

    /**
     * Called when the Fragment is no longer started.  This is generally
     * tied to {@link Activity#onStop() Activity.onStop} of the containing
     * Activity's lifecycle.
     */
    @Override
    public void onStop() {
        presenter.onStop();

        logBase(getClass().getName() + " : onStop");
        super.onStop();
    }

    public void runIfArgs(@NonNull final Action1<Bundle> action) {
        Bundle args = getArguments();
        if (args != null) {
            action.call(args);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        logBase(getClass().getName() + " : onResume");
        presenter.updateModel();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        permissionHandler().handlePermissionRequestResult(requestCode, permissions, grantResults);
    }

    public abstract int getLayoutResId();

    protected void showPermissionRationale(final View rootView,
                                           final int requestCode,
                                           final @NonNull String[] permissions,
                                           final String permissionRationale) {
        Snackbar.make(
                rootView,
                permissionRationale,
                Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.OK, view -> {

                    // Request permission
                    ActivityCompat.requestPermissions(getActivity(),
                            permissions,
                            requestCode);
                })
                .show();
    }

    protected void checkAndRequestLocationPermission(final View rootView,
                                                     final Action0 onOkAction) {

        final String permission = Manifest.permission.ACCESS_FINE_LOCATION;
        final int requestCode = Constants.REQUEST_LOCATION_PERMISSION;

        permissionHandler().requestWithChecking(getActivity(),
                permission,
                requestCode,
                onOkAction,
                () -> showPermissionRationale(rootView,
                        requestCode,
                        new String[]{permission},
                        getString(R.string.permission_rationale)));
    }
}