package net.mapoint.ui.base;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.Toast;

import net.mapoint.R;
import net.mapoint.utils.Constants;
import net.mapoint.utils.LogUtils;
import net.mapoint.utils.PermissionHandler;

import rx.functions.Action0;

public abstract class BaseActivity<PresenterType extends IPresenter> extends AppCompatActivity {

    private static final String TAG = "Base";

    @Nullable
    private PresenterType presenter;
    private Navigator navigator;
    private PermissionHandler permissionHandler;

    @LayoutRes
    public abstract int getLayoutResId();
    public abstract PresenterType presenterProvider();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        logBase(getClass().getName() + " : onCreate");
        setContentView(getLayoutResId());

        navigator = Navigator.instance();
        presenter = presenterProvider();
        permissionHandler = PermissionHandler.instance();
    }

    protected Navigator navigator() {
        return navigator;
    }

    public PresenterType presenter() {
        return presenter;
    }

    protected PermissionHandler permissionHandler() {
        return permissionHandler;
    }

    @Override
    protected void onDestroy() {
        logBase(getClass().getName() + " : onDestroy");
        if (presenter != null) {
            presenter.unsubscribe();
        }
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        logBase(getClass().getName() + " : onResume");
        presenter.updateModel();
    }

    @Override
    protected void onStop() {
        logBase(getClass().getName() + " : onStop");

        presenter.onStop();
        super.onStop();
    }

    @Override
    public Resources getResources() {
        return MapointApp.getContext().getResources();
    }

    protected void updateFragment(@NonNull final BaseFragment fragment,
                                  @NonNull final String tag,
                                  final int fragmentHolderId,
                                  final boolean needAddBackStack) {

        final FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager.findFragmentByTag(tag) == null) {
            final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            if (needAddBackStack) {
                fragmentTransaction.addToBackStack(tag);
            }
            fragmentTransaction.replace(fragmentHolderId, fragment, tag);
            fragmentTransaction.commitAllowingStateLoss();
        }
    }

    protected void showLongToast(final int messageResId) {
        Toast.makeText(this, getString(messageResId), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        permissionHandler().handlePermissionRequestResult(requestCode, permissions, grantResults);
    }

    protected void showPermissionRationale(final View rootView,
                                           final int requestCode,
                                           final @NonNull String[] permissions,
                                           final String permissionRationale) {
        Snackbar.make(
                rootView,
                permissionRationale,
                Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.OK, view -> {
                    // Request permission
                    ActivityCompat.requestPermissions(this,
                            permissions,
                            requestCode
                    );
                })
                .show();
    }

    protected void checkAndRequestLocationPermission(final View rootView,
                                                     final Action0 onOkAction) {

        final String permission = Manifest.permission.ACCESS_FINE_LOCATION;
        final int requestCode = Constants.REQUEST_LOCATION_PERMISSION;

        permissionHandler().requestWithChecking(this,
                permission,
                requestCode,
                onOkAction,
                () -> showPermissionRationale(rootView,
                        requestCode,
                        new String[]{permission},
                        getString(R.string.permission_rationale)));
    }

    private void logBase(final String message) {
        LogUtils.log(TAG, message);
    }

    public void showInfoDialog(
            final String title,
            final String message,
            final String positiveText,
            final DialogInterface.OnClickListener onPositiveListener) {

        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);

        if (title != null) {
            builder.setTitle(title);
        }
        builder.setMessage(Html.fromHtml(message));

        if (onPositiveListener != null) {
            builder.setPositiveButton(positiveText, onPositiveListener);
        }

        Dialog dialog = builder.create();
        dialog.show();
    }
}
