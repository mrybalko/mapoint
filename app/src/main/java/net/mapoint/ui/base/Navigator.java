package net.mapoint.ui.base;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;

import com.flurry.android.FlurryAgent;

import net.mapoint.data.locations.EventData;
import net.mapoint.data.locations.LocationData;
import net.mapoint.ui.adding.AddingEventActivity;
import net.mapoint.ui.adding.ParameterType;
import net.mapoint.ui.adding.parameter.mutli.AddingMultiParametersActivity;
import net.mapoint.ui.adding.parameter.single.AddingParameterActivity;
import net.mapoint.ui.adding.telephone.AddingPhoneActivity;
import net.mapoint.ui.adding.time.AddingDateTimeActivity;
import net.mapoint.ui.event.EventWithDetailsActivity;
import net.mapoint.ui.main.categories.CategoriesActivity;
import net.mapoint.ui.main.location.LocationWithDetailsActivity;
import net.mapoint.utils.Singleton;

import java.util.ArrayList;

import static net.mapoint.utils.Constants.REQUEST_CODE_ADD_CATEGORY;
import static net.mapoint.utils.Constants.REQUEST_CODE_ADD_PARAMETER;
import static net.mapoint.utils.Constants.REQUEST_CODE_ADD_PHONE;
import static net.mapoint.utils.Constants.REQUEST_CODE_ADD_DATE_TIME;

public class Navigator {
    private static final Singleton<Navigator> _instanceHolder = new Singleton<>(Navigator::new);

    public static Navigator instance() {
        return _instanceHolder.instance();
    }

    public void goToAddingEvent(final Activity activity) {
        start(activity, AddingEventActivity.newIntent(activity));
    }

    public void goToLocationDetails(final Activity activity,
                                    final LocationData locationDetails) {

        start(activity, LocationWithDetailsActivity.newIntent(activity, locationDetails));
    }

    public void goToEventDetails(final Activity activity,
                                 final EventData eventData) {

        start(activity, EventWithDetailsActivity.newIntent(activity, eventData));
    }

    public void goToCategories(final Activity activity) {
        start(activity, CategoriesActivity.newIntent(activity));
    }

    public void goToBrowser(final Activity activity, final String url) {
        Intent signUpIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        start(activity, signUpIntent);
    }

    public void callNumber(@NonNull final Activity activity, String number) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:"+ number));
        start(activity, callIntent);
    }

    public void goToNotificationSettingsChanging(final Activity activity,
                                                 final String channel) {

        Intent intent = null;

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN &&
                android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            intent = new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS");

        } else if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP &&
                android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            intent = new Intent("android.settings.APP_NOTIFICATION_SETTINGS");
            intent.putExtra("app_package", activity.getPackageName());
            //intent.putExtra("app_uid", getApplicationInfo().uid);
            intent.putExtra("android.provider.extra.APP_PACKAGE", activity.getPackageName());

        } else if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            intent = new Intent(Settings.ACTION_CHANNEL_NOTIFICATION_SETTINGS);
            intent.putExtra(Settings.EXTRA_APP_PACKAGE, activity.getPackageName());
            intent.putExtra(Settings.EXTRA_CHANNEL_ID, channel);
        }

        if (intent != null) {
            start(activity, intent);
        }
    }

    public void goToAddingParameter(final Activity activity,
                                    final ParameterType parameterType,
                                    final String value) {

        startForResult(activity,
                AddingParameterActivity.newIntent(activity, parameterType, value),
                REQUEST_CODE_ADD_PARAMETER);
    }

    public void goToAddingCategory(final Activity activity, final ArrayList<String> parameters) {

        startForResult(activity,
                AddingMultiParametersActivity.newIntent(activity, ParameterType.Category, parameters),
                REQUEST_CODE_ADD_CATEGORY);
    }

    public void goToAddingPhone(final Activity activity,
                                final String value) {

        startForResult(activity,
                AddingPhoneActivity.newIntent(activity, value),
                REQUEST_CODE_ADD_PHONE);
    }

    public void goToAddingTime(final Activity activity) {

        startForResult(activity,
                AddingDateTimeActivity.Companion.newIntent(activity),
                REQUEST_CODE_ADD_DATE_TIME);
    }

    private static boolean startForResult(@NonNull final Activity activity,
                                          @NonNull final Intent intent,
                                          int requestCode) {
        activity.startActivityForResult(intent, requestCode);
        return true;
    }

    private static boolean start(@NonNull final Activity activity,
                                 @NonNull final Intent intent) {
        activity.startActivity(intent);
        return true;
    }

    public void share(Context context) {

        FlurryAgent.logEvent("share_action");

        final String appLink = "https://play.google.com/store/apps/details?id=net.mapoint";

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, appLink);
        sendIntent.setType("text/plain");
        context.startActivity(sendIntent);
    }

    private Navigator() {
    }

}
