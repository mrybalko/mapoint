package net.mapoint.ui

import android.os.Bundle
import android.support.design.widget.BottomSheetDialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import net.mapoint.R

class DislikeMenu: BottomSheetDialogFragment() {

    companion object {

        fun newInstance(): DislikeMenu {
            return DislikeMenu()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View? = inflater?.inflate(R.layout.fr_dislike_menu, container, false)

        view?.findViewById<TextView>(R.id.dislike_menu_not_like)?.setOnClickListener({dismiss()})
        view?.findViewById<TextView>(R.id.dislike_menu_spam)?.setOnClickListener({dismiss()})

        return view
    }
}