package net.mapoint.ui.adding.time

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.text.TextUtils
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import kotlinx.android.synthetic.main.ac_adding_date_time.*

import net.mapoint.R
import net.mapoint.data.adding.AddingEventDateTimeData
import net.mapoint.ui.base.BaseActivity
import net.mapoint.utils.list.BaseViewHolder
import net.mapoint.utils.list.ListAdapter
import net.mapoint.utils.list.ListDataSet
import kotlinx.android.synthetic.main.v_top_view.*

import java.util.ArrayList

class AddingDateTimeActivity : BaseActivity<AddingDateTimePresenter>(), AddingDateTimeView {

    companion object {

        fun newIntent(context: Context): Intent {
            val intent = Intent(context, AddingDateTimeActivity::class.java)
            return intent
        }
    }

    private lateinit var adapter: ListAdapter<AddingEventDateTimeData>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initViews()

        findViewById<View>(R.id.back_btn).setOnClickListener { view -> finish() }

        addingDateTimeWidget.setOnAddClickedAction { dates ->  presenter().onAddedDates(dates)}
    }

    private fun initViews() {

        initRecycler()
        initDoneBtn()
    }

    private fun initRecycler() {
        val layoutManager = LinearLayoutManager(this@AddingDateTimeActivity)
        recycler.layoutManager = layoutManager
        val dividerItemDecoration = DividerItemDecoration(this@AddingDateTimeActivity, layoutManager.orientation)

        recycler.addItemDecoration(dividerItemDecoration)

        adapter = ListAdapter(
                { context, view -> DateTimesListItemHolder(view) },
                { R.layout.li_adding_date_time },
                ListDataSet(ArrayList()))

        adapter.setOnItemClickListener { adapterView, view, position, id ->
            val (startDate, endDate, times) = adapter.getItem(position)
        }

        recycler.adapter = adapter
    }

    private fun initDoneBtn() {
        actionBarRightText.text = getString(R.string.done)
        actionBarRightText.setOnClickListener {presenter().onDoneClicked() }
    }

    override fun getLayoutResId(): Int {
        return R.layout.ac_adding_date_time
    }

    override fun presenterProvider(): AddingDateTimePresenter {
        return AddingDateTimePresenter(this)
    }

    override fun updateDates(dates: MutableList<AddingEventDateTimeData>) {
        adapter.updateData(ListDataSet(ArrayList(dates)))
        adapter.notifyDataSetChanged()
    }

    override fun closeWithResultOk() {
        setResult(Activity.RESULT_OK);
        finish()
    }

    private inner class DateTimesListItemHolder(view: View) : BaseViewHolder<AddingEventDateTimeData>(view) {

        private val TAG = "DateTimesListItemHolder"

        private val datesView: TextView
        private val timesView: TextView
        private val removeActionView: ImageView

        init {
            datesView = view.findViewById(R.id.datesView)
            timesView = view.findViewById(R.id.timesView)
            removeActionView = view.findViewById(R.id.removeAction)
        }

        override fun onBind(data: AddingEventDateTimeData) {
            super.onBind(data)

            val datesValue:String = data.startDate + if (!TextUtils.isEmpty(data.endDate)) " - " + data.endDate else ""
            datesView.text = datesValue

            var timesValue:String = ""
            for (time in data.times) {
                timesValue += "\n" + time
            }
            if(!TextUtils.isEmpty(timesValue)) {
                timesView.text = timesValue
                timesView.visibility = View.VISIBLE
            }

            removeActionView.setOnClickListener({view -> presenter().onRemoveItemClicked(data)})
        }
    }
}
