package net.mapoint.ui.adding.parameter.single;

import android.util.Log;

import net.mapoint.data.adding.ParameterData;
import net.mapoint.data.category.CategoryData;
import net.mapoint.data.category.CategoryGroupData;
import net.mapoint.domain.api.locations.data.search.SearchManager;
import net.mapoint.managers.CategoriesManager;
import net.mapoint.ui.adding.ParameterType;
import net.mapoint.ui.base.BasePresenter;
import net.mapoint.utils.LogUtils;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;

public class AddingParameterPresenter extends BasePresenter<AddingParameterView> {

    private static final String TAG = "AddingParameterPrtr";

    private SearchManager searchManager;
    private CategoriesManager categoriesManager;
    private ParameterType parameterType;

    private ParameterData selectedParameter;

    public AddingParameterPresenter(AddingParameterView view) {
        super(view);

        searchManager = SearchManager.instance();
        categoriesManager = CategoriesManager.instance();
    }

    public void parameterType(ParameterType parameterType, String value) {
        this.parameterType = parameterType;
        if (value != null) {
            runIfView(view -> view.updateEnteredText(value));
        }
    }

    @Override
    public void updateModel() {
        super.updateModel();

        if (parameterType == ParameterType.Category) {
            addSubscription(categoriesManager.updateCategories()
                    .subscribe((either) -> {
                                if (either.isLeft()) {
                                    runIfView(view -> view.updateValues());
                                }
                            },
                            throwable -> LogUtils.log(TAG, ".updateCategories", throwable)));
        }
    }

    void onDoneClicked() {
        if (selectedParameter == null) {
            runIfView(view -> view.showCreatingConfirmationDialog(parameterType, s -> { }));
        } else {
            runIfView(view -> view.closeWith(selectedParameter, parameterType));
        }
    }

    public void onRequestTextUpdated(String enteredText) {
        if (selectedParameter != null && !selectedParameter.value().equals(enteredText)) {
            selectedParameter = null;
        }

        addSubscription(getFilteredParameters(parameterType, enteredText)
                .subscribe(filteredParameters -> {

                            selectedParameter = getParameterByValue(enteredText, filteredParameters);
                            runIfView(view -> view.updateHints(filteredParameters));
                        },
                        throwable -> Log.e(TAG, "onRequestTextUpdated", throwable)));
    }

    public void onItemClicked(ParameterData parameterData) {
        selectedParameter = new ParameterData(
                parameterData.id(),
                parameterData.value());

        runIfView(view -> view.updateEnteredText(parameterData.value()));
    }

    public void onClearClicked() {
        runIfView(view -> view.clearEnteredText());
        selectedParameter = null;
    }

    private Observable<List<ParameterData>>
    getFilteredParameters(ParameterType parameterType, String enteredText) {

        switch (parameterType) {
            case LocationName: {
                return fetchLocations(enteredText);
            }

            case Category: {
                return fetchCategories(enteredText);
            }
        }

        return Observable.just(new ArrayList<>());
    }

    private Observable<List<ParameterData>> fetchLocations(String enteredText) {
        return searchManager.getLocationsByNamePart(enteredText)
                .first()
                .map(either -> {
                    if (either.isLeft()) {
                        return either.left().get();
                    } else {

                        //TODO : showError
                    }

                    return new ArrayList<>();
                });
    }

    private Observable<List<ParameterData>> fetchCategories(String enteredText) {
        List<ParameterData> categories = convertCategoriesData();
        List<ParameterData> filteredCategories = filteredParameters(enteredText, categories);
        return Observable.just(filteredCategories);
    }

    ////////

    private ParameterData getParameterByValue(String value, List<ParameterData> parameters) {
        for (ParameterData parameter : parameters) {

            String parameterValueLow = parameter.value().toLowerCase();
            String valueLow = value.toLowerCase();
            if (parameterValueLow.equals(valueLow)) {
                return parameter;
            }
        }

        return null;
    }

    private List<ParameterData> convertCategoriesData() {
        List<ParameterData> categories = new ArrayList<>();

        List<CategoryGroupData> groups = categoriesManager.getLocalCategories();
        for (CategoryGroupData group : groups) {

            List<CategoryData> subcategories = group.getSubcategoryList();

            for (CategoryData categoryData : subcategories) {
                categories.add(new ParameterData(
                        categoryData.getId(),
                        categoryData.getName()));
            }
        }

        return new ArrayList<>(categories);
    }

    private List<ParameterData> filteredParameters(String enteredText, List<ParameterData> parameters) {
        List<ParameterData> filteredParameters = new ArrayList<>();

        for (ParameterData parameter : parameters) {

            String parameterValueLow = parameter.value().toLowerCase();
            String enteredTextLow = enteredText.toLowerCase();
            if (enteredTextLow.equals("") || parameterValueLow.contains(enteredTextLow)) {
                filteredParameters.add(parameter);
            }
        }

        return filteredParameters;
    }
}
