package net.mapoint.ui.adding.time

import net.mapoint.data.adding.AddingEventDateTimeData

interface AddingDateTimeView {

    fun updateDates(dates: MutableList<AddingEventDateTimeData>)
    fun closeWithResultOk()
}
