package net.mapoint.ui.adding.telephone;
import android.text.TextUtils;

import net.mapoint.ui.base.BasePresenter;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AddingPhonePresenter extends BasePresenter<AddingPhoneView> {

    private static final String TAG = "AddingParameterPrtr";

    private Set<String> phones;

    public AddingPhonePresenter(AddingPhoneView view) {
        super(view);

        phones = new HashSet<>();
    }

    void onDoneClicked() {
        if (phones.size() > 0) {
            runIfView(view -> view.closeWith(phones));
        }
    }

    public void onAddedClicked(String newPhone) {

        if (validateUserPhone(newPhone)) {
            phones.add(newPhone);
            runIfView(view -> {
                view.updatePhones(phones);
                view.clearEditPhoneView();
            });

        }
    }

    private boolean validateUserPhone(String phone) {
        Pattern pattern = Pattern.compile("^\\+\\d{3}\\-\\d{2}\\-\\d{3}\\-\\d{2}\\-\\d{2}$");
        Matcher matcher = pattern.matcher(phone);

        return !TextUtils.isEmpty(phone) && matcher.matches();
    }

    public void setPhones(Set<String> phones) {
        this.phones = new HashSet<>(phones);
        runIfView(view -> view.updatePhones(phones));
    }

    void onPhoneChanged(String value, String template) {
        String newText = matchToTemplate(value, template);
        runIfView(view -> view.updateFormattedPhone(newText));
    }

    void onEditPhone(String value) {
        phones.remove(value);
        runIfView(view -> {
            view.editPhone(value);
            view.updatePhones(phones);
        });
    }

    private String matchToTemplate(String raw, String template){
        StringBuilder newString  = new StringBuilder();
        int rawLength = raw.length();

        int rawIndex = 0;
        for (int templateIndex = 0; templateIndex < template.length(); templateIndex++) {
            String symbol = template.substring(templateIndex, templateIndex+1);
            if (rawIndex < rawLength) {
                if ("x".equals(symbol)) {
                    symbol = "";
                    while(rawIndex < rawLength) {
                        char rawChar = raw.charAt(rawIndex);
                        if (Character.isDigit(rawChar)) {
                            symbol = String.valueOf(rawChar);
                            rawIndex++;
                            break;
                        }
                        rawIndex++;
                    }
                }

                newString.append(symbol);
            } else {
                break;
            }
        }

        return newString.toString();
    }
}
