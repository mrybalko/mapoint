package net.mapoint.ui.adding;

public enum ParameterType {
    LocationName(1),
    Category(2),
    Coordinates(3),
    Phone(4),
    Link(5),
    EventName(6),
    FullText(7),
    Address(8),
    Time(9);

    private int value;

    ParameterType(int value) {
        this.value = value;
    }

    public int value() {
        return value;
    }
}
