package net.mapoint.ui.adding.parameter.mutli;

import net.mapoint.data.adding.ParameterData;
import net.mapoint.ui.adding.ParameterType;

import java.util.List;

public interface AddingMultiParametersView {

    void updateHints(List<ParameterData> hints);
    void clearEnteredText();

    void closeWith(List<ParameterData> parameterData, ParameterType parameterType);

    void updateValues();
}