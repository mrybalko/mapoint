package net.mapoint.ui.adding.time

import net.mapoint.data.adding.AddingEventDateTimeData
import net.mapoint.managers.DatesTimeManager
import net.mapoint.ui.base.BasePresenter

class AddingDateTimePresenter(view: AddingDateTimeView) : BasePresenter<AddingDateTimeView>(view) {
    companion object {

        private val TAG = "AddingDateTimePresenter"
    }

    private var dateTimes: MutableList<AddingEventDateTimeData>

    init {
        dateTimes = DatesTimeManager.datesTime()
    }

    override fun updateModel() {
        super.updateModel()
        updateViews()
    }

    fun onAddedDates(newDatesTimes: AddingEventDateTimeData) {
        dateTimes.add(newDatesTimes)
        updateViews()
    }

    fun onRemoveItemClicked(datesTimesForRemove: AddingEventDateTimeData) {
        dateTimes.remove(datesTimesForRemove)
        updateViews()
    }

    fun onDoneClicked() {
        DatesTimeManager.setDatesTime(dateTimes)
        runIfView{view -> view.closeWithResultOk()}
    }

    fun updateViews(){
        runIfView({view -> view.updateDates(dateTimes)})
    }
}
