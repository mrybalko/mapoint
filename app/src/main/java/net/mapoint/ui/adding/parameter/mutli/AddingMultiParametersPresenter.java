package net.mapoint.ui.adding.parameter.mutli;

import android.util.Log;

import net.mapoint.data.adding.ParameterData;
import net.mapoint.data.category.CategoryData;
import net.mapoint.data.category.CategoryGroupData;
import net.mapoint.domain.api.locations.data.search.SearchManager;
import net.mapoint.managers.CategoriesManager;
import net.mapoint.ui.adding.ParameterType;
import net.mapoint.ui.base.BasePresenter;
import net.mapoint.utils.LogUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import rx.Observable;

public class AddingMultiParametersPresenter extends BasePresenter<AddingMultiParametersView> {

    private static final String TAG = "AddingParameterPrtr";

    private SearchManager searchManager;
    private CategoriesManager categoriesManager;
    private ParameterType parameterType;

    private Set<String> selectedStringParameters;

    public AddingMultiParametersPresenter(AddingMultiParametersView view) {
        super(view);

        searchManager = SearchManager.instance();
        categoriesManager = CategoriesManager.instance();
    }

    public void setupSelectedStringParameters(ParameterType parameterType, ArrayList<String> parameters) {
        this.parameterType = parameterType;
        if (parameters != null) {
            selectedStringParameters = new HashSet<>(parameters);
        }
    }

    @Override
    public void updateModel() {
        super.updateModel();

        if (parameterType == ParameterType.Category) {
            addSubscription(categoriesManager.updateCategories()
                    .subscribe((either) -> {
                                if (either.isLeft()) {
                                    runIfView(view -> view.updateValues());
                                }
                            },
                            throwable -> LogUtils.log(TAG, ".updateCategories", throwable)));
        }
    }

    void onDoneClicked() {
        List<ParameterData> selectedParameterDataList = new ArrayList<>();
        if (parameterType == ParameterType.Category) {
            List<ParameterData> parameterDataList = toParameterFromLocalCategories();
            for (ParameterData parameterData : parameterDataList){
                if (parameterData.isSelected()) {
                    selectedParameterDataList.add(parameterData);
                }
            }

            runIfView(view -> view.closeWith(selectedParameterDataList, parameterType));
        }
    }

    void onRequestTextUpdated(String enteredText) {
        addSubscription(getFilteredParameters(parameterType, enteredText)
                .subscribe(filteredParameters -> runIfView(view -> view.updateHints(filteredParameters)),
                        throwable -> Log.e(TAG, "onRequestTextUpdated", throwable)));
    }

    void onItemClicked(ParameterData parameterData) {
        String value = parameterData.value();
        if (selectedStringParameters.contains(value)) {
            selectedStringParameters.remove(value);
        } else {
            selectedStringParameters.add(value);
        }

        runIfView(view -> view.updateValues());
    }

    void onClearClicked() {
        runIfView(view -> view.clearEnteredText());
    }

    private Observable<List<ParameterData>>
    getFilteredParameters(ParameterType parameterType, String enteredText) {

        switch (parameterType) {
            case LocationName: {
                return fetchLocations(enteredText);
            }

            case Category: {
                return fetchCategories(enteredText);
            }
        }

        return Observable.just(new ArrayList<>());
    }

    private Observable<List<ParameterData>> fetchLocations(String enteredText) {
        return searchManager.getLocationsByNamePart(enteredText)
                .first()
                .map(either -> {
                    if (either.isLeft()) {
                        return either.left().get();
                    } else {

                        //TODO : showError
                    }

                    return new ArrayList<>();
                });
    }

    private Observable<List<ParameterData>> fetchCategories(String enteredText) {
        List<ParameterData> categories = toParameterFromLocalCategories();
        List<ParameterData> filteredCategories = filteredParametersByRequestText(enteredText, categories);
        return Observable.just(filteredCategories);
    }

    ////////

    private List<ParameterData> toParameterFromLocalCategories() {
        List<ParameterData> categories = new ArrayList<>();

        List<CategoryGroupData> groups = categoriesManager.getLocalCategories();
        for (CategoryGroupData group : groups) {

            List<CategoryData> subcategories = group.getSubcategoryList();

            for (CategoryData categoryData : subcategories) {
                categories.add(new ParameterData(
                        categoryData.getId(),
                        categoryData.getName(),
                        selectedStringParameters.contains(categoryData.getName())));
            }
        }

        return new ArrayList<>(categories);
    }

    private List<ParameterData> filteredParametersByRequestText(String enteredText, List<ParameterData> parameters) {
        List<ParameterData> filteredParameters = new ArrayList<>();

        for (ParameterData parameter : parameters) {

            String parameterValueLow = parameter.value().toLowerCase();
            String enteredTextLow = enteredText.toLowerCase();
            if (enteredTextLow.equals("") || parameterValueLow.contains(enteredTextLow)) {
                filteredParameters.add(parameter);
            }
        }

        return filteredParameters;
    }
}
