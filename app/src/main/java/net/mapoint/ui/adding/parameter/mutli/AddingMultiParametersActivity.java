package net.mapoint.ui.adding.parameter.mutli;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import net.mapoint.R;
import net.mapoint.data.adding.ParameterData;
import net.mapoint.ui.adding.ParameterType;
import net.mapoint.ui.base.BaseActivity;
import net.mapoint.utils.list.BaseViewHolder;
import net.mapoint.utils.list.ListAdapter;
import net.mapoint.utils.list.ListDataSet;

import java.util.ArrayList;
import java.util.List;

public class AddingMultiParametersActivity extends BaseActivity<AddingMultiParametersPresenter> implements AddingMultiParametersView {

    private View rootView;

    private EditText requestEt;
    private ImageView clearEt;
    private TextView doneBtn;
    private RecyclerView recyclerView;
    private ListAdapter<ParameterData> adapter;

    public static final String KEY_ADDING_PARAMETER_TYPE = "event_adding_parameter_type";
    private static final String KEY_ADDING_PARAMETERS_VALUE = "event_adding_parameters_value";

    public static final String KEY_ADDING_PARAMETER_RESULT_DATA_LIST = "event_adding_parameter_result_data_list";

    public static Intent newIntent(final Context context,
                                   final ParameterType parameterType,
                                   final ArrayList<String> parameters) {

        Intent intent = new Intent(context, AddingMultiParametersActivity.class);
        intent.putExtra(KEY_ADDING_PARAMETER_TYPE, parameterType.value());
        intent.putStringArrayListExtra(KEY_ADDING_PARAMETERS_VALUE, parameters);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        rootView = findViewById(R.id.root_view);
        initViews();

        int parameterTypeValue = (int) getIntent().getSerializableExtra(KEY_ADDING_PARAMETER_TYPE);
        ParameterType parameterType =  null;
        for (ParameterType tempParameterType : ParameterType.values()) {
            if (parameterTypeValue == tempParameterType.value()) {
                parameterType = tempParameterType;
            }
        }

        ArrayList<String> parameters = (ArrayList<String>) getIntent().getSerializableExtra(KEY_ADDING_PARAMETERS_VALUE);
        if (parameters != null) {
            presenter().setupSelectedStringParameters(parameterType, parameters);
        }

        findViewById(R.id.back_btn).setOnClickListener(view -> finish());
    }

    private void initViews() {

        initRequestEditText();
        initRecycler();
        initDoneBtn();
    }

    private void initRequestEditText() {
        requestEt = rootView.findViewById(R.id.auto_complete);
        requestEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                presenter().onRequestTextUpdated(s.toString());
            }
        });

        clearEt = rootView.findViewById(R.id.clear_btn);
        clearEt.setOnClickListener(v -> {
            presenter().onClearClicked();
        });
    }

    private void initRecycler() {
        recyclerView = rootView.findViewById(R.id.recycler);

        LinearLayoutManager layoutManager = new LinearLayoutManager(AddingMultiParametersActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(
                recyclerView.getContext(),
                layoutManager.getOrientation());

        recyclerView.addItemDecoration(dividerItemDecoration);

        adapter = new ListAdapter(
                (context, view) -> new HintsListItemHolder(view),
                () -> R.layout.li_multi_parameters,
                new ListDataSet(new ArrayList<String>()));

        adapter.setOnItemClickListener((adapterView, view, position, id) -> {
            ParameterData data = adapter.getItem(position);
            presenter().onItemClicked(data);
        });

        recyclerView.setAdapter(adapter);
    }

    private void initDoneBtn() {
        doneBtn = rootView.findViewById(R.id.actionBarRightText);
        doneBtn.setOnClickListener(view -> presenter().onDoneClicked());
        doneBtn.setText(getString(R.string.done));
    }

    @Override
    public int getLayoutResId() {
        return R.layout.ac_adding_parameter;
    }

    @Override
    public AddingMultiParametersPresenter presenterProvider() {
        return new AddingMultiParametersPresenter(this);
    }

    @Override
    public void updateHints(List<ParameterData> hints) {
        if (hints.size() > 0) {
            adapter.updateData(new ListDataSet<>(hints));
            adapter.notifyDataSetChanged();
            recyclerView.setVisibility(View.VISIBLE);
        } else {
            recyclerView.setVisibility(View.GONE);
        }
    }

    @Override
    public void clearEnteredText() {
        requestEt.setText("");
    }

    @Override
    public void closeWith(List<ParameterData> selectedParameters, ParameterType parameterType) {
        Intent intent = new Intent();
        intent.putExtra(KEY_ADDING_PARAMETER_TYPE, parameterType.value());
        intent.putExtra(KEY_ADDING_PARAMETER_RESULT_DATA_LIST, new ArrayList<>(selectedParameters));
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    @Override
    public void updateValues() {
        presenter().onRequestTextUpdated(requestEt.getText().toString());
    }

    private final class HintsListItemHolder extends BaseViewHolder<ParameterData> {

        private TextView hintValue;
        private CheckBox checkBox;

        public HintsListItemHolder(View view) {
            super(view);

            hintValue = view.findViewById(R.id.text);
            checkBox = view.findViewById(R.id.checkbox);
        }

        @Override
        public void onBind(ParameterData data) {
            super.onBind(data);

            hintValue.setText(data.value());
            checkBox.setChecked(data.isSelected());
        }
    }
}
