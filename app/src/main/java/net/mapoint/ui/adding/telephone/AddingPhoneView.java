package net.mapoint.ui.adding.telephone;

import java.util.Set;

public interface AddingPhoneView {

    void updatePhones(Set<String> phones);
    void closeWith(Set<String> phones);
    void updateFormattedPhone(String formattedPhone);
    void editPhone(String phone);
    void clearEditPhoneView();
}
