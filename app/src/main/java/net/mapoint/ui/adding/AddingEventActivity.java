package net.mapoint.ui.adding;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;

import net.mapoint.R;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import net.mapoint.data.adding.AddingEventData;
import net.mapoint.data.adding.AddingEventDateTimeData;
import net.mapoint.data.adding.SearchLocationData;
import net.mapoint.managers.CategoriesManager;
import net.mapoint.ui.adding.parameter.mutli.AddingMultiParametersActivity;
import net.mapoint.ui.adding.parameter.single.AddingParameterActivity;
import net.mapoint.data.adding.ParameterData;
import net.mapoint.ui.base.BaseActivity;
import net.mapoint.ui.main.location.LocationUtils;
import net.mapoint.utils.MarkerBitmapProvider;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import rx.functions.Action1;

import static android.view.View.GONE;
import static net.mapoint.utils.Constants.REQUEST_CODE_ADD_CATEGORY;
import static net.mapoint.utils.Constants.REQUEST_CODE_ADD_DATE_TIME;
import static net.mapoint.utils.Constants.REQUEST_CODE_ADD_PARAMETER;
import static net.mapoint.utils.Constants.REQUEST_CODE_ADD_PHONE;

public class AddingEventActivity extends BaseActivity<AddingEventPresenter> implements AddingEventView {

    private GoogleMap googleMap;

    private View rootView;
    private View progressHolder;

    private EditText locationNameTv;
    private EditText addressEt;
    private EditText eventNameEt;
    private EditText categoryTv;
    private EditText phonesTv;
    private EditText linkTv;
    private EditText fullDescriptionTv;
    private EditText timeTv;

    private TextView addingBtn;

    private boolean onMapClickEnabled;

    public static Intent newIntent(final Context context) {

        Intent intent = new Intent(context, AddingEventActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        rootView = findViewById(R.id.root_view);
        initViews();

        findViewById(R.id.back_btn).setOnClickListener(view -> finish());

        onMapClickEnabled = true;
    }

    private void initViews() {

        progressHolder = findViewById(R.id.progress_holder);

        locationNameTv = rootView.findViewById(R.id.event_location_name);
        setFocusable(locationNameTv, false);
        locationNameTv.setOnClickListener(v -> presenter().onLocationNameClicked(locationNameTv.getText().toString()));

        addressEt = rootView.findViewById(R.id.event_address);
        subscribeOnTextChanged(addressEt, value -> presenter().updateParameter(ParameterType.Address, value));

        eventNameEt = rootView.findViewById(R.id.short_description);
        subscribeOnTextChanged(eventNameEt, (Action1<String>) value -> presenter().updateParameter(ParameterType.EventName, value));

        categoryTv = rootView.findViewById(R.id.event_category);
        setFocusable(categoryTv, false);
        categoryTv.setOnClickListener(v -> presenter().onCategoryClicked(categoryTv.getText().toString()));

        phonesTv = rootView.findViewById(R.id.event_phones);
        setFocusable(phonesTv, false);
        phonesTv.setOnClickListener(v -> presenter().onPhoneClicked(phonesTv.getText().toString()));

        timeTv = rootView.findViewById(R.id.event_date_times);
        setFocusable(timeTv, false);
        timeTv.setOnClickListener(v -> presenter().onTimeClicked(timeTv.getText().toString()));

        linkTv = rootView.findViewById(R.id.link);
        subscribeOnTextChanged(linkTv, (Action1<String>) value -> presenter().updateParameter(ParameterType.Link, value));

        fullDescriptionTv = rootView.findViewById(R.id.event_full_description);
        subscribeOnTextChanged(fullDescriptionTv, (Action1<String>) value -> presenter().updateParameter(ParameterType.FullText, value));

        initAddingBtn();
    }

    private void initAddingBtn() {
        addingBtn = rootView.findViewById(R.id.adding_btn);
        addingBtn.setOnClickListener(view -> presenter().onAddingBtnClicked());
    }

    @Override
    public int getLayoutResId() {
        return R.layout.ac_adding_event;
    }

    @Override
    public AddingEventPresenter presenterProvider() {
        return new AddingEventPresenter(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;

        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setCompassEnabled(false);
        googleMap.getUiSettings().setZoomGesturesEnabled(true);
        checkAndRequestLocationPermission(rootView,
                () -> googleMap.setMyLocationEnabled(true));

        googleMap.setOnMapClickListener(latLng -> presenter().onMapClicked(latLng));

        presenter().onMapReady();
    }

    private void enableMapInteraction(boolean enable) {
        onMapClickEnabled = enable;
    }

    @Override
    public void goToCurrentLocation(Location location) {
        googleMap.clear();
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 14.0f));
    }

    @Override
    public void updateData(AddingEventData addingEventData) {
        updateEventsViews(addingEventData);
        eventViewsState();
    }

    @Override
    public void close() {
        finish();
    }

    @Override
    public void showInfoDialog() {
        showInfoDialog("", getString(R.string.fill_required_fields), getString(R.string.OK), (dialog, which) -> dialog.dismiss());
    }

    private void updateEventsViews(AddingEventData data) {

        updateFactsViews(data);

        updateTextViewValue(eventNameEt, data.getText());

        updateCategories(data.getCategories());

        updateTextViewValue(phonesTv, data.getPhones());
        updateTextViewValue(fullDescriptionTv, data.getFullText());
        updateTextViewValue(timeTv, datesTimeToString(data.getDatesTime()));
    }

    private void updateCategories(List<ParameterData> categories) {

        String categoriesString = "";
        if (categories != null && !categories.isEmpty()) {

            List<String> categoriesValues = new ArrayList<>();
            for (ParameterData parameterData: categories) {
                categoriesValues.add(parameterData.value());
            }

            categoriesString = LocationUtils.subcategoriesUIString(categoriesValues);
        }

        updateTextViewValue(categoryTv, categoriesString);
    }

    private String datesTimeToString(List<AddingEventDateTimeData> datesTime) {

        if (datesTime != null) {
            StringBuilder datesTimeString = new StringBuilder();
            for (AddingEventDateTimeData dateTimeData : datesTime) {

                if (datesTimeString.length()>0) {
                    datesTimeString.append("\n\n");
                }
                datesTimeString.append(dateTimeData.getStartDate());

                String endDate = dateTimeData.getEndDate();
                if (!TextUtils.isEmpty(endDate)) {
                    datesTimeString.append(" - ");
                    datesTimeString.append(endDate);
                }

                for(String time : dateTimeData.getTimes()) {
                    datesTimeString.append("\n");
                    datesTimeString.append(time);
                }
            }

            return datesTimeString.toString();
        }

        return "";
    }

    private void updateFactsViews(AddingEventData data) {

        updateTextViewValue(locationNameTv, data.getLocationNameValue());
        updateAddressView(data.getAddress());
        updateTextViewValue(linkTv, data.getLink());

        if (data.getLat() != null && data.getLng() != null) {
            updateMarker(new LatLng(data.getLat(), data.getLng()));
            enableMapInteraction(false);
        } else {
            enableMapInteraction(true);
        }
    }

    private void factViewsState() {
        eventNameEt.setVisibility(GONE);
        categoryTv.setVisibility(GONE);
        phonesTv.setVisibility(GONE);
        fullDescriptionTv.setVisibility(GONE);
    }

    private void eventViewsState() {
        eventNameEt.setVisibility(View.VISIBLE);
        categoryTv.setVisibility(View.VISIBLE);
        phonesTv.setVisibility(View.VISIBLE);
        fullDescriptionTv.setVisibility(View.VISIBLE);
    }

    private void updateAddressView(String value) {
        setFocusable(addressEt, !TextUtils.isEmpty(value));
        addressEt.setText(value);
    }

    @Override
    public void updateMarker(@Nullable LatLng latLng) {
        googleMap.clear();
        googleMap.addMarker(new MarkerOptions()
                .position(latLng)
                .title("")
                .snippet("")
                .icon(BitmapDescriptorFactory.fromBitmap
                        (MarkerBitmapProvider.getBitmapByColor(CategoriesManager.ADD_EVENT_COLOR))));

        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15.0f));
    }

    @Override
    public void goToAddingParameter(final ParameterType parameterType,
                                    final String value) {
        if (parameterType == ParameterType.Phone) {
            navigator().goToAddingPhone(AddingEventActivity.this,
                    value);

        } else if (parameterType == ParameterType.Time) {
            navigator().goToAddingTime(AddingEventActivity.this);

        } else if (parameterType == ParameterType.LocationName) {
            navigator().goToAddingParameter(AddingEventActivity.this,
                    parameterType,
                    value);
        } else if (parameterType == ParameterType.Category) {
            navigator().goToAddingCategory(AddingEventActivity.this, LocationUtils.subcategoriesFromUIString(value));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_CODE_ADD_PARAMETER && resultCode == Activity.RESULT_OK) {
            ParameterData parameterData = (ParameterData) data.getSerializableExtra(AddingParameterActivity.KEY_ADDING_PARAMETER_RESULT_DATA);
            int parameterTypeValue = (int) data.getSerializableExtra(AddingParameterActivity.KEY_ADDING_PARAMETER_TYPE);

            ParameterType parameterType =  null;
            for (ParameterType tempParameterType : ParameterType.values()) {
                if (parameterTypeValue == tempParameterType.value()) {
                    parameterType = tempParameterType;
                }
            }

            if (parameterData != null && parameterData.value() != null && parameterType != null) {
                presenter().onParameterUpdated(parameterType, Arrays.asList(parameterData));
            }
        } else if (requestCode == REQUEST_CODE_ADD_CATEGORY && resultCode == Activity.RESULT_OK) {

            ArrayList<ParameterData> categories = (ArrayList<ParameterData>) data.getSerializableExtra(AddingMultiParametersActivity.KEY_ADDING_PARAMETER_RESULT_DATA_LIST);
            presenter().onParameterUpdated(ParameterType.Category, categories);

        } else if (requestCode == REQUEST_CODE_ADD_PHONE && resultCode == Activity.RESULT_OK) {
            String phonesString = (String) data.getSerializableExtra(AddingParameterActivity.KEY_ADDING_PARAMETER_RESULT_DATA);
            presenter().updateParameter(ParameterType.Phone, phonesString);

        } else if (requestCode == REQUEST_CODE_ADD_DATE_TIME && resultCode == Activity.RESULT_OK) {
            presenter().updateDates();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void showProgress(boolean inProgress) {
        progressHolder.setVisibility(inProgress ? View.VISIBLE : GONE);
    }

    @Override
    public void showSingleChoiceDialog(List<SearchLocationData> locationDataList) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(AddingEventActivity.this);

        builder.setTitle(R.string.select_location_name);

        StringListAdapter adapter = new StringListAdapter();
        adapter.updateData(locationDataList);

        builder.setSingleChoiceItems(adapter, -1, (dialog, which) -> {
            if (which < locationDataList.size()) {
                presenter().onSelectedNameByCoordinates(locationDataList.get(which));
            }

            dialog.dismiss();
        });

        builder.setNegativeButton(R.string.cancel, (dialog, which) -> dialog.dismiss());
        builder.create().show();
    }

    private class StringListAdapter extends BaseAdapter {

        private List<SearchLocationData> dataList;

        public StringListAdapter() {
            dataList = new ArrayList<>();
        }

        public void updateData(List<SearchLocationData> data) {
            this.dataList = data;
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return dataList.size();
        }

        @Override
        public SearchLocationData getItem(int position) {
            return dataList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            convertView = LayoutInflater.from(AddingEventActivity.this).
                        inflate(R.layout.li_location_description, parent, false);

            TextView nameView = (TextView) convertView.findViewById(R.id.name);
            TextView typeView = (TextView) convertView.findViewById(R.id.type);
            TextView addressView = (TextView) convertView.findViewById(R.id.address);
            SearchLocationData locationData = dataList.get(position);

            if (!TextUtils.isEmpty(locationData.getName())) {
                nameView.setText(locationData.getName());
                nameView.setVisibility(View.VISIBLE);

                if (!TextUtils.isEmpty(locationData.getType())) {
                    typeView.setText(locationData.getType());
                    typeView.setVisibility(View.VISIBLE);
                } else {
                    typeView.setVisibility(GONE);
                }


                addressView.setVisibility(View.GONE);
            } else {
                nameView.setVisibility(GONE);
                typeView.setVisibility(GONE);

                addressView.setVisibility(View.VISIBLE);
                addressView.setText(locationData.getAddress());
            }

            return convertView;
        }
    }

    private void subscribeOnTextChanged(EditText editText, Action1<String> action) {
        editText.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                action.call(s.toString());
            }
        });
    }

    private void setFocusable(EditText editText, boolean focusable) {
        editText.setFocusable(focusable);
    }

    private void updateTextViewValue(TextView textView, String value) {
        int typeface = Typeface.NORMAL;

        if (!TextUtils.isEmpty(value)) {
            typeface = Typeface.BOLD;
        }

        textView.setTypeface(null, typeface);
        textView.setText(value);
    }
}
