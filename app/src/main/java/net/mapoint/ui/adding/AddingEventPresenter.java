package net.mapoint.ui.adding;

import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import net.mapoint.R;
import net.mapoint.data.adding.AddingEventData;
import net.mapoint.data.adding.ParameterData;
import net.mapoint.data.adding.SearchLocationData;
import net.mapoint.domain.api.common.CommunicationError;
import net.mapoint.domain.api.locations.data.LocationMapper;
import net.mapoint.domain.api.locations.data.search.SearchManager;
import net.mapoint.managers.CategoriesManager;
import net.mapoint.managers.DatesTimeManager;
import net.mapoint.managers.LocationManager;
import net.mapoint.ui.base.BasePresenter;
import net.mapoint.utils.Either;

import java.util.Iterator;
import java.util.List;

import rx.Observable;

public class AddingEventPresenter extends BasePresenter<AddingEventView> {

    private static final String TAG = "AddingEventPresenter";

    private LocationManager locationManager;
    private SearchManager searchManager;

    private ParameterData locationName;

    private AddingEventData.Builder addingEventDataBuilder;

    public AddingEventPresenter(AddingEventView view) {
        super(view);

        DatesTimeManager.INSTANCE.resetDatesTime();
        searchManager = SearchManager.instance();
        locationManager = LocationManager.instance();
        addingEventDataBuilder = new AddingEventData.Builder();
    }

    @Override
    public void updateModel() {
        super.updateModel();

        if (locationName != null) {
            String id = locationName.id();

            SearchLocationData searchLocationData = searchManager.getLocationData(id);

            if (searchLocationData != null) {
                addingEventDataBuilder
                        .lat(searchLocationData.getLat())
                        .lng(searchLocationData.getLng())
                        .address(searchLocationData.getAddress())
                        .locationName(locationName);

            } else {
                goToCurrentLocation();

                addingEventDataBuilder
                        .lat(0.0f)
                        .lng(0.0f)
                        .address("")
                        .locationName(locationName);
            }

            updateDataOnView();
        }

        updateDataOnView();
    }

    public void onMapReady() {
        goToCurrentLocation();
    }

    private void updateDataOnView() {
        runIfView(view -> view.updateData(addingEventDataBuilder.build()));
    }

    private void goToCurrentLocation() {
        addSubscription(locationManager.currentLocation()
        .first()
        .subscribe(location -> runIfView(view -> view.goToCurrentLocation(location)),
                throwable -> Log.e(TAG, "goToCurrentLocation", throwable)));
    }

    void onMapClicked(LatLng latLng) {
        runIfView(view -> {
            view.updateMarker(latLng);

            addSubscription(
                    withProgress(
                            searchManager.getLocationsByCoord(latLng))
                            .subscribe(list -> {
                                        if (list != null && !list.isEmpty()) {
                                            view.showSingleChoiceDialog(list);
                                        }
                                    },
                                    throwable -> Log.e(TAG, "getLocationsByCoordinates", throwable)));
        });
        //generateAddress(latLng);
    }

    void onAddingBtnClicked() {

        if (isRequestFieldValid()) {
            if (eventIsFact()) {
                addEventToServer(locationManager.addFact(addingEventDataBuilder.build()));
            } else {
                addEventToServer(locationManager.addEvent(addingEventDataBuilder.build()));
            }
        } else {
            runIfView(view -> view.showInfoDialog());
        }
    }

    public boolean eventIsFact() {

        boolean isEventsHasFact = false;

        List<ParameterData> categories = addingEventDataBuilder.getCategories();

        Iterator<ParameterData> iterator = categories.iterator();

        while (iterator.hasNext()) {
            ParameterData data = iterator.next();
            if(data.id().equals(CategoriesManager.FACT_CATEGORY_ITEM_ID)) {
                iterator.remove();
                isEventsHasFact = true;
            }
        }

        return isEventsHasFact && addingEventDataBuilder.getCategories().isEmpty();
    }

    private void addEventToServer(Observable<Either<String, Either<CommunicationError, LocationMapper.LocationsHandledError>>> observable) {
        addSubscription(observable
                .subscribe(either -> {
                    if (either.isLeft()) {
                        showSystemMessage(R.string.event_added);
                        runIfView(view -> view.close());
                    } else {
                        showSystemMessage(R.string.event_added_error);
                    }
                }, throwable -> {
                    Log.d(TAG, "addEventToServer", throwable);
                }));
    }


    private boolean isRequestFieldValid() {
        boolean result = true;

        if (addingEventDataBuilder.getCategories() == null || addingEventDataBuilder.getCategories().isEmpty()) {
            result = false;
        }

        if (TextUtils.isEmpty(addingEventDataBuilder.getAddress())) {
            result = false;
        }

        if (TextUtils.isEmpty(addingEventDataBuilder.getText())) {
            result = false;
        }

        if (addingEventDataBuilder.getLocationName() == null) {
            result = false;
        }

        return result;
    }

    void onCategoryClicked(String value) {
        runIfView(view -> view.goToAddingParameter(ParameterType.Category, value));
    }

    void onLocationNameClicked(String value) {
        runIfView(view -> view.goToAddingParameter(ParameterType.LocationName, value));
    }

    void onPhoneClicked(String value) {
        runIfView(view -> view.goToAddingParameter(ParameterType.Phone, value));
    }

    void onTimeClicked(String value) {
        runIfView(view -> view.goToAddingParameter(ParameterType.Time, value));
    }

    private <Type> Observable<Type> withProgress(Observable<Type> observable) {
        return observable
                .doOnSubscribe(() -> runIfView(view -> view.showProgress(true)))
                .doOnCompleted(() -> runIfView(view -> view.showProgress(false)))
                .doOnError((throwable) -> runIfView(view -> view.showProgress(false)));
    }

    void onParameterUpdated(ParameterType parameterType, List<ParameterData> parameters) {

        switch (parameterType) {
            case Category: {

                addingEventDataBuilder.categories(parameters);
                break;
            }

            case LocationName: {
                if (parameters != null && !parameters.isEmpty()) {
                    ParameterData firstParameter = parameters.get(0);
                    locationName = firstParameter;
                    addingEventDataBuilder.locationName(new ParameterData(firstParameter.id(), firstParameter.value()));
                    try {
                        long id = Long.parseLong(firstParameter.id());
                        addingEventDataBuilder.locationId(id);
                    } catch (RuntimeException ex) {
                        ex.printStackTrace();
                    }
                }
                break;
            }

            default:{}
        }
    }

    void updateParameter(ParameterType parameterType, String value) {
        switch (parameterType) {

            case EventName: {
                addingEventDataBuilder.text(value);
                break;
            }

            case Address: {
                addingEventDataBuilder.address(value);
                break;
            }

            case Link: {
                addingEventDataBuilder.link(value);
                break;
            }

            case FullText: {
                addingEventDataBuilder.fullText(value);
                break;
            }

            case Phone: {
                addingEventDataBuilder.phones(value);
                break;
            }
        }
    }

    void updateDates() {
        addingEventDataBuilder.datesTime(DatesTimeManager.INSTANCE.datesTime());
    }

    void onSelectedNameByCoordinates(SearchLocationData searchLocationData) {
        String name = searchLocationData.getName();
        String nameValue = !TextUtils.isEmpty(name) ? name : "";

        addingEventDataBuilder
                    .locationId(searchLocationData.getId())
                    .address(searchLocationData.getAddress())
                    .lat(searchLocationData.getLat()).lng(searchLocationData.getLng())
                    .locationName(new ParameterData(searchLocationData.getId().toString(), nameValue));

        updateDataOnView();
    }
}
