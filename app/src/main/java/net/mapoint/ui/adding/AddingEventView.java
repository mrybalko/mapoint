package net.mapoint.ui.adding;

import android.location.Location;

import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;

import net.mapoint.data.adding.AddingEventData;
import net.mapoint.data.adding.SearchLocationData;

import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface AddingEventView extends OnMapReadyCallback {

    void updateMarker(@Nullable LatLng latLng);

    void goToAddingParameter(final ParameterType parameterType,
                             final String value);

    void showProgress(boolean inProgress);

    void showSingleChoiceDialog(List<SearchLocationData> locationDataList);

    void goToCurrentLocation(Location location);

    void updateData(AddingEventData addingEventData);

    void close();

    void showInfoDialog();
}
