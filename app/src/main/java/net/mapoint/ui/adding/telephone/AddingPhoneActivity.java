package net.mapoint.ui.adding.telephone;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import net.mapoint.R;
import net.mapoint.ui.base.BaseActivity;
import net.mapoint.utils.data.PhonesUtils;
import net.mapoint.utils.list.BaseViewHolder;
import net.mapoint.utils.list.ListAdapter;
import net.mapoint.utils.list.ListDataSet;

import java.util.ArrayList;
import java.util.Set;

public class AddingPhoneActivity extends BaseActivity<AddingPhonePresenter> implements AddingPhoneView {

    private View rootView;

    private EditText phonetEt;
    private View addIv;
    private TextView doneBtn;
    private RecyclerView recyclerView;
    private ListAdapter<String> adapter;

    private TextWatcher phoneFormatter;

    private static final String KEY_ADDING_PARAMETER_VALUE = "event_adding_parameter_value";

    public static final String KEY_ADDING_PARAMETER_RESULT_DATA = "event_adding_parameter_result_data";

    public static Intent newIntent(final Context context,
                                   final String value) {

        Intent intent = new Intent(context, AddingPhoneActivity.class);
        intent.putExtra(KEY_ADDING_PARAMETER_VALUE, value);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        rootView = findViewById(R.id.root_view);
        initViews();

        String phonesString = getIntent().getStringExtra(KEY_ADDING_PARAMETER_VALUE);
        if (phonesString != null) {
            presenter().setPhones(PhonesUtils.stringToPhones(phonesString));
        }

        findViewById(R.id.back_btn).setOnClickListener(view -> finish());
    }

    private void initViews() {

        initRequestEditText();
        initRecycler();
        initDoneBtn();
    }

    private void initRequestEditText() {
        phonetEt = rootView.findViewById(R.id.enter_phone_et);

        String template = getString(R.string.phone_number_template_blr);

        phoneFormatter = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                phonetEt.removeTextChangedListener(this);
                String raw = editable.toString();
                presenter().onPhoneChanged(raw, template);
            }
        };

        phonetEt.addTextChangedListener(phoneFormatter);

        addIv = rootView.findViewById(R.id.add_phone_btn);
        addIv.setOnClickListener(v -> {
            presenter().onAddedClicked(phonetEt.getText().toString());
        });
    }

    private void initRecycler() {
        recyclerView = rootView.findViewById(R.id.recycler);

        LinearLayoutManager layoutManager = new LinearLayoutManager(AddingPhoneActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(
                recyclerView.getContext(),
                layoutManager.getOrientation());

        recyclerView.addItemDecoration(dividerItemDecoration);

        adapter = new ListAdapter(
                (context, view) -> new PhonesListItemHolder(view),
                () -> R.layout.li_phones,
                new ListDataSet(new ArrayList<String>()));

        adapter.setOnItemClickListener((adapterView, view, position, id) -> {
            String data = adapter.getItem(position);
            presenter().onEditPhone(data);
        });

        recyclerView.setAdapter(adapter);
    }

    private void initDoneBtn() {
        doneBtn = rootView.findViewById(R.id.actionBarRightText);
        doneBtn.setOnClickListener(view -> presenter().onDoneClicked());
        doneBtn.setText(getString(R.string.done));
    }

    @Override
    public int getLayoutResId() {
        return R.layout.ac_adding_phone;
    }

    @Override
    public AddingPhonePresenter presenterProvider() {
        return new AddingPhonePresenter(this);
    }

    @Override
    public void updatePhones(Set<String> hints) {
        adapter.updateData(new ListDataSet<>(new ArrayList<>(hints)));
        adapter.notifyDataSetChanged();
    }

    @Override
    public void closeWith(Set<String> phones) {
        Intent intent = new Intent();
        intent.putExtra(KEY_ADDING_PARAMETER_RESULT_DATA, PhonesUtils.phonesToString(phones));
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    @Override
    public void updateFormattedPhone(String formattedPhone) {
        phonetEt.setText(formattedPhone);
        phonetEt.setSelection(formattedPhone.length());
        phonetEt.addTextChangedListener(phoneFormatter);
    }

    @Override
    public void editPhone(String phone) {
        phonetEt.setText(phone);
    }

    @Override
    public void clearEditPhoneView() {
        phonetEt.setText("");
    }

    private final class PhonesListItemHolder extends BaseViewHolder<String> {

        private TextView textValue;

        public PhonesListItemHolder(View view) {
            super(view);

            textValue = view.findViewById(R.id.text);
        }

        @Override
        public void onBind(String data) {
            super.onBind(data);

            textValue.setText(data);
        }
    }
}
