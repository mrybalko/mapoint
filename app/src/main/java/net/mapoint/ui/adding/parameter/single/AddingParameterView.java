package net.mapoint.ui.adding.parameter.single;

import net.mapoint.data.adding.ParameterData;
import net.mapoint.ui.adding.ParameterType;

import java.util.List;

import rx.functions.Action1;

public interface AddingParameterView {

    void updateHints(List<ParameterData> hints);
    void updateEnteredText(String enteredText);
    void clearEnteredText();

    void closeWith(ParameterData parameterData, ParameterType parameterType);
    void showCreatingConfirmationDialog(ParameterType parameterType,
                                        Action1<String> action);

    void updateValues();
}
