package net.mapoint.ui.adding.parameter.single;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import net.mapoint.R;
import net.mapoint.data.adding.ParameterData;
import net.mapoint.ui.adding.ParameterType;
import net.mapoint.ui.base.BaseActivity;
import net.mapoint.utils.list.BaseViewHolder;
import net.mapoint.utils.list.ListAdapter;
import net.mapoint.utils.list.ListDataSet;

import java.util.ArrayList;
import java.util.List;

import rx.functions.Action1;

public class AddingParameterActivity extends BaseActivity<AddingParameterPresenter> implements AddingParameterView {

    private View rootView;

    private EditText requestEt;
    private ImageView clearEt;
    private TextView doneBtn;
    private RecyclerView recyclerView;
    private View noContentHint;
    private ListAdapter<ParameterData> adapter;

    public static final String KEY_ADDING_PARAMETER_TYPE = "event_adding_parameter_type";
    private static final String KEY_ADDING_PARAMETER_VALUE = "event_adding_parameter_value";

    public static final String KEY_ADDING_PARAMETER_RESULT_DATA = "event_adding_parameter_result_data";

    public static Intent newIntent(final Context context,
                                   final ParameterType parameterType,
                                   final String value) {

        Intent intent = new Intent(context, AddingParameterActivity.class);
        intent.putExtra(KEY_ADDING_PARAMETER_TYPE, parameterType.value());
        intent.putExtra(KEY_ADDING_PARAMETER_VALUE, value);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        rootView = findViewById(R.id.root_view);
        initViews();

        int parameterTypeValue = (int) getIntent().getSerializableExtra(KEY_ADDING_PARAMETER_TYPE);
        ParameterType parameterType =  null;
        for (ParameterType tempParameterType : ParameterType.values()) {
            if (parameterTypeValue == tempParameterType.value()) {
                parameterType = tempParameterType;
            }
        }

        String value = (String) getIntent().getSerializableExtra(KEY_ADDING_PARAMETER_VALUE);
        presenter().parameterType(parameterType, value);

        findViewById(R.id.back_btn).setOnClickListener(view -> finish());
        noContentHint = findViewById(R.id.no_content_hint);
        noContentHint.setVisibility(View.VISIBLE);
    }

    private void initViews() {

        initRequestEditText();
        initRecycler();
        initDoneBtn();
    }

    private void initRequestEditText() {
        requestEt = rootView.findViewById(R.id.auto_complete);
        requestEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                presenter().onRequestTextUpdated(s.toString());
            }
        });

        clearEt = rootView.findViewById(R.id.clear_btn);
        clearEt.setOnClickListener(v -> {
            presenter().onClearClicked();
        });
    }

    private void initRecycler() {
        recyclerView = rootView.findViewById(R.id.recycler);

        LinearLayoutManager layoutManager = new LinearLayoutManager(AddingParameterActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(
                recyclerView.getContext(),
                layoutManager.getOrientation());

        recyclerView.addItemDecoration(dividerItemDecoration);

        adapter = new ListAdapter(
                (context, view) -> new HintsListItemHolder(view),
                () -> R.layout.li_hints,
                new ListDataSet(new ArrayList<String>()));

        adapter.setOnItemClickListener((adapterView, view, position, id) -> {
            ParameterData data = adapter.getItem(position);
            presenter().onItemClicked(data);
        });

        recyclerView.setAdapter(adapter);
    }

    private void initDoneBtn() {
        doneBtn = rootView.findViewById(R.id.actionBarRightText);
        doneBtn.setOnClickListener(view -> presenter().onDoneClicked());
        doneBtn.setText(getString(R.string.done));
    }

    @Override
    public int getLayoutResId() {
        return R.layout.ac_adding_parameter;
    }

    @Override
    public AddingParameterPresenter presenterProvider() {
        return new AddingParameterPresenter(this);
    }

    @Override
    public void updateHints(List<ParameterData> hints) {
        if (hints.size() > 0) {
            adapter.updateData(new ListDataSet<>(hints));
            adapter.notifyDataSetChanged();
            recyclerView.setVisibility(View.VISIBLE);
            noContentHint.setVisibility(View.GONE);
        } else {
            recyclerView.setVisibility(View.GONE);
            noContentHint.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void updateEnteredText(String enteredText) {
        requestEt.setText(enteredText);
    }

    @Override
    public void clearEnteredText() {
        requestEt.setText("");
    }

    @Override
    public void closeWith(ParameterData parameterData, ParameterType parameterType) {
        Intent intent = new Intent();
        intent.putExtra(KEY_ADDING_PARAMETER_RESULT_DATA, parameterData);
        intent.putExtra(KEY_ADDING_PARAMETER_TYPE, parameterType.value());
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    @Override
    public void showCreatingConfirmationDialog(ParameterType parameterType,
                                               Action1<String> action) {

        String enteredText = requestEt.getText().toString();

        if (!TextUtils.isEmpty(enteredText)) {
            String messagePrefix = (parameterType == ParameterType.Category) ? getString(R.string.add_custom_category) :
                    (parameterType == ParameterType.LocationName) ? getString(R.string.location_not_found) : "";

            final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(AddingParameterActivity.this);


            String message = messagePrefix;

            builder.setMessage(message);

            builder.setNegativeButton(R.string.OK, (dialog, which) -> dialog.dismiss());

            Dialog dialog = builder.create();
            dialog.show();
        }
    }

    @Override
    public void updateValues() {
        presenter().onRequestTextUpdated(requestEt.getText().toString());
    }

    private final class HintsListItemHolder extends BaseViewHolder<ParameterData> {

        private TextView hintValue;

        public HintsListItemHolder(View view) {
            super(view);

            hintValue = view.findViewById(R.id.text);
        }

        @Override
        public void onBind(ParameterData data) {
            super.onBind(data);

            hintValue.setText(data.value());
        }
    }
}
