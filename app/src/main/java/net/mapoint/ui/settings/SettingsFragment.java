package net.mapoint.ui.settings;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.mapoint.R;
import net.mapoint.ui.base.BaseFragment;

public class SettingsFragment extends BaseFragment<SettingsPresenter> implements SettingsView {

    private View rootView;
    private TextView signUpTv;
    private TextView signInTv;
    private View buttonsHolder;
    private View inputForm;
    private TextView inputFormBtn;

    private TextView repeatPassword;

    @Override
    public int getLayoutResId() {
        return R.layout.fr_settings;
    }

    @Override
    public SettingsPresenter presenterProvider() {
        return new SettingsPresenter(this);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        rootView = super.onCreateView(inflater, container, savedInstanceState);
        initViews();
        return rootView;
    }

    void initViews() {
        buttonsHolder = rootView.findViewById(R.id.buttons_holder);
        inputForm = rootView.findViewById(R.id.input_form);
        inputFormBtn = rootView.findViewById(R.id.input_form_btn);
        repeatPassword = rootView.findViewById(R.id.repeat_password);

        signInTv = rootView.findViewById(R.id.sign_in);
        signInTv.setOnClickListener(view -> {
            presenter().onSignInClicked();
        });
        signUpTv = rootView.findViewById(R.id.sign_up);
        signUpTv.setOnClickListener(view -> {
            presenter().onSingUpClicked();
        });

        showButtonsHolder();
    }

    @Override
    public void update() {
        presenter().updateModel();
    }

    void showBtnsHolderView(boolean show) {
        buttonsHolder.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    void showInputForm(boolean show) {
        inputForm.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showInputForm(SettingsPresenter.Type type) {

        switch (type) {
            case SignUp: {
                showBtnsHolderView(false);
                showInputForm(true);
                inputFormBtn.setText(R.string.sing_up_action);
                repeatPassword.setVisibility(View.VISIBLE);

                break;
            }
            case SignIn:{
                showBtnsHolderView(false);
                showInputForm(true);
                inputFormBtn.setText(R.string.sing_in_action);
                repeatPassword.setVisibility(View.GONE);
                break;
            }
            default:{}
        }
    }

    @Override
    public void showButtonsHolder() {
        showBtnsHolderView(true);
        showInputForm(false);
    }

    @Override
    public boolean handleBackPressed() {
        return presenter().needHandleBackPress() || super.handleBackPressed();
    }
}
