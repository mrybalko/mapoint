package net.mapoint.ui.settings;

import net.mapoint.ui.base.BasePresenter;

public class SettingsPresenter extends BasePresenter<SettingsView> {

    private static final String TAG = "SettingsPresenter";

    enum Type {
        None,
        SignIn,
        SignUp
    }

    private Type type;

    public SettingsPresenter(SettingsView view) {
        super(view);
        type = Type.None;
    }

    @Override
    public void updateModel() {

    }

    void onSingUpClicked() {
        type = Type.SignUp;
        runIfView(view -> view.showInputForm(type));
    }

    void onSignInClicked() {
        type = Type.SignIn;
        runIfView(view -> view.showInputForm(type));
    }

    boolean needHandleBackPress() {
        if (type != Type.None) {
            runIfView(SettingsView::showButtonsHolder);
            type = Type.None;
            return true;
        }

        return false;
    }
}
