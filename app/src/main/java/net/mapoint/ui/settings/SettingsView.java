package net.mapoint.ui.settings;

import net.mapoint.utils.Updatable;

public interface SettingsView extends Updatable {

    void showInputForm(SettingsPresenter.Type type);
    void showButtonsHolder();
}
