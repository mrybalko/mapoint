package net.mapoint.ui;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.nex3z.flowlayout.FlowLayout;

import net.mapoint.R;
import net.mapoint.data.category.CategoryData;
import net.mapoint.data.category.CategoryGroupData;
import net.mapoint.ui.widget.categories.CategoryGroupHeader;

import java.util.List;

public class GroupsRow extends ConstraintLayout {

    private boolean inited;

    private View selectAll;
    private LinearLayout groupsContainer;
    private FlowLayout categoriesContent;
    private View content;
    private String selectedGroupId;
    private SelectionListener selectionListener;

    public GroupsRow(Context context) {
        super(context);
        init(null);
    }

    public GroupsRow(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public GroupsRow(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    private void init(final AttributeSet attrs) {
        if (!inited) {

            final LayoutInflater inflater = LayoutInflater.from(getContext());
            inflater.inflate(R.layout.v_categories_row, this, true);
            content = findViewById(R.id.content);
            groupsContainer = (LinearLayout)findViewById(R.id.groups_container);
            categoriesContent = (FlowLayout)findViewById(R.id.categories_content);

            selectAll = findViewById(R.id.select_all);
            selectAll.setOnClickListener(v -> {
                if (selectionListener != null) {
                    selectionListener.onAllItemSelectedInGroup(selectedGroupId);
                }
            });

            updateContentVisibility(false);
            inited = true;
        }
    }

    public void updateGroups(List<CategoryGroupData> groups, String selectedGroupId, int groupsCount) {
        updateContentVisibility(false);

        groupsContainer.setWeightSum(groupsCount);
        groupsContainer.removeAllViews();

        this.selectedGroupId = selectedGroupId;
        for (CategoryGroupData group: groups) {
            CategoryGroupHeader header = createGroupHeaderView(group);

            boolean isExpanded = group.getId().equals(selectedGroupId);

            if (group.getNeedExpandItems()) {
                if (isExpanded) {
                    updateContentVisibility(true);
                    header.showExpandedIndicators(true);
                    updateSubcategories(group.getSubcategoryList());
                }
            }

            header.setExpanded(isExpanded);
            groupsContainer.addView(header);

            header.showCheckedIndicators(isExpanded || group.isSomeSubcategoriesChecked());
        }
    }

    private CategoryGroupHeader createGroupHeaderView(CategoryGroupData groupData) {

        CategoryGroupHeader headerView = new CategoryGroupHeader(getContext());

        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                0,
                LayoutParams.MATCH_PARENT, 1.0f);

        headerView.setLayoutParams(param);

        headerView.setTitle(groupData.getName());
        headerView.setSelectedColor(groupData.getColor());
        headerView.setIcon(groupData.getIcon());

        headerView.setOnClickListener(v -> {
            boolean newIsSelected = !headerView.isExpanded();

            String id = groupData.getId();
            if (selectionListener != null) {
                selectionListener.onGroupSelected(id, newIsSelected);
            }
        });

        return headerView;
    }

    private void updateContentVisibility(Boolean needShow) {
        content.setVisibility(needShow? VISIBLE : GONE);
    }

    public void updateSubcategories(final List<CategoryData> categories) {

        categoriesContent.removeAllViews();
        for(CategoryData category : categories) {
            categoriesContent.addView(createTextView(category.getId(), category.getName(), category.isSelected(), category.getColor()));
        }
    }

    private TextViewStroke createTextView(String itemId, String value, boolean isSelected, int selectedColor) {
        TextViewStroke textViewStroke = new TextViewStroke(getContext());
        textViewStroke.setTextColor(getResources().getColor(R.color.text_color_dark));
        textViewStroke.setText(value);
        textViewStroke.setColors(selectedColor, getResources().getColor(R.color.category_item_stroke_color_default));
        textViewStroke.setSelected(isSelected);

        textViewStroke.setOnClickListener(v -> {
            textViewStroke.setSelected(!textViewStroke.isSelected());
            if (selectionListener != null) {
                selectionListener.onItemSelected(itemId);
            }
        });

        return textViewStroke;
    }

    public void setSelectionListener(SelectionListener selectionListener) {
        this.selectionListener = selectionListener;
    }

    public interface SelectionListener {
        void onItemSelected(String itemId);
        void onAllItemSelectedInGroup(String groupId);
        void onGroupSelected(String groupId, boolean isChecked);
    }
}
